# Contributing guide
## Methodology
This translation, and the book that will be edited after will be
released bit by bit, maybe this way:
* we
  will
  [translate the book in plain text](https://gitlab.com/net-as-artwork/networking/tree/master/trans),
  to focus on the translation, not on the form the book could take
  (see the `trans` folder),
* then, we will take care of the
  *view*, formating and style by using
  the [Markdown](https://daringfireball.net/projects/markdown/)
  language, with [Pandoc](http://pandoc.org/) in head,
* finally, the publishing will be released by using Pandoc and a set
  of scripts in the `MAKEFILE`. The book will be released on a
  website, as PDF and/or printed to paper in a very DIY way, each chapter
  separately (the DIY releasing don't allow such a big book with
  photocopy).

## Infrastructure

[Git and Gitlab](https://gitlab.com/net-as-artwork/networking/) will
be used to translate the book.

## For people who don't know how to use git
If at some point it would be considered as a bad idea to use Git for
this work, it would be possible to use
[collaborative pads](https://cryptpad.facil.services/code/) for the work-in-progress and then export it back to git. It would be used if the team would grow around people who don't want/don't know how to use git.

## Download Git

[Download Git](https://git-scm.com/downloads) and then
[install it](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git).

You can then clone the repository by typing

    git clone git@gitlab.com:net-as-artwork/networking.git

## Conventions

During first chapter translation, we used a messy custom markup system manipulated with standard scripting tools (see `makefile`) but we progressively moved to more standard Pandoc's Markdown.

### Translations files

The translation files are under the `/trans` folder. Each file
contains a chapter and is named starting with a number to keep it in
order. They contains the original text *and* the french translations.

These files are used during the translation process. Once the translation is achieved, they should be archived in `/translation/archived/` and the file should be cleaned and converted to Mardown using `make remove-fake-markdown-comments` to work on editing and formatting.

#### Paragraphs

The original text lines all begins by a `_`.  It makes very easy
to remove them when the work is finished (see makefile).

    _ This is a paragraph of the original text book that you
    _ will translate. This is pure foo bar baz text.

For each paragraph of original text, add your translation under it. It makes possible to compare the text and review it easily.

    _ This is a paragraph of the original text book that you
    _ will translate. This is pure foo bar baz text.

    Voici le paragraphe du texte traduit qui doit rester sous
    son paragraphe parent de façon à facilter la relecture.

Note that until the end of chapter 01 translation, `#` was used, not `_`. We decided to use `_` as it don't have any markdown meaning (we hope it won't underline anything...)

#### Translator's notes

Translators notes must be written using Pandoc Markdown comments

    [//]: # (This is a translator note that will be ignored)
    [//]: # (by the Pandoc engine while exporting)

Note that it can be useful to define a macro in your text editor to convert a region to a note or to simply insert blank notes. For the record, we previously used, during the first chapter translation, the `--` prefix as a translator notes indicator but it is no longer the case.

    -- This was a note

A makefile command makes possible to find notes easily

    make find-notes

#### Text formating

Keep in mind that the original book words or bit of sentences that are in italics could be lost in the process. To simplify this keep the token in italics for the following editing part by using the standard Markdown syntax:

   this is an *italic string* in a longer text

### Editing files

After the translation, we need to fix typos and format the text. The files to use are in the `/editing` directory.

To convert a file, use `make remove-fake-mardown-comment`. The translation `.txt` file is automatically cleaned and copied to `/editing`.

#### Links

Links must be checked, and if broken, must be resolved with a [Web Archive link](htps://web.archive.org) edited this way

     [`https://tinyurl.com/ybrhyh6s`](https://web.archive.org/web/20150507054306/http://www.wumingfoundation.com/english/biography.html#nightbus)

They will display nicely with a TinyURL that makes possible for people to type easily the reference from a paper booklet, and in case of PDF, display a nice link.

    `https://tinyurl.com/ybrhyh6s`

They should also be displayed `in a monospace font`, as we didn't use this convention for anything else.

TinyURL has been choosed for being a good long term URL shortener service.

#### Footnotes

Author footnotes are referenced in the text by numbers between brackets (this is the [Pandoc's Markdown way](https://pandoc.org/MANUAL.html#footnotes) and it don't require for the identifier numbers to be ordered: they are only identifiers):

    This is a reference to a note[^11] followed by another note[^7]

The Notes can be found at the end of each chapter:

    [^11]: This this the definition of the note

    [^7]: This this the definition of the second note

When we will transform the document to PDF using Pandoc, the notes will be compiled appropriately on each page.

Translator notes must be used as footnotes. Use the same convention as above, by refering them with the *« , N.d.T »* mention (*« Note du Traducteur »*)

    This was difficult to translate[^12]
    ...
    [^12]: but we made it, N.d.T

Some utilities notes can be used many times. Their identifiers are above `99`:

    This texte contains some français[^100] word
    ...
    [^100]: En français dans le texte, N.d.T

Note that Emacs provide a lots of helper functions to deal with footnotes from it's [markdown-mode](https://jblevins.org/projects/markdown-mode/) (insertion, navigation, etc.).

### Translation consistency

We use the french inclusive grammar as much as we can. Please use the *« · »* [middle dot](https://fr.wikipedia.org/wiki/Point_m%C3%A9dian) for this.

Some terms can be tricky to translate because they are not well
defined in the field of the subject that interest us, they can be a
bit taboo (I am not sure that artists speak of "réseautage") or you
won't find it in dictionaries.

Please consider using these ones:

* **art of networking** => *art en réseau*
* **art as network** => ?
* **medium** -> *support*, *média*
* **networking** => *réseau*, *réseautage*, *mise en réseau*,
  according to the context
* **net (of relations)** => *réseau (de relations)*, *net*, according
  to the context
* **networker** => *travailleur en réseau*, *réseauteur*
* **to network** => *travailler en réseau*, *réseauter*
* **network platform** => *plateforme en réseau*
* **platform** => *plateforme*


Note that in specific contexts, other translations can be
better. Quebec french or Swiss can help as they seem to use less
anglicisms.

For correctness, do not use anglicisms when possible:

* **digital** => *numérique* (*digital* in french is related to
  **fingers**)

http://www.linguee.fr/francais-anglais/search?source=auto&query=networker

If you think these terms are bad, feel free to suggest better ones.

## Automating some processes
### Generating txt files from original book

It is possible to generate txt files from the PDF automatically
using [`pdftotext`](https://en.wikipedia.org/wiki/Pdftotext). Just
type:

    make txt

### TODO other automatized things

ex: deleting all the original text in work files

See makefile

### Emphasized text, quotes and italics

The most important thing is usually consistency.

They must be typed between `*` as in the following example.

    Les actions appelées *flux-events* ou *flux-concerts* selon les cas,
    sont basés sur l'exécution d'instructions en public, ouvertement
    banales, simplement des gestes de la vie courante, contenus dans de
    courts scripts

Quotes must respect the french typographic conventions, using non breakable spaces, *not normal spaces*, plus guillemets:

    Guglielmo Achille Cavellini a été cité dans de nombreux
    travaux de Vittore Baroni, qui le définit comme *« une figure centrale
    de l'aventure entière du mail art »* (Baroni, 1997).

See [#13](https://gitlab.com/net-as-artwork/networking/issues/13) for how to set Emacs to make easier to edit quotes.


## Text Edition

The translation is edited with
[Emacs](https://www.gnu.org/software/emacs/emacs.html). It is not
required to use this editor, though, it is required to use an editor
that keeps plain text and don't insert tags or formatting. The
following advice are related to Emacs. Feel free to use the editor you
like the most.

The spellcheck has been made using Flyspell and the Aspell spell
checkers and dictionaries. I will contribute some personal dictionary
for the many specific terms that can be found in this text.

The spellcheck can be done real-time using `flyspell`

    M - x flyspell-mode M - x ispell-change-dictionary

For ease of reading, all `fill` the text automatically

    M - x auto-fill

## TODO - Clean finished translations files of any comments

    make clean
