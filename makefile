DUMMY := $(shell if [ -f editing/index.md ]; then rm editing/index.md; fi) $(shell mkdir -p tmp)
MD := $(sort $(wildcard editing/*.md))
HTML := $(MD:editing/%.md=public/%.html)
PDF := $(MD:editing/%.md=public/%.pdf)

all: hello $(HTML) $(PDF) public/index.html public/book.epub public/book.pdf

hello:
	@echo "Bienvenue dans le projet de traduction de Networking"
	@echo ""

public/%.html: editing/%.md
	@echo "==> Converting $< to $@"
	@pandoc $< --include-in-header=utils/style.html -f markdown -t html -o $@

public/%.pdf: editing/%.md
	@echo "==> Converting $< to $@"
	@pandoc $< -o $@

editing/index.md: $(MD)
	@echo "==> Creating TOC from all markdown files"
	@cat title.txt > 'editing/index.md'; \
	echo "![](icon/book_networking_bazzichelli_cover_with_translation_proposal.gif)" >> 'editing/index.md'; \
	echo '' >> 'editing/index.md'; \
	echo "## Sommaire" >> "editing/index.md"; \
	echo "" >> "editing/index.md"; \
	for md in $(MD); do \
		html=`echo "$$md" | sed -e 's/editing\///g' -e 's/\.md/.html/g'`; \
		pdf=`echo "$$md" | sed -e 's/editing\///g' -e 's/\.md/.pdf/g'`; \
		grep "$$md" -e '^# ' | sed -e 's/^# /+ [/g' -e "s/$$/]($${html}) [(pdf)]($${pdf})/g" >> "editing/index.md" ; \
	done; \
	echo -e '\nCe livre est aussi disponible au format [EPUB](book.epub) et [PDF](book.pdf). Il est en cours de traduction et est publié sous [GNU Free Documentation License](https://gitlab.com/net-as-artwork/networking/blob/master/LICENSE). Vous pouvez aider à traduire cet ouvrage sur [Gitlab](https://gitlab.com/net-as-artwork/networking/).\n\nTraduction française coordonnée par [Sébastien Feugère](https://hi.balik.network/)' >> 'editing/index.md'

public/book.epub: $(HTML)
	@echo "==> Generating epub"
	@pandoc -o public/book.epub title.txt $(HTML)

public/book.pdf: $(MD)
	@echo "==> Generating pdf"
	@pandoc --file-scope --toc -V documentclass=book -o public/book.pdf title.txt $(MD)

# Convert the PDF file to txt
pdf2txt:
	pdftotext src/networking_bazzichelli.pdf
	echo "networking_bazzichelli.txt has been write to src/"
	tree src

# TODO - Insert license in work files
insert-license:
	echo "TODO Insert license in work files"
	# Copyright (c) French edition 2016, 2017, 2018, Sébastien
	# Feugère and Seb Hu-Rillettes
	# Copyright (c) English edition 2008, Digital Aesthetics Research
	# Center, Aarhus University, and Tatiana Bazzichelli
	# Copyright (c) Italian edition 2006, costlan editori s.r.l., Milan

	# Permission is granted to copy, distribute and/or modify this document
	# under the terms of the GNU Free Documentation License, Version 1.3 or
	# any later version published by the Free Software Foundation; with no
	# Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.  A
	# copy of the license is included in the section entitled "GNU Free
	# Documentation License".

# Displays lines that starts with --
find-notes:
	grep "\--" trans/*.txt

stop-working-with-sharp-symbol:
	sed -i -e 's/^#/_/g' trans/05_art_on_the_net_and_for_the_net.txt

# Remove markown comments and create the editing file
# If the file already use, remove all unecessary stuff before
clean-original-translation-comments:
	sed '/^_/d' trans/archived/02_towards_the_cyber_utopias.txt >> editing/02_towards_the_cyber_utopias.md

# Convert the translation .txt files to markdown
txt2markdown:
	# TODO
	cp trans/*.txt view/
	# Some conventions should be auto changed, like comments

commiter-feugere:
	@git config user.name "Sébastien Feugère"
	@git config user.email "smonff@riseup.net"

commiter-rillettes:
	@git config user.name "Seb Hu-Rillettes"
	@git config user.email "shr@balik.network"

md2pdf:
	pandoc --latex-engine=xelatex editing/000_preface.md -o export/000_preface.pdf
	pandoc --latex-engine=xelatex editing/01_networking_cultures.md -o export/01_networking_cultures.pdf
