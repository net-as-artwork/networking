# Les cultures en réseau
## Le réseau comme Art

[//]: # (Fri Jun 17 17:35:09 Je décide d'adopter "art en réseau" pour "art)
[//]: # (of networking", bien que "networker" peut se traduire par)
[//]: # ("réseauteur", certainement essentiellement dans des textes Suisses)
[//]: # (ou Québécois et que on aurait pu employer "réseautage". Mais celui)
[//]: # (ci est trop connoté à quelque chose de purement "professionnel" et)
[//]: # ("corporate". Toutefois, les métaphores du lien et de la mise en )
[//]: # (relation ne doivent pas être exclues non plus, on peut toujours )
[//]: # (utiliser "mise en réseau" pour "networking" dans un contexte où)
[//]: # (c'est signifiant)

L'art en réseau peut être vu comme une métaphore de l'art comme un
réseau; un art qui crée un réseau. Afin d'être capable de catégoriser
correctement les nombreuses pratiques artistiques de l'art multimédia
au cours des vingt dernières années, il est fondamental de
reconstruire ce concept d'art en réseau. Ce concept est souvent
utilisé dans les nouveaux médias. Il est encore rarement analysé pour
ses caractéristiques propres, excepté par quelques publications qui
sont pour la plupart disséminées dans des cercles autonomes ou promus
par des maisons d'édition spécialisées[^1].

L'art en réseau est une pratique artistique qui prend ses origines
dans un passé plutôt lointain. Par *« réseauter »*, on entend créer
des réseaux de relations, partager des expériences et des idées. Cela
signifie aussi créer des contextes dans lesquels des personnes peuvent
se sentir libres de communiquer et de créer d'une façon
*« horizontale »*. C'est-à-dire d'une façon dans laquelle l'expéditeur
et le destinataire, l'artiste et le public, sont (con)fondus ; perdant
ainsi leur sens initial.

L'art en réseau est basé sur la figure de l'artiste comme créateur de
plateformes de partage et de contextes favorisant la connexion et
l'échange. Cette figure se dissémine parmi ceux qui acceptent
l'invitation et créent des occasions de mise en réseau en retour. Pour
ces raisons, parler encore d'artiste ne fait plus sens ici, puisque le
sujet actif devient tour à tour opérateur du réseau ou
réseauteur. Peut être qu'il s'agit du genre d'art contemporain le plus
difficile à définir, car il ne repose ni sur des objets physiques, ni
sur des outils numériques ou analogiques, mais sur des individus
entretenant des relations et construisant des processus. Individus qui
peuvent créer d'autres contextes relationnels à leur tour, ou faire
naître à des productions créatives, ce qui n'est pas négligeable si
l'on considère plus attentivement l'idée de partage.

[//]: # (Fri Jun 17 22:17:12 La logique commerciale autorise de "vendre")
[//]: # (l'idée de partage ?)

La logique commerciale, souvent connectée aux environnements
traditionnels de l'art contemporain permet *« l'échange »* de ce don
spontané. C'est peut-être la raison pour laquelle il existe peu de
publications sur le sujet, parce que il a toujours été considéré qu'il
était plus important de vivre jusqu'au bout certaines dynamiques,
plutôt que d'écrire à propos d'elles. De plus, dans le milieu des
expositions et dans les textes écrits, il est toujours nécessaire de
réaliser un processus de sélection. Cependant, cet aspect n'existe pas
dans le monde du réseau : tout le monde peut contribuer en amenant sa
propre pierre à l'édifice, et une liberté totale existe.

[//]: # (Fri Jun 17 23:21:31 Sur ces deux dernières lignes, on peut se)
[//]: # (demander la pertinence d'une telle assertion, à savoir: la liberté)
[//]: # (totale existe-t-elle (nous ne répondrons pas à la question, mais)
[//]: # (nous posons des doutes quand à son existence, hors système)
[//]: # (utopique) et deuxièmement, est-ce que tout le monde peut vraiment)
[//]: # (apporter sa pierre à l'édifice ou bien certains phénomènes sociaux)
[//]: # (reprennent-t-ils leurs droits en privant certains de la possibilité)
[//]: # (de participer?)

[//]: # (Fri Jun 17 23:37:10 Il pourrait être intéressant de définir en quoi)
[//]: # (les travaux de Dürer sont "extraordinaires" dans la perspective de)
[//]: # (notre propos.  https://en.wikipedia.org/wiki/Melencolia_I)

Commençons donc au commencement. D'après ma reconstruction, les
premières formes d'art comme réseau ont eu lieu au seizième siècle, et
en particulier dans la période maniériste. Cette période est définie
fortement par l'interconnexion entre des artistes vivant dans
différents pays, allant de l'Europe du Nord à l'Italie. Albrecht
Dürer, un protagoniste clé, qui parmi ses nombreux travaux a créé des
xylographies et des gravures d'un intérêt extraordinaire, a grandement
influencé l'iconographie de cette période. Pensons par exemple à la
fameuse xylographie *Melancolia* de 1514. Dans la seconde moitié du
quinzième siècle en Allemagne, s'était déjà répandue la technique de
reproduction en série des images sur papier. Par exemple, Gutenberg
imprima sa *Bible Latine à 42 Lignes* à Mayence, et a obtenu
approximativement un tirage de 180 copies. À cette époque, le fait de
créer des images *« en série »* était considéré comme une véritable
révolution, un phénomène qui peut vraiment être comparé à l'avènement
de la télévision et des ordinateurs et aux techniques de reproduction
numérique inventées cinq cents ans plus tard.

Avec Albrecht Dürer nous trouvons les caractéristiques d'une forte
modernité qui lui permet de disséminer ses images partout en Europe,
pour donner naissance à un réseau d'échanges et de relations lui ayant
apporté une présence jusqu'en Italie. Dans la première partie du
seizième siècle l'activité d'Albrecht Dürer pose les fondations de la
formation d'une culture Européenne unifiée. Ses gravures et
xylographies étaient trouvables dans de nombreux pays, et comme
c'était le cas pour Marcantonio Raimondi, elles faisaient parfois
l'objet de contrefaçons. Dürer a constitué une relation remarquable
avec l'Italie, puisqu'il était désireux d’entrer en contact avec la
pensée de la Renaissance et avec les personnalités les plus reconnues
de cette époque, telles que Giovanni Bellini et Jacopo de’ Barbari,
créateur, entre autres, de la fameuse xylographie de Venise [^2].

Pour revenir aux dynamiques de réseaux, l'intérêt dans la diffusion du
travail de chacun de façon périodique et réticulaire se retrouve dans
bien des contextes, qu'ils soient internationaux ou italiens. Le
phénomène contemporain qui relie le mieux ces pratiques est le mail
art. Il s'agit d'une ligne de transmission du savoir qui a conduit aux
types d'expérimentations vidéos et de langages artistiques présents
dans les pratiques de Fluxus. Ces pratiques s'emboîtent comme des
poupées russes et sont visibles dans un large éventail
d'expérimentations artistiques, tout particulièrement dans
l'avant-garde des premières décades du vingtième siècle.

En effectuant un bond dans le temps, on voit que dans les années
quatre-vingts, le réseau a commencé à développer une forte identité en
Italie (il a commencé à se développer dans les années soixante-dix
dans le contexte Européen). Il a ensuite évolué en une dimension
couvrant différents médias, profitant de leurs développements
respectifs au fil du temps. À cette époque, les photocopieurs étaient
répandus et leur utilisation s'est pérennisée dans les milieux
*« alternatifs *. Il suffit de penser à la production massive de
punkzines et fanzines dans les Centre Sociaux, tout comme à leur
production en dehors de cet environnement. Par ailleurs, l'idée de la
mort de l'art à l'intérieur de l'idéologie punk vise à rendre
accessible la création à tous. N'importe qui peut participer, du
moment que le désir de faire est là. Le concept du *« Do It
Yourself »* se retrouve dans le phénomène à venir du réseau ; en
combinant les apports du mail art, du néoisme au plagiarisme en
passant par Luther Blissett, et jusque dans les années
quatre-vingt-dix, quand la dynamique de réseau s'est affirmé en masse
au travers de l'utilisation des ordinateurs et d'internet.

Dans les années quatre-vingt-dix, l'idée de réseau fut développée de
façon plus approfondie, adoptant des formes issues du monde de
l'éthique hacker. Bien plus qu'un *« happening artistique »*, cette
idée de réseau ne permet pas de négliger le fait que la technologie
devrait être considérée d'un point de vue critique comme support
ouvert. Dans le domaine informatique, le terme de *« hack »* désigne
littéralement une *« bidouille ingénieuse »*. Ce terme est associé aux
concepts d'échange et de partage social, dont l'objectif est la
circulation de l'information et l'accès à la connaissance. Le but
d'une telle approche face à la technologie n'est pas la destruction,
mais la création, donner vie à des processus dans lesquels
l'utilisation de l'informatique est un support important, pas une fin
en soi. Cette *« aptitude »* apparaît à la fin des années
quatre-vingts, avec les progrès des BBS, système de bulletins
électroniques, ces bases de données permettant d'envoyer et de
diffuser de l'information d'un point à un autre par la connexion à un
ordinateur et à un modem, construisant ainsi les bases des plateformes
communes qui allaient marquer la future histoire de l'Internet.

L'idée du réseau comme art se retrouve également dans de nombreux
travaux de net.art. Les Italiens Marco Deseriis et Giuseppe Marano
définissent le net.art comme *« l'art de la Connexion »*. Dans leur
livre, Deseriis et Marano racontent sa naissance et son évolution,
décrivant une grande partie de ce phénomène qui s'est développé à un
niveau international dans les années quatre-vingt-dix.[^3] En réalité,
la plupart du courant net.art était diffusé par le biais de
plateformes collectives. Les listes de diffusion telles que
*« Nettime »* [`www.nettime.org`](www.nettime.org/) en sont un exemple
concret.

L'art en réseau va au-delà de l'idée des moyens de communication vus
comme des conteneurs et il devrait être interprété comme une oeuvre
conceptuelle dans laquelle ce qui compte est la capacité de connexion,
dépassant l'idée d'un art créé par un individu. Dès lors, *« l'Art est
facile »*, comme l'a dit Guiseppe Chiari en 1972, libérant le
spectateur de ses inhibitions liées au respect d'un art se posant
comme *« supérieur »*.[^4]

## Du Ready-made à Fluxus

Notre parcours commence au temps des avant-gardes historiques. Pendant
cette période, nous nous sommes dirigés vers la construction d'une
société de masse qui fut la conséquence directe de l'affirmation
propre de la *foule urbaine* comme un objet social (clairement visible
lors des premières Expositions Universelles de la seconde moité du
dix-neuvième siècle)[^5].

Dans les collages cubistes et dadaïstes, des fragments de la vie de
tous les jours tels que des tickets de train, des articles de
journaux, des instantanés, des objets banals tels que des bouteilles,
des chapeaux, des roues de bicyclettes, des fers à repasser, sont
devenus partie intégrante du monde de l'art. Dans l'oeuvre bien connue
de Marcel Duchamp *« Fountain »*, datant de 1917, un urinoir devient
une oeuvre d'art simplement au travers du *« geste »* de l'artiste de
lui donner ce statut, en le signant. La pratique du *« readymade »*
décontextualise un objet, lui attribuant un sens nouveau et
différent. Le *« readymade »* ouvre le champ au commencement de ces
pratiques artistiques qui transforment des objets du quotidien en des
oeuvres d'art, faisant voler en éclats les concepts traditionnels de
l'oeuvre d'art elle-même. L'œuvre peut tout à fait être un objet
commun, qui devient simplement de l'art parce que l'artiste en a
décidé ainsi.

[//]: # (1 Il serait hyper-malin ici de citer Daniel Spoerri)
[//]: # (2 À propos des "matériaux usagés et détériorés", on peut lier ceci)
[//]: # (directement à l'(anti-)esthétique punk/DIY, mais qu'est ce que une)
[//]: # (esthétique DIY???)

Le néo-dadaïsme aux États-Unis et le nouveau réalisme en Europe ont
développé la pratique du *« readymade »* dans les années cinquante,
pas seulement en donnant vie à des compositions d'objets du quotidien,
ce qui s'inscrivait dans le contexte des *collages*[^100] de
l'avant-garde historique, mais en insérant aussi des objets de
récupération. Par exemple, la série des *« Poubelles »*[^100] de Arman
Fernandez nous rappellent les travaux du dadaïste Kurt Schwitters, qui
a composé des *assemblages* à partir de matériaux usagés et
détériorés. Robert Roschenberg associait quand à lui la peinture
gestuelle de son *« Action Painting »* avec des fragments du
quotidien.

Le Pop art des années soixante comporte la même omniprésence du
quotidien, dans lequel la standardisation de la production
industrielle devient une valeur artistique en soi. L'idée de réinsérer
la vie dans l'art se retrouve également chez Piero Manzoni, qui a
travaillé dans le sillage de la vague *pop* des années soixante. En
1961, il *produit* une oeuvre composée d'une série de de boites de
conserve numérotées de la désormais fameuse *« Merde
d'Artiste »*. Simonetta Fadda remarqua que, dans ce cas, la figure de
l'artiste comme créateur occupe de un point central.

> *Piero Manzoni a présenté* « Merde d'Artiste » *comme sa propre
> production. Il la légitimise comme telle. C'est une affirmation
> paradoxale sur le pouvoir que l'artiste possède sur la réalité, qui
> rend la vie inclue dans l'art (comme Marcel Duchamp l'a fait plus
> tôt). Ce n'est seulement plus tard, avec les pratiques du réseau sur
> internet, que le point de vue s'est retrouvé inversé: le but n'est
> plus de réinsérer de la vie dans l'art, mais de réinsérer l'art dans
> la vie. C'est la véritable révolution de notre temps (Fadda, 2006,
> conversation privée avec l'auteure)*.

La recherche dans le domaine de la relation *« art-vie »* renvoie aux
performances et *happenings* de la fin des années cinquante. Ces
*« oeuvrévènements »*[^37] qui se sont tout particulièrement affirmés
dans la décade suivante, ont défini un lien en pleine ascension entre
pratiques artistiques et expériences concrètes. L'oeuvre d'art était
un élément matériel jusqu'à ce qu'elle ne devienne un évènement,
poursuivant la voie initialement établie par Giacomo Balla et
Fortunato Depero dans le *« Théâtre des Objets Futuristes »*[^39] et
les soirées de spectacles dada. Avec les *happenings*, une forme d'art
a pris forme et continue de se développer en éliminant presque
complètement la composante objet. L'art devient une action en soi et
le spectateur est invité à intervenir personnellement, à s'investir
dans le processus. Ces pratiques artistiques jouent avec le
non-prédictible: l'art veut être une découverte, une expérience, une
pratique de terrain, une interaction.

Durant les *« actions »* de Allan Kaprow, l'inventeur du *happening*
comme *« oeuvrévènement »*[^36], les participants agissent en suivant
un script. Bien que le point de départ soit connu, l'oeuvre résulte de
l'interaction entre les artistes et les spectateurs. Avec la pratique
des *happenings*, on voit l'apparition d'une forme d'*art Intermédial*
réunissant le théâtre, la musique, la littérature, et les modes
d'expression picturaux ou sculpturaux. Par exemple, la Merce
Cunningham Dance Company et le musicien John Cage, qui ont servi
d'inspiration à Fluxus, fonctionnent sur le même terreau
artistique. De plus, Fluxus est le noyau principal de notre discussion
sur le réseautage, car il propose des hypothèses qui seront
rencontrées plus tard dans la plupart des collectifs artistiques,
posant les fondations de la culture du réseau telle que nous
l'entendons aujourd'hui.

Fluxus a tenté d'éliminer les barrières entre le compositeur et ceux
qui écoutent, entre l'artiste et ceux qui observent, en maintenant un
dialogue constant composé de pratiques concrètes, minimales, éphémères
et iconoclastes, dans lesquelles les composantes de surprise et
d'ironie jouent un rôle clé. D'après Ken Friedman, il s'agit d'une
*« philosophie jetée à travers la réalité »* qui a complètement
révolutionné le concept d'évènement artistique. Hypothèse
fondamentale, malgré le fait que la plupart des évènements Fluxus se
tenaient sur *scène*. Dans les performances Fluxus, est maintenue en
réalité une mise à distance des membres du public, qui sont plus
connectés avec ses *« créatures »* que avec ses performances. Dans
tous les cas, Fluxus reste fondamental comme outil d'interprétation
des phénomènes à venir, s´étalant du mail art au néoisme jusqu'aux
nombreuses expérimentations vidéo et informatiques à venir.

La naissance du mouvement Fluxus s'inscrit dans la période allant de
la fin des années cinquante au début des années soixante. Fluxus a
réuni des participations artistiques hétérogènes, s'étant initialement
rencontrées à New York, à Manhattan, dans le quartier de Soho,
pratiques faisant figurer la participation active de Marcel
Duchamp. Beaucoup des étudiants de John Cage, de Allan Kaprow à Dick
Higgins, Alison Knowles, Jackson McLow, Richard Maxfield, George
Brecht et Al Hansen se sont rencontrés en 1958 à la New School for
Social Research. Ils constituent le noyau dur du mouvement qui va
alors se répandre partout en Europe grâce aux démarches de Georges
Maciunas, figure clé de Fluxus.

Maciunas, le véritable réseauteur du groupe, donne naissance en 1961 à
la galerie A/G en collaboration avec Almius Salcius, contribuant à la
formation d'un vaste réseau composé de nombreuses personnalités
actives au sein du cercle littéraire et plus largement, du milieu
artistique. Parmis eux, La Monte Young, Yoko Ono, Terry Riley et
Simone Forti, juste pour en citer quelques un·e·s, ont gravité entre
New York et San Francisco plus tard.

Au début des années soixante, c'est grâce à Maciunas et Higgins que
Fluxus s'est fait connaître en Europe, faisant se rencontrer des
personnalités telles que Robert Filliou, Joseph Beuys, Nam June Paik,
Wolf Vostell et des musiciens tels que l'Italien Giuseppe Chiari, qui
sont fondamentaux, de par leurs contributions dans le champ de l'art
contemporain.

[//]: # (Mon Aug 15 19:13:46 à propos des "fluxus-event scores", on dirait)
[//]: # (clairement de la programmation, des instructions exécutées, ce qui)
[//]: # (nous amène à nous interroger sur l'héritage de ces pratiques)

Fluxus a été central dans l'élaboration de la notion d'*intermedia*,
qui se retrouve dans les pratiques futures en réseau, ainsi que dans
la notion d´évènement procédural collectif. Les actions appelées
*flux-events* ou *flux-concerts* selon les cas, sont basés sur
l'exécution d'instructions en public, ouvertement banales, simplement
des gestes de la vie courante, contenus dans de courts scripts (les
*fluxus-event scores*).

L'idée d'*intermedia*, une terme utilisé dans une très modeste mesure
pour la première fois par le poète-philosophe Samuel Taylor
Coleridge[^6], décrit la pratique d'assembler différents médias et
types d'expression artistiques, allant du design à la poésie, de la
peinture au théâtre. Un concept presque naturel pour Fluxus si l'on
considère les nombreux éléments interdisciplinaires présents dans le
mouvement. Dans les *flux-events*, les schémas d'expression
traditionnels sont démolis, alors que les intervention provocantes
souvent ironiques issus de micro-objets de la vie quoitidienne
deviennent des points centraux, des chapeaux aux raquettes de
ping-pong, équipés de symboles récurrents qui sont quasiment devenus
*« mystiques »*. Les actions ordinaires sont décontextualisées et
deviennent des concepts prenant un sens nouveau et souvent
imprévisible.

La première forme de la collection des *fluxus-events* se retrouve
dans les partitions des *fluxus-concerts*: les interprètes lisaient
les instructions ou les propositions qui devaient être suivies: une
sorte de *« composition ouverte »* à réaliser durant le
*happening*. Plus tard, les évènements furent organisés de façon
différente, que ce soit par les collections de cartes postales telles
que *Water Yam* par George Brecht[^37] ou par les livres comme
*Grapefruit* de Yoko Ono[^38], en passant par les pamphlets produits
sous l'emblème du Great Bear, une collection publiée dans les années
soixante par Dick Higgins pour Something Else Press, la maison
d'édition qu'il a fondé en 1964 (les pamphlets de la série du Great
Bear peuvent être consultés à l'adresse
[`http://www.ubu.com/historical/gb/index.html`](http://www.ubu.com/historical/gb/index.html)).

George Maciunas est une autre figure emblématique qui s'est fortement
appliqué à ce que Fluxus laisse des traces visibles. Il s'en est
assuré en en éditant des ouvrages de manifestations et travaux
individuels ou collectifs, tels que la publication de *An Anthology*
ou *Fluxus YearBook*, en compilant les *fluxfilms*, ou en créant ce
que l'on a nommé les *fluxboxes*: de petites boites contenant de
petits objets *« subversifs »* et des multiples. Les fluxboxes sont
des oeuvres multimédia totales, chacune différentes les unes des
autres. Elles apportent une authenticité qui rend encore plus évident
que le réseauteur n'est pas seulement un créateur de
contextes/évènements collectifs, mais aussi un véritable artiste en
réseau[^7].

## Les Diagrammes Fluxus

De 1953 à 1973, George Maciunas créa de véritables cartes
hypertextuelles au travers de schémas et de diagrammes dessinés à la
main sur des pages de bloc-notes en se conformant au concept de Fluxus
comme étant une *« attitude alternative face à l'art, la culture et la
vie »*[^8]. En 1961, c'est Maciunas lui même qui forgea le terme
Fluxus: l'art comme étant supposé être un divertissement, amusant,
simple, pas prétentieux pour un sou et existant afin de se focaliser
sur des aspects insignifiants de la vie, ne supposant pas de larges
compétences pour être saisi, et sans aucune valeur
institutionnelle. Fluxus est plus interprété comme une attitude, une
forme de vie, que comme un mouvement artistique. Le terme Fluxus
dérive du verbe Latin *fluere*, et l'individu est considéré comme
*fluctuant*, un courant dans lequel la vie quotidienne fait son entrée
dans l'art. *Tu ne fais pas du Fluxus, tu es Fluxus*[^9].

Pour Maciunas, Fluxus ne se manifestait pas seulement comme une
attitude conceptuelle, mais était considéré comme une pratique de vie,
et une *créature* de l'art d'une façon totalement ouverte et libre en
même temps. En opposition au très *glamour* Pop Art, Fluxus a été
initié comme une forme d'art *« abordable »*. Bien informé de
l'histoire de l'Union Soviétique (il a passé son enfance en Lituanie),
Maciunas concrétisa ses convictions dans les valeurs démocratiques de
l'art avec l'idée d'une réalisation progressive d'un *Art total*, en
s'inspirant du courant des constructivistes Russes et des mouvements
anarchistes du début des années vingt, tout en travaillant sur des
concepts tels que le *« slavophilisme »* en opposition à
*« l'occidentalisme »* (il contribua à la définition de ces
termes). Sa biographie le relie intimement à trois pays, les USA,
l'Allemagne et la Lituanie, et cela se retrouve dans ses travaux et
créations au sein de Fluxus comme un véritable réseau étendu
d'artistes *« hypermédias »*. Dans la position intellectuelle qu'il a
tenue au fil des années. on peut aussi citer l'inspiration du *Left
Front of the Arts* (LEF)[^40], créé en 1917. LEF est une association
d'artistes socialistes qui entrait en résonance avec ses objectifs de
dépasser l'art bourgeois - l'idée traditionnelle de la musique, du
théâtre, de la sculpture, de la peinture et de la poésie - mettant au
jour une nouvelle forme d'art *« augmenté »*.

Dans ses *Fluxcharts* (les diagrammes Fluxus), Maciunas applique la
théorie de l'expansion du temps et de l'espace directement à la
musique expérimentale de John Cage dans la reconstruction de périodes
historiques à une échelle macroscopique. Le premier diagramme *Atlas
of Russian History* a été débuté au printemps 1950, et a été suivi par
l'*Atlas of Prehistoric Chinese Art* en 1958 puis par le *Greek and
Roman History of Art Chart*, des thèmes que Maciunas explora lors de
ses études à l'université. Bien que construits que sur de simples
pages de bloc-notes, les diagrammes historiques de Maciunas sont
résolument hypertextuels et tri-dimensionnels. Subdivisés en sections
contenant des écritures manuscrites élégantes et minutieuses, insérés
à l'intérieur d'axes spatio-temporels, qui en réalité devaient être
lus par l'ouverture de la *« fenêtre »* suivante, construites avec
d'autres bouts de papier collés à coté du texte de façon à creuser
plus profondément, comme des sortes de pop-ups en papier.

Le premier diagramme explicitement relatif à Fluxus date de Décembre
1961 et a été montré au public lors d'une petite manifestation d'été
néo-dada organisée par Maciunas à la Galerie Parnass à
Wuppertal. Personne ne connaissait Fluxus à l'époque en Europe et
l'évènement fut présenté comme une relecture des travaux de John Cage
et une présentation de l'esthétique de l'art post-Cagien en
Amérique. L'évènement était présenté d'une façon rigoureusement
académique. Maciunas lui même prépara une lecture de son *Neo-dada in
Music, Theater, Poetry and Art* traduit en Allemand et récité par
l'acteur Arthus C. Caspari. Dans le texte, Fluxus n'est pas mentionné
explicitement et le centre d'intérêt semble être porté sur quelque
chose d'autre.

C'est dans ce contexte que se déroule l'entrée de Fluxus sur la scène
artistique Européenne. En guise de surprise, Nam June Paik monta sur
scène derrière Caspari et montra un des schémas de Maciunas, dessiné à
grande échelle sur une planche de bristol blanc, générant des rires
dans le public. Le diagramme est une republication en Allemand de
*Time/time projected in 2dim.space POETRY GRAPHICS/space projected in
time GRAPHIC MUSIC/Time/Time projected in space MUSIC THEATRE;* résumé
en: *Time-Space Chart*[^41]. C'est ce projet qui anticipa le concept à
venir des *Fluxcharts* de Maciunas.

Le large panel de pratiques artistiques dans lequel Fluxus est
intervenu est reconstitué dans les Fluxcharts à partir d'un flux
linéaire allant de dada aux poésies sonores futuristes en passant par
les collages dada de Kurt Schwitters. Les catégories de poésie,
illustration, musique et théâtre sont divisées en treize
sous-catégories insérées dans un espace-temps artistique nommé
*time-art* et *space-art*. Les différents *Fluxartistes* sont répartis
à l'intérieur des treize catégories en se basant sur leurs modes
d'expression et certains, tels que Jackson Mac Low et Dick Higgings
sont inclus dans plusieurs catégories du fait de leur propension
exacerbée à être*« intermédiaux »*.

Maciunas était un pionnier de nos temps, créant périodiquement une
*Fluxnewsletter* afin de pouvoir recueillir des conseils ou une
orientation des membres du groupe, en ce qui concerne l'ajout du nom
de certains artistes ou de diverses pratiques à ses schémas,
collectant inlassablement des informations sur chacun d'entre elleux
et archivant ces informations.

Un autre schéma Fluxus daté de 1966, appelé *Fluxus (Its historical
Development and Relationship to Avant-garde Movement)*, est divisé en
deux parties. Dans la partie supérieure, Maciunas essaie de donner une
définition de ce que sont l'*art* et le *non-art*. La partie
inférieure est une schématisation de ce qu'est Fluxus et sa relation à
tous les autres *–ismes*, en le distinguant du modernisme. D'après
Maciunas, Fluxus prend ses racines depuis le théâtre Baroque et dans
les spectacles *« multimedia »* montrés dans les jardins de
Versailles, jusqu'au Readymade de Marcel Duchamp et à la musique de
John Cage, les prédécesseurs directs de Fluxus. Ces derniers sont par
conséquent classés dans les catégories spacio-temporelles des artistes
qui *appartiennent ou n'appartiennent pas* à Fluxus. Dans ce schéma
toutefois, tout comme dans les *Expanded Art Diagrams*, qui arriveront
plus tard, il est intéressant de noter que Maciunas n'a pas créé une
simple liste de noms, mais qu'il a divisé les gens en quatre
catégories principales.

C'est peut-être là que réside le *« côté obscur »* de la fascinante
opération d'historisation de George Maciunas, une caractéristique la
rendant moins *« mécanique »* et plus humaine. Le talon d'Achille du
réseauteur est souvent sa manie de la schématisation et du
catalogage. Il s'agit également de l'aspect le plus délicat de l'Art
en Réseau, intimement connecté aux personnalités et éléments
empiriques de quiconque s'attelant à faire avancer les choses.

La première catégorie de schéma inclut les *pré-Fluxiste* Georges
Brecht et *Ben* Vautier déjà actifs sur ce terrain avant la naissance
de Fluxus. Le noyau dur du groupe apparaît plus tard sur la ligne
spatio-temporelle: George Brecht et Ben Vautier, George Maciunas,
Robert Watts, Philip Corner, Dick Higgins, Alison Knowles, Benjamin
Patterson, Nam June Paik, Jackson Mac Low, Tomas Schmit, Emmett
Williams, Henry Flynt, tous insérés dans la catégorie *Fluxus*.

Ensuite apparaissent les *« indépendants »*, allant de Joseph Beuys à
Ray Johnson, en passant par La Monte Young, qui ont seulement
collaboré temporairement avec Fluxus. Enfin, toujours en suivant la
ligne de progression du temps qui s'étend de 1959 à 1966, le groupe
des *« exclus »* apparaît, celui des membres fondateurs de Fluxus qui,
d'après Maciunas, ont décidé de s'écarter d'eux même du groupe. Ils
sont reconnaissables dans le diagramme par une ligne en pointillés,
dans la continuité de l'axe du temps.

[//]: # (À ce stade, une illustration de schéma sous licence libre serait)
[//]: # (bienvenue ^^ (voir http://www.georgemaciunas.com et une éventuelle)
[//]: # (authorisation de reproduction via commons.wikimedia.org?). Également,)
[//]: # (http://www.georgemaciunas.com a été rajouté en)
[//]: # (webliographie)

Les raisons mise en avant par Maciunas pour cette liste de membres
expulsés comprend les critères suivants: *« attitude allant contre
l'esprit collectif, excès d'individualisme, désirs de gloire
personnelle, complexe de la prima donna »*. De nombreux représentants
internationalement reconnus de ce mouvement ont été inclus dans ce
groupe d'expulsés et listés en introduction au schéma[^10]. Des
raisons secondaires, mais pas suffisantes pour justifier une
*« exclusion »*, étaient *« opportunisme, transfert dans des groupes
rivaux consécutifs à une proposition offrant plus de notoriété,
attitude concurrentielle et création de pratiques de rivalité »*. Nam
June Paik a été plus tard *« exclu de Fluxus »* dans les schémas de
Maciunas comme d'autres artistes, et cette position déclencha de
profondes controverses internes. Les membres du groupe commencèrent à
se demander ce que Fluxus était réellement et quelles étaient ses
limites, accusant Maciunas d'appliquer les mêmes pratiques d'expulsion
que Guy Debord dans l'Internationale situationiste ou comme André
Breton avec la création du *critère d'appartenance* au
surréalisme. Malgré ces accusations, dues avant tout à des différences
de position idéologique, Maciunas continua son historisation
méthodique et infatigable de Fluxus et de ses pratiques périphériques.

En 1966, il créa l'*Expanded Art Diagram* décrivant le développement
de l'art jusqu'en 1966: l'année à partir de laquelle l'expérience
Fluxus est généralement considéré comme prenant fin. Le diagramme se
trouve sur le verso du flyer de la *Fluxfest Sale* dont Environ 2000
copies furent tirées. Ce diagramme suit le mêmes orientations que les
précédents, incluant cependant la notion de *Culture Politique*, dont
les origines remontent à l'année 1964, à l'intérieur de la ligne du
temps qui va des iconoclastes Byzantins aux actions des gardes rouges
Chinois, soulignant clairement le point de vue de Maciunas sur sa
perception de l'art comme anti-Bourgeois et populaire (une position
qui le rapproche dans le diagramme de Henry Flynt). Ce point de vue
deviendra de plus en plus prédominant dans les dernières années de son
oeuvre.

Néanmoins, l'oeuvre la plus achevée de Maciunas fut le *Diagram of
Historical Development of Fluxus and other 4 Dimensional, Aural,
Optic, Olfactory, Epithelial and Tactile Art Forms* (1973), qui
mesurait un mètre par trois. Il comprenait toutes les activités de
Fluxus s'étalant de 1948 à 1971, ajoutant même des activités
posthumes. Les diagrammes Fluxus attirèrent l'attention jusqu'en
Italie et Maciunas se remit au travail pour la publication du *Diagram
of Historical Development of Fluxus* de 1973[^11]. Le diagramme ne fut
finalement publié par la maison d'édition Kalejdoskop que en 1979, un
an après sa mort, et à une échelle réduite.

## Le réseau du mail art

Le mail art est un réseau de relations construites autour du circuit
postal, et s'exerce en principe par l'envoi et la réception de de
lettres, de cartes ou de toutes autres choses, tout autour du monde,
établissant des liens *virtuels* entre de nombreux individus,
simplement réunis par l'intérêt de la communication. Le mail art, tout
particulièrement parce qu'il est resté en dehors des circuits
commerciaux artistiques, a toujours été considéré comme *« la
Cendrillon de l'art »*, mais ce fut finalement une chance car jamais
personne ne l'a historicisé et cela a permis la préservation de son
caractère d'originalité et de nouveauté qui le rendent unique et
toujours d'actualité aujourd'hui.

Le mail art est une forme d'art ouvert à tous. Les travaux des
artistes et participants à ses divers projets ont toujours été montrés
sans discrimination liée à une sélection en amont. Il y a un jeu de
mot amusant en italien: *« mail art = mai l’art »*[^42]. Cela montre
comment de nombreux *mail-non-artists* ont délibérément souhaités être
tenus à l'écart de la collecte institutionnelle et des musées, tels
que celleux qui compilent des fragments de mail art dans des archives
sans fin en dehors de toute passion pour le sujet.

Bien que reconnu par peu de personnes, ou plutôt par le peu qui ont
directement expérimenté avec le mail art, cette forme d'art est la
véritable source d'inspiration des *« pratiques en réseau »*.

Ce n'est pas pour rien que le terme, créé par l'artiste-sociologue
français Robert Filliou[^12], qui la définit le mieux est *Eternal
Network*[^43]. Un réseau éternel qui, débutant dans les années
cinquante, a concerné des centaines de personnes et fut constitué
d'enveloppes, de tampons en caoutchouc et de toute une foule d'objets
auto-produits, ou de bouts de papier transformés en créations envoyées
par la poste. Cela impliqua des individus reliés par l'appartenance à
un réseau informel, ne s'y rattachant que par leurs connaissances
internes, du temps où un réseau consistait en un échange d'adresses et
de correspondances en *one-to-one* ou en *one-to-many*.

Le mail art, un processus artistique qui réunit les dimensions
publiques et privées d'une façon harmonieuse, prit forme en un réseau
de petites oeuvres envoyées à quiconque entrait dans le circuit postal
collectif et en même temps, donnait vie à des relations amicales
bi-directionnelles, vécues dans l'intimité de la boîte à lettres.

[//]: # (N.d.T.: Les "appels" du mail art font fortement penser aux "appels à)
[//]: # (projet" de l'art contemporain, sauf qu'ils ont évacué l'aspect simple)
[//]: # (et DIY du mail art et n'en utilisent que la forme pour aboutir à un)
[//]: # (système de jurys, de hiérarchies, de compétitions et de prix)

La plupart du temps, un processus de travail de mail art émerge quand
les *réseauteurs de service* proposent des projets thématiques et
envoient des sortes de *« calls »*[^44] aux autres personnes du
réseau. Ceux qui le souhaitent peuvent accepter l'invitation et
envoyer à leur tour leurs oeuvres de mail art à l'adresse
spécifiée. Une collection collective, qui reste aux côtés de
l'opérateur, est produite à partir de l'appel à projets initial, alors
que les participants sont récompensés par celui-ci à l'aide d'une
petite publication lors de la fin du projet, ou par d'autres formes
d'éditions indépendantes compilant les travaux récoltés.

Pour une brève reconstitution historique, la naissance du mail art est
habituellement placée au début des années soixante, mais possède en
réalité des origines bien ancrées dans les années cinquante. Vittore
Baroni, avec Piermario Ciani[^13], jouèrent un rôle central dans la
diffusion et le développement du mail art en Italie, en prenant en
compte que il n'y a en réalité pas de *vrai* point de départ en ce qui
concerne le mail art, si on considère les possibilités infinies que le
médium postal peut offrir, et la facilité avec laquelle des réseaux
peuvent être constitués simplement en utilisant des enveloppes et des
timbres[^14].

De façon plus commune, l'origine du mail art est reliée à la figure de
Ray Johnson (1927-1995) et à sa *New York Correspondence School*. Le
nom *NYCS* rappelle les nombreuses écoles d'art qui ont fleuri à cette
période sur la côte est. Ray Johnson nous a laissé de nombreux travaux
de mail art: des collections entières de cartes postales, de tampons,
des cartons d'invitation à des évènements qui n'existèrent jamais, des
croquis et de petits dessins, parmi lesquels l'image du mignon petit
lapin avec un nez long et sinueux, qui est devenu un symbole
aujourd'hui. Johnson est décédé en 1995. Il plongea depuis un pont de
New York dans l'eau gelée, habillé sur son trente-et-un, un acte
considéré par de nombreuses personnes comme sa dernière
performance. Il créa une activité de mise en réseau fondamentale, qui
n'a bien sur pas été très reconnue par les critiques officielles quand
il était en vie, excepté lors d´évènements isolés tels que le *New
York Correspondence School Show*, curatorié par Marcia Tucker au
Whitney Museum de New York en 1970.

Après sa mort, une rétrospective majeure sera organisée au Whitney
Museum en 1999, indiquant d'une certaine manière la fin d'une
aventure, qui néanmoins n'a ni début, ni fin, précisément grâce à sa
façon d'être ouverte et *éternelle*. Ray Johnson était et restera, une
figure très importante du mail art et sa notoriété parmi les membres
du réseau le place parmi les principaux défenseurs de l'art en
réseau[^15].

Vittore Baroni cite le futurisme, en soulignant la connexion entre le
mail art et d'autres mouvements artistiques possédant des
caractéristiques créatives communes telles que Giacomo Balla et
Francesco Cangiullo et le dadaïsme, avec les collages de Marcel
Duchamp et Kurt Schwitters. Dans le texte *« The Early Days of Mail
Art »*[^45] de l'anthologie *Eternal Network*, l'artiste Ken Friedman
se rappelle du fameux *Timbre Bleu* de Yves Klein, qui a circulé par
courrier au milieu des années cinquante, déclenchant l'effervescence
dans la bureaucratie de l'époque. Il est certain cependant que la
région dans laquelle le mail art a été la plus influente fut Fluxus,
dans lequel les membres opéraient par contact postal rapproché. La
newsletter de Dick Higgins, connue pour la dissémination des activité
de la maison d'édition de George Maciunas *Something Else Press*, a
amené à la création des *Fluxus diagrams*. Même Ben Vautier, Robert
Watts et Miecko Shiomi furent les partisans d'intéressantes
expérimentations avec les timbres postaux, des tampons et des cartes
postales d'artistes.

En réalité, le champs d'expérimentation du mail art ne s'étend pas
seulement à l'envoi de *« créations »* postales, mais aussi à la
confection de timbres, de tampons, d'autocollants et autres
productions auto-produites. Le principal canal de diffusion italien
pour ces pratiques fut le magazine de mail art *Arte Postale*, édité
par Vitorre Baroni, avec des contributions de centaines de personnes
allant de Ray Johnson à des enfants, présentant des projets musicaux,
des textes, des catalogues, des cartes postales, des tampons postaux,
des timbres, et de la *poésie postale*. Le premier numéro, datant
d'octobre 1979, initie un parcours d'une vingtaine d'années d'une
publication périodique, plus tard suivie par le bulletin *Real
Correspondence*, diffusé par e-mail. Chaque numéro de *Arte Postale*
est différent des autres et possède un thème spécifique, allant
parfois de pair avec un projet d'exposition.

Pendant l'été 2000, le Museo d’arte moderna of Bassano del Grappa
présenta *Sentieri Interrotti. Crisi della rappresentazione e
iconoclastia nelle arti dagli anni ‘50 alla fine del secolo*[^46],
avec une section sur le mail art curatoriée par Vittore
Baroni. L'exposition présentait des travaux et productions de
mouvements tels que cobra, le lettrisme, Gutai, les actionnistes
viennois, Fluxus, la poésie visuelle, etc. Dans la section dédiée à
l'art postal, Vittore Baroni montra quelques projets collectifs créés
entre les années soixante et quatre-vingt-dix ainsi que quelques
publications représentatives de ce domaine, et un échantillon de
travaux individuels sous la forme d'un parcours chronologique allant
des oeuvres de Ray Johnson au mail art en transitant par internet. Un
projet collectif intitulé *« Il tavolo del piccolo iconoclasta »*[^49]
fut conçu pour l'occasion. C'était une longue table blanche
positionnée au centre de l'espace d'exposition dédiée au mail art,
avec une série de différents tampons et tampons encreurs colorés
disposés sur la table et une pile de feuilles de papier possédant des
*motifs de cibles en cercles concentriques* imprimées avec différents
symboles (visages de politiciens, bustes religieux, célébrités de
télévision, artistes célèbres, oeuvres d'art, logos de
multinationales, etc.). Les visiteurs étaient encouragés à utiliser
les tampons en caoutchouc comme des *« armes de fortune »* dans un
acte iconoclaste d'annulation des *« images cibles »* quand ils le
jugeaient approprié, conservant l'oeuvre créée comme une trace de
l'exposition. Ceci, de façon à montrer l'aspect collaboratif et
participatif de l'art postal, une pratique compréhensible et
vérifiable: ouverte-à-tous[^16].

Un aspect important émergeant de cette réflexion est précisément
l'idée du don, de faire de l'art et de le distribuer librement. C'est
la capacité à créer des réseaux horizontaux non basés sur le profit,
mais plutôt sur la notion de création de relations spontanées, par
conséquent centrales au mail art. C'est même la raison pour laquelle
le mail art est toujours vivant aujourd'hui, parce qu'il est basé sur
des mécanismes relationnels faisant partie de notre vie et émotions de
tous les jours, qui s'étendent bien au delà de la mode du moment.[^17]

Dans *« Real Correspondance 6 »* (Vittore Baroni, 1981)[^18], ce
processus est mis en lumière, alors qu'il passe d'une communication
linéaire, inhérente aux circuits de l art traditionnels, à celle de l
art réticulaire du mail art et des autres arts en réseau
successifs. Il est intéressant de noter comment la figure de *network
operator*[^47] est insérée au milieu du circuit et comme elle
interagit au même niveau que le public, qui devient créateur au
travers de l'utilisation de divers médias de masse.

L'oeuvre montre l'opérateur en réseau comme une figure qui colle bien
avec la future figure de l'artiste en réseau[^48] via l'outil
informatique, avec la re-combinaison des différents médias pour la
création d'une plateforme d'interaction et d'actions procédurales
communes. Ce n'est pas par accident que l'idée de *network congres*,
les premières rencontre dédiées à ceux qui travaillent en réseaux en
Italie, qu'ils s'agisse de réseaux postaux ou informatiques, comme
véhicules de communication ouverte (la première a eu lieu en 1992,
dans l'espace *Mu* de Vittorio Veneto), est issue du circuit du mail
art.

Également en Italie, une autre figure du panorama du mail art et de
l'art en réseau est Guglielmo Achille Cavellini (1914-1990), aussi
connu sous le pseudo de *CAG*, une figure centrale dans l'histoire du
mail art, qui a toujours joué avec les codes du système artistique,
créant une auto-historicisation ironique, et en même temps,
ambitieuse. C'est une figure notable pour notre recherche car il
constitue un pont entre les dynamiques artistiques insérées dans le
système commercial et l'art en réseau comme processus dans lequel le
réseau de relations et le partage jouent un rôle
fondamental. Guglielmo Achille Cavellini a été cité dans de nombreux
travaux de Vittore Baroni, qui le définit comme *« une figure centrale
de l'aventure entière du mail art »* (Baroni, 1997). Cavellini
réintègre le culte de l'ego et du génie individuel dans les dynamiques
en réseau, habituellement opposées à toute tentative
d'auto-célébration en faveur des dynamiques collectives et
communautaires. L'effet est simultanément, ironique, paradoxal et
critique.

> *Déjà un des collectionneurs d'art moderne Italien les plus réputés
> et amis des principaux talents de sa génération, il a cultivé
> régulièrement au fur et à mesure des années, une activité picturale
> fervente, mais c'est seulement en 1971 que, se sentant injustement
> ignoré par les critiques, il décida de sortir de l'ombre par lui
> même, en faisant de sa propre personne un objet artistique, une
> série de fantastiques stratégies promotionnelles* (Baroni, 1997,
> p194).

Une activité ardente de productions va suivre cette période: des
catalogues d'exposition *cavelliniennes* qui n'eurent jamais lieu, des
timbres à son effigie, des autocollants et journaux autobiographiques,
des cartes postales promotionnelles, des autoportraits où il figure
avec des artistes reconnus. Des *« Expositions à Domicile »* qui
rappellent celles de l'Avant-garde, envoyées par courrier à des amis
et connaissances du *monde de l'art* et par la suite, à tout les
membres du circuit du mail art, que Cavellini découvrit et transforma
en un véritable vecteur de sa propre auto-historicization, se
préoccupant de répondre à tout·e·s celleux qui lui écrivaient.

[//]: # (Une pratique devenue incroyablement courante et presque obligatoire)
[//]: # (par la suite pour tout artiste, spécialement lorsqu'ils sont *émergents*)

Sa capacité aux interprétations ironiques ou créatives des mécanismes
du business artistique firent de lui un des premiers *culture jammers*
de notre époque. De façon encore plus intéressante, parce que son
refus du système de l'art ne l'a pas forcé à s'en distancier, mais l'a
amené à manipuler son propre langage et stratégies à la première
personne, travaillant à la promotion de son propre ego sans se rendre
anonyme, anticipant ainsi les nombreuses techniques de *marketing
créatif* et la guérilla des médias actuels.

## Identités multiples, néoisme et Luther Blisset

[//]: # (N.d.T.: D'après Wikipédia "Monty Cantsin is a multiple-use name that)
[//]: # (anyone can adopt, but has close ties to Neoism. Monty Cantsin was)
[//]: # (originally conceived as an "open pop star." In a philosophy)
[//]: # (anticipating that of free software "open pop star." In a philosophy)
[//]: # (anticipating that of free software and open source, anyone should)
[//]: # (perform in his name and thus contribute to and participate in his)
[//]: # (fame and achievements". Cette partie n'explicite pas assez le fait)
[//]: # (que Monty Cantsin *n'existe pas* vraiment.)
[//]: # (https://en.wikipedia.org/wiki/Monty_Cantsin)

Fondée en 1979 par *« Monty Cantsin »*, l'identité multiple basée à
Montréal au Canada et à Baltimore aux États-Unis, le néoisme fut
rapidement diffusé partout en Amérique, en Europe et en Australie,
impliquant des dizaines de personnes. Jusqu'aux années quatre-vingt,
le réseau du mail art fut utilisé comme un canal de propagande pour le
néoisme, prenant corps dans le *Neoism Apartment Festival*[^50], en
Amérique du Nord en Europe et en Australie entre 1980 et 1988 à
travers différentes publications. Parmi elles le magazine *Smile*
devrait être retenu, nom sous lequel il commença à être distribué en
1984, donnant naissance à un réseau de personnes qui ont produit et
diffusé leur propre magazine *auto-produit*, contaminant leur magazine
avec ceux des autres et suivant ainsi une sort de *chemin
processionnel*. D'après le [*`tENTATIVELY, a
cONVENIENCE`*](https://www.thing.de/projekte/7:9%23/tent_index.html)
néoiste, *« le néoisme est un préfixe international, plus qu'un suffixe,
avec rien entre les deux. Le néoisme n'existe pas, et c'est une pure
invention de ses ennemis, les anti-néoistes »*[^19].

Le néoisme trouve son expression au travers de pratiques artistiques
et d'expérimentations dans les médias. Il embrasse une philosophie qui
présupose l'utilisation d'identités multiples, l'accumulation de
pseudonymes, la discussion autour de concepts tels que *l'identité* et
*l'originalité*, et la réalisation de *canulars*[^51], de paradoxes,
de plagiats et de contrefaçons, éléments qui reviendront plus tard
dans des mouvements collectifs tels que le Luther Blisset Project
(LBP) et dans les actions de net.artistes, dont les Italiens de
0100101110101101.org.

Le néoisme est connu avant tout au travers des textes de Stewart Home,
qui furent fondamentaux pour sa diffusion, mais aussi, d'après
d'autres néoistes plutôt enclins à ne pas définir et historiciser le
mouvement et à ne pas créer de formes d'appartenances, assez relatifs
à l'expérience personnelle de Home. Dans la définition donnée sur
Wikipédia, l'encyclopédie libre, le néoisme est considéré comme une
sous-culture internationale s'unissant et combattant au moyen de l'art
expérimental[^20] (les courants tels que dada, le surréalisme, Fluxus
et l'art conceptuel), ainsi qu'avec la culture punk, la musique
industrielle, l'electro-pop, les mouvements religieux et les
politiciens libertaires et athées, la science-fiction et le
paranormal, le graffiti et les performances de rue, le mail art, les
cultures gays et lesbiennes, et la première *Church of the
SubGenius*[^52].

Une intéressante et surprenante description du néoisme peut être
trouvée dans le *Manuel Ultime du Néoisme*[^54] sur le site
[`www.neoism.net`](https://web.archive.org/web/20060822212144/http://www.neoism.net/). Le
site fut le résultat d'une expérimentation menée avec la technologie
du *wiki*, permettant aux pages d'être éditées librement et de créer
de nouveaux liens et définitions en fonction des souhaits de chacun,
en concordance avec l'idée néoiste du collectif et de la
contamination. Bien que l'expérimentation du wiki n'ait pas débouché
sur les résultats escomptés, le site présentait toujours du contenu
nouveau, donnant l'impression d'être un projet véritablement en
perpétuel changement, capable de muter tout autant que la personnalité
de Monty Cantsin le permet.

Les symboles-icones du néoisme sont le *fer à vapeur de feu*, le
sèche-linge *antenne* créant un flux télépathique entre les gens, des
coupes de cheveux improvisées pendant des performances, la croix
rouge, un type particulier de nourriture épicée comme le *chili* et
aussi le *chapatti*. Monty Cantsin était non seulement une identité
multiple, mais aussi un mode de vie bien réel pour les nombreuses
personnes qui choisirent d'embrasser le mode de vie néoiste, en
ouvrant des boutiques de vidéo expérimentale, en organisant des
performances, en publiant des (maga)zines, donnant vie à des projets
indépendants.

[//]: # (Les APT fests font parfaitement penser aux rassemblements de)
[//]: # (hackers tels que le)
[//]: # (Chaos Communication Congres <https://fr.wikipedia.org/wiki/Chaos_Communication_Congress>)
[//]: # (qui a lieu tous les ans en Allemagne depuis 1984 ou d'autres hackmeetings)

Le détail qui fait du néoisme un art en réseau devient évident dans la
description de l'Apartment Festival[^55] tel qu'on le retrouve dans la
micro-édition *What is an uh, uh, Apartment Festival*[^56] datant de
1981, nommé *The Apt Project, The Practice of a Common Cause*[^57].

> *Les* APT Festivals *sont des évènements durant généralement une
> semaine, avec des activités diverses telles que des conférences et
> performances, mais la raison d'être de ces* rassemblements amicaux,
> entraînements, ou manoeuvres d'appartement *est de créer une
> situation de rencontre interpersonnelles agréables et simples entre
> les différents collaborat·eu·rice·s intéressé·e·s. Les APT
> fests ne sont NI des festivals de* performance *NI des festivals*
> d'installations *. Les APT fests sont les* fêtes mobiles[^100] *du
> réseau néoiste*.

Ce sont les néoistes qui ont évoqué en 1981 le terme de
*Toile-réseau*[^58], donnant naissance à un discours sur le réseautage
libertaire systématisé basé sur le principe du *Centre de Recherche
Néoiste* (CRN)[^100]. Le *Centre de Recherche Néoiste* est apparu à
Montréal, comme une séquelle de la *conspiration culturelle
Néoiste*. L'idée était de *promouvoir la recherche collaborative entre
différentes recrues à un niveau national et international, avec comme
objectif de partager et de diffuser des idées innovantes et de
développer de nouvelles formes de communication[^21]*.

Nous parlons ici de *Situations Ouvertes*, dans lesquelles les
personnes qui catalysent leur propres énergies, donnent vie à des
séries de collaborations entre les différents membres du réseau. Le
Centre de Recherche Néoiste mît en place l'APT Festival Project en
1979, initiant une série d'évènements d'appartement à Montréal,
Baltimore et Toronto.

Afin de coordonner les évènements collectifs et les différents
projets, les néoistes utilisèrent le Mail art comme principal vecteur
de communication, inventant une *stratégie en toile d'araignée*,
conçue dans la dimension de leur *toile-réseau*. Parmi les nombreux
exemples de Mail art créé dans le circuit néoiste, nous avons le *DATA
project*[^22] de Pete Horobin et évidemment les activités de Vittore
Baroni, qui était en contact direct avec le réseau néoiste.

Les pratiques du plagiat des années quatre-vingts dont le néoisme
était une des plus grandes forces directrices, ont convergé lors du
*Plagiarism Festival*. Le premier *Festival du Plagiat* a eu lieu à
Londres en 1988 organisé par Steward Home et Graham Hardwood. Steward
Home l'organisa, une fois seulement après avoir pris ses distances
avec le néoisme. D'un côté, s'ouvrant à un spectre de participants
plus étendu (des mail artists aux différents personnages actifs dans
les cercles underground), et de l'autre côté, en se limitant aux
pratiques alors théorisées. John Berndt s'exprime en termes critiques
à propos du Festival du Plagiat:

> *[les Festivals du Plagiat] ont émergé comme une émanation des*
> Neoist Apartment Festivals *, évènements qui étaient eux même des
> produits d'un plagiat des Fluxusfestivals s'étant tenus quelques
> annés auparavant. Quoi qu'il en soit, [les Festivals du Plagiat]
> embrassèrent un champs d'activité plus large et pas pas un thème
> unique (tel que* l'originalité de la propriété intellectuelle
> artistique *, qui était la thématique du premier festival à Londres
> en 1988)[^23].

Le dernier Festival du plagiat s'est tenu à Glasgow pendant la semaine
du quatre au onze août 1989, et fut organisé par Stewart Home et Billy
Clark, un des membres fondateurs de la galerie underground
*Transmission* - désormais bien connue. Les participants étaient Lloyd
Dunn (des TapeBeatles), Matthew Fuller, Klaus Maeck et Walter Hartmann
(auteur du *Decoder Hanbuch*, 1984, qui accompagnait le film allemand
*Decoder*, écrit et produit par Maeck[^24]), Tom Vague, Jamie Reid,
Florian Cramer et les Italiens Raf Valvola et Gomma du magazine
*Decoder/Shake Edizioni* (qui tire son nom du film de Klaus Maek).

Ce dernier décrit l'évènement dans le cinquième numéro du magazine
*Decoder* (1990):

> *La manifestation était organisée selon une conception radicale, une
> théorie de base de* l'artiste *et de l'art en général. L'hypothèse
> de base est que l'artiste ne devait pas être vu comme un génie
> unique. Il s'agit d'une vision d'origine romantique qui est
> inappropriée à témoigner de la situation actuelle. En opposition à
> cette idée et vision* bourgeoise *de l'art, la proposition
> officielle controversée consistait à interpréter selon la pratique
> du plagiarisme, par le clonage et le détournement du sens, comme
> seule alternative possible. C'était en réalité la pratique d'une
> centaine d'artistes tout autour du monde, qui signaient leur travail
> avec un seul et même nom (Karen Eliot). Adhérant à cette analyse que
> le projet proclama durant le festival, consistant à mettre en oeuvre
> et à propager une* grève de l'art *, une manifestation regroupant
> depuis 1993 n'importe quelle forme de pratique artistique et*
> d'objectification *de façon générale[^25]*.

Le terme *« plagiarisme »* était une re-élaboration opérée par Stewart
Home du concept situationiste de *« détournement »*. Guy Debord avait
inventé le terme *détournement* dans les pages de l'Internationale
situationiste, en effectuant une hybridation du concept de plagiat de
Lautréamont et de *« l'effet de distanciation »*
(*« Verfremdungseffekt »*) de Brecht (cf Cramer, 2006). Par
détournement, on entend l'acte de subvertir des formes de
communication courantes – allant d'affiches publicitaires à des
extraits de films – s'en appropriant les signes linguistiques pour
créer une *« réécriture sémiotique »*, en les isolant et les insérant
dans un contexte différent, démasquant la fiction de la communication
pour observer la réalité avec un oeil nouveau. Les pratiques
situationistes puisent leur sources dans le lettrisme, dans le Groupe
Cobra et dans le Mouvement International pour un Bauhaus Imaginiste
(*MIBI*).

La critique radicale de l'art théorisé est le point central de
l'Internationale situationiste (1957-1972), avec la contestation des
valeurs traditionnelles bourgeoises, la subversion et le boulversement
des valeurs culturelles traditionnelles, de son train de vie quotidien
médiocre, la transformation de l'architecture en anti-architecture, le
potentiel révolutionaire du *temps libre*, la mise en place de
*« situations »* définies comme des *« moments de vie, concrètement et
délibérément construits par l'organisation collective d'une ambiance
unitaire et d'un jeu d'évènements »*[^68]. Une de ces pratiques est la
*« psycho-géographie »*, une exploration sensible des territoires par
le moyen de la *« dérive »*[^26]. L'idée de *psycho-géographie* et de
*dérive* est directement tirée de la proposition d'*« architecture
infinitésimale »* de Isidore Isou, qui proposa de substituer les
éléments de construction spatiale par ceux correspondant au ressenti
et à la volonté de ceux vivant sur place.

> *La psycho-géographie possède la fonction d'un exercice quotidien
> spontané et ludique avec la* dérive *, une promenade libre sans
> itinéraire prédéfini (rappelant la* flânerie *surréaliste), réalisée
> dans une nouvelle dimension humaine, fondée sur la libération des
> désirs et sur l'exploration de la subjectivité, par opposition avec
> le conditionnement des besoins consuméristes.*[^69]

Les situationistes sont généralement cités comme source d'influence
philosophique de différentes pratiques radicales ultérieures: il est
cependant nécessaire de préciser que c'est un point de vue qui a été
nuancé au fur et à mesure des années, avec une suppression de
l'empreinte dogmatico-théorique qui a caractérisé une partie de
l'histoire de l'Internationale situationiste, pour faire place à
divers concepts libertaires et à de multiples formes d'action.

Au coeur de cette discussion, une autre pratique collective de
*singularité multiple* s'associe naturellement à l'*identité
multiple*[^70] de Luther Blissett. Un fil de transmission relie
ensemble les trois identités de Monty Cantsin, Karen Eliot et Luther
Blisset, bien que certaines différences existent[^27]. Monty Cantsin a
été créé comme une *open pop star* et une identité multiple réelle et
vivante. Le nom avait été proposé à Istvan Kantor et Maris Kundzins
par David Zack, mais seul Istvan Kantor l'a concrètement adopté dans
son parcours artistique et performatif. Parce qu'il était possible de
l'associer avec des personnes *en chair et en os*, en risquant souvent
d'être identifié à Istvan Kantor, le nom fut utilisé comme une
identité collective seulement pendant la première époque du néoisme.

De nombreux néoistes sont en réalité connus sous d'autres pseudonymes
tels que tENTATIVELY a cONVENIENCE, Kiki Bonbon, Reinhardt U. Sevol,
etc.  Le nom Karen Eliot fut suggéré par Stewart Home comme un nom
multiple et anonyme, dans le but d'être utilisé comme une signature
collective pour des oeuvres d'art ou des écrits, et ceci afin d'aller
contre l'idée de l'individualité d'auteur. Il n'a pas été créé dans le
but d'être associé avec des personnes physiques, même si au fil du
temps il fut toujours lié à Stewart Home. Luther Blissett est né comme
un fantôme des médias, une figure mythologique à laquelle une
tradition encore plus mythique attacha les noms légendaires de Harry
Kipper et Coleman Healy. Une figure mystérieuse, une identité assez
différente d'un pseudonyme, qui n'avait pas été crée pour être
divulguée au public, un peu comme Kaiser Soze dans le film *The Usual
Suspect*[^28].  Luther Blissett n'est pas n'importe qui, et en même
temps, n'importe qui peut être Luther Blissett. Il est né comme un
mythe urbain, avec pour objectif de saboter les centres de contrôle du
pouvoir au travers d'activités de déstabilisation culturelle,
s'introduisant dans les infrastructures médiatiques comme un vers dans
la pomme, rongeant tout ce qu'il pouvait de l'intérieur, en laissant
uniquement la peau intacte. Luther Blissett est né de l'action
conjointe de personnalités actives issues de mondes différents, venant
du Mail art ou du néoisme en allant jusqu'à des pratiques radicales
externes, suivant le sillage des identités multiples de Monty Cantsin
et Karen Elliot, mais se distanciant de l'environnement artistique
afin de potentiellement s'ouvrir à tous. Çà n'a pas de sens, à mon
avis, de lister par écrit les noms des personnes qui ont eu l'idée de
lancer ce projet étant donné que, entre autres, la signification du
Luther Blissett Project (LBP) était d'aller au delà de l'attitude
d'appartenance individualiste.

Le nom Luther Blissett est un dérivé du nom de l'ancien footballeur
Jamaïcain qui joua en tant qu'avant-centre dans les équipes de Watford
et de Milan. Parmi les différentes sources d'inspiration pour ce nom
on retrouve un projet en réseau d'un mail artiste Anglais qui avait
diffusé des compositions et des collages d'un joueur de football, dont
Luther Blissett. Une heureuse union sonore entre *looter*[^59] et
*bliss*[^60].  Quand on lui demande: *« Qu'est-il nécessaire de faire
pour devenir un* sharer[^61] *? »*, Luther Blissett, répond:

> *Il ne suffit que de renoncer à sa propre identité, avec tous les
> avantages que cela comporte. Plonger dans la vague des émotions de
> colère et de joie que tu sens passer autour de toi, re-élaborer ta
> signature sans laisser d'empreinte. Car ceux qui sont comme toi ne
> savent que faire avec une oeuvre signée: c'est une chose finie, dont
> tu as toi-même décrété l'achèvement, et à laquelle personne ne
> pourra rien ajouter de nouveau. La* non-identité *des* sharers *va
> main dans la main avec l'incomplétude* (Blissett, 1996, p. 19).

Initialement apparu en 1994, le Luther Blisset Project s'est propagé
de l'Italie au Royaume Uni, aux États Unis, en Hollande, en Allemagne,
en Autriche, en Finlande et en Hongrie. Comme un véritable virus, le
LBP débarqua pour déclencher une série de canulars médiatiques aux
profonds impacts, conjointement à des *happenings*, des spectacles et
des performances dans le métro, des articles dans des publications, des
actions de dépassement de l'art en faveur de la vie quotidienne,
devenant un des projets les plus actifs de cette période.

En Italie, le LBP atteint son pic de diffusion entre 1994 et 1999,
donnant vie à diverses expérimentations créatives et canulars
médiatiques entre Bologne et Rome. Les composantes libertaires du LBP,
unies dans la volonté de créer un réseau de nombreux *multi-vidus* et
en subvertissant les catégories rigides (incluant *« la politique »*,
*« le spectacle »*, *« la communication médiatique »*) a trouvé un
terrain fertile dans notre pays[^62], dans lequel les individus ont
tendance à s'exprimer au travers d'images et d'identités individuelles
fixes telles que le travail, la famille, la maison, la politique, etc.

Parmi ces divers canulars, il y a celui où Luther Blissett prend part
à l'émission *Chi l'ha visto?*[^71] dans laquelle des
enquêteurs sont à la recherche d'un artiste fictif nommé *Harry
Kipper*. Luther Blisset dit que l'artiste a disparu à la frontière
Italo-Yougoslave lors d'un voyage à travers l'Europe en VTT, pendant
lequel l'artiste avait l'intention de tracer le mot *art* sur la carte
du continent. À partir de cette situation, un jeu paradoxal commence,
dans lequel les membres de l'équipe de télévision poursuivent les
(fausses) traces de la personne disparue et, juste avant que
l'émission ne soit diffusée, découvrent que l'identité de celui qu'ils
recherchaient étaient celle d'un fantôme.

Un autre canular fameux est la création du faux livre annoncé par
l'éditeur Mondadori, appelé *« net.gener@tion, manifesto delle nuove
libertà »*[^63], signé par Luther Blisset et publié par l'éditeur
Milanais en 1996. Tout parut normal dans un premier temps, mais en
fouillant un peu plus, il fut découvert que le contenu du livre était
fabriqué de toutes pièces, à l'aide de fausses adresses internet. Le
livre fut bien sur retiré du marché très rapidement, aussi vite qu'il
suffit à l'éditeur pour se rendre compte du canular (après que le
livre ait été mis en circulation tout de même...).

Une action qui est restée historique pour celles et ceux qui faisaient
partie de ce cercle à cette époque fut le happening
psycho-géographique *Neoist Bus* qui eut lieu en 1995 dans le bus de
nuit nº30 à Rome. Durant ce *happening*, le bus était détourné pour
répandre une fête néoiste au travers des rues de la ville,
subvertissant la monotonie d'un moyen de transport public. Les
personnes investies disposaient d'un flux en live par téléphone depuis
la radio Città Futura[^64], sur laquelle l'émission *Radio Blissett*
était diffusée, afin de créer une action hypermédiatique qui
connecterait les voitures, les bus, les téléphones publics ou privés,
les radios et les ordinateurs.

Le bus, *corps mobile de la métropole*, voyageait au travers des rues
de Rome rempli de passagers inhabituels, portant tous le même nom:
*Luther Blisset*. L'ironie de la situation ne fut évidemment pas
comprise par la police, qui intervient avec l'empressement requis
pour ce genre d'occasion, tirant en l'air en mode shérif avec leurs
armes, incarnant le plus pur style de film de western, et incarcérant
dix-huit *« des occupants du bus »* qui refusaient de montrer leurs
papiers et revendiquaient tous s'appeler Luther Blisset! Cet évènement
devint un véritable mythe pour l'underground italien, le transformant
en une légende soutenue par divers *« croyants »*, qui, ne
s'inquiètent souvent pas trop de vérifier précautionneusement de tels
faits. Dans de nombreux articles et publications sur le sujet, on lit
que l'épisode eut lieu dans un train, et culminant dans une affaire au
tribunal au mois de mars de l'année suivante, avec des charges
d'insurrection, de refus d'obtempérer et d'insultes à agents dans
lesquels pour tous les accusés assignés à comparaître, on pouvait
lire: *« Blisset Luther né le XX.XX.19XX à XXXX, résidant rue de
XXXX »*[^30].

En réalité, sur le site de la Fondation WuMing, il est souligné que
cet épisode eut lieu dans le bus de nuit. Tel que publié sur le site:

> *Ce n'était pas un train, c'était un bus de nuit. Ça s'est déroulé
> le 17 juin 1995. Quelques dizaines de* ravers *occupèrent et*
> détournèrent *un bus de nuit. Une rave party prit place dans le
> véhicule jusqu'à ce que la police décide de bloquer la rue et d'y
> mettre un terme. Lorsque les ravers sortirent du bus, les policiers
> les attaquèrent, un d'entre eux tirant même trois coups de feu en
> l'air. Un journaliste d'une radio indépendante (Radio Cittaà Futura)
> était également dans le bus, couvrant l'évènement en direct par
> téléphone pour une émission lorsque les détonations furent entendues
> par des milliers d'auditeurs. Dix huit personnes furent
> arrêtées. Certaines dirent qu'elles étaient *« Luther Blissett »*,
> mais aucune cependant ne revendiqua celà au poste de police plus
> tard. Les média couvrirent l'évènement de façon extensive, ce qui
> démontra à quel point le nom *« Luther Blissett »* pénétrait les
> sous-cultures de la jeunesse. Nous n'avons aucune idée de comment
> cette histoire s'est transformée en fable à propos de
> quatre-personnes-dans-un-train. Des années plus tard, cela refait
> toujours surface par ci et par là. On a trouvé çà marrant un moment,
> maintenant c'est juste, comment dire... Flippant.*[^31]

Le canular le plus complexe prit place à Lazio en 1997 lors de la
diffusion d'actualités à propos de la zone de Viterbo, où eurent lieu
des messes noires et des rituels sataniques, documentés par des traces
physiques, textuelles et audiovisuelles laissées par les supposés
*adeptes du démon*. Le canular dura un an, générant une totale *« paranoïa de
masse »* (Wu Ming, 2006), à tel point que l'émission de
télévision *Studio Aperto* de la première chaîne italienne consacra un
épisode à l'évènement. Comme toujours, *Luther Blissett* révèle
*l'arrière des coulisses* de l'affaire, démontrant comment la
soi-disant *légende urbaine* est née en révélant les mécanismes par
lesquels les actualités *sensationnalistes* sont diffusées au travers
des circuits d'information[^32].

L'histoire et la documentation du Luther Blissett Project (LBP) peut
être consultée sur le site internet
[`www.lutherblissett.net`](http://www.lutherblissett.net/), site
décrivant les activités de Luther Blissett et les coupures de presse
des affaires les plus célèbres. Les *vétérans* du Luther Blissett
Project proclamèrent leur suicide rituel en décembre 1999, un
*seppuku* collectif mettant terme à cinq ans d'expérimentations, et
donnant naissance à d'autres projets[^33]. Parmi ceux-ci, les plus
connus sont le collectif Wu Ming et le duo 0100101110101101.org. En
dépit du *seppuku*, le pseudonyme Luther Blissett restera en vie via
diverses expériences internationales.

Durant l'intervalle de temps 1996-1998, quatre membres du LBP de
Bologne rédigèrent le roman *Q*, sous le nom de Luther Blissett. Il fut
publié par Einaudi en 1999 en licence Copyleft et des traductions
furent distribuées dans bien d'autres pays. En même temps, les quatre
écrivains du roman révélèrent leurs noms rééls au cours d'une
interview avec le quotidien *La Republica*, revendiquant de
représenter moins de 0,04% du Luther Blissett Project. En janvier 2000,
après l'arrivée d'une personne supplémentaire dans le collectif
d'écriture Blissettien, l'expérience Wu Ming débutta. Wu Ming signifie
*anonyme* en Chinois Mandarin, comme indiqué sur le site
[`www.wumingfoundation.com`](https://www.wumingfoundation.com/).

Poursuivant la tradition du LBP, le livre fut écrit par cinq membres
différents dont les noms ont été rendus publics (Roberto Bui, Giovanni
Cattabriga, Luca di Meo, Federico Guglielmi et Riccardo Pedrini), mais
préférant être appelés Wu Ming 1, 2, 3, 4, 5 (les chiffres étant
déterminés par l'ordre alphabétique de leurs noms de famille). Sur le
site de Wu Ming, on peut lire:

> *Le nom du groupe s'entend comme un hommage à la dissidence et au
> refus du status d'Auteur en tant que star. L'identité des cinq
> membres de Wu Ming n'est pas tenue secrête, mais nous pensons que
> nos travaux sont plus importants que nos biographies ou visages
> individuels.*

Wu Ming a produit différents romans de grande ampleur, tous étant le
résultat de recherches historiques approfondies. Parmi eux, nous avons
déjà mentionné *Q*[^65] (1999), une oeuvre non-romanesque prenant
place au seizième siècle, et présenté comme un western théologique
dont le protagoniste est un espion engagé par le Cardinal Carafa,
*Asce di Guerra*[^66] (2000), *54* (2002), et d'autres travaux pour
lesquels il n'y a qu'une seule signature Wu Ming[^34]. Les romans de
Wu Ming sont *« open source »* et peuvent être téléchargés depuis leur
site internet. Wu Ming publie également périodiquement deux
newsletters. *Giap* (2000) et *Nandroposa* (2001)[^35], au travers
desquelles l'on peut s'informer à propos des nouvelles publications et
lire des commentaires variés à propos de différents sujets, écrits par
les membres de Wu Ming. Une autre production de Wu Ming est le
scénario écrit en collaboration avec Guido Chiesa qui a dirigé
*Lavorare con lentezza*[^67] (Fandango, 2004). Le film, à la frontière
entre le documentaire et la fiction, est centré sur Radio Alice et le
mouvement de Bologne durant les années 1970, parcourant l'histoire,
les idéaux et les pratiques qui ont inspiré une part majeure du
phénomène italien qui inspirera le futur activisme politique. Il
suffit de lire le livre *Alice è il diavolo*[^72] (1976, reimprimé par
Shake en 2002) qui explore l'histoire de la Radio Alice à Bologne et
les activités du collectif A/traverso, pour comprendre à quel point
les esthétiques et pratiques de tant de mouvements italiens actuels
puisent leurs origines dans les années 70, durant une période de radio
libre, où la politique était vécue radicalement, une lutte qui se
solda pour de nombreuses personnes par de la prison. Ces combats sont
aussi représentés dans le film *Alice è in Paradiso*[^73] (2002),
produit par Fandango à nouveau, et revisitant les aventures d'un
véritable collectif de hackers des réseaux de télécommunications, où
l'ordinateur est remplacé par la radio. Pour conclure la discussion
sur les identitées multiples et les mouvements anti-artistiques, en
les unifiant avec ceux des utopies anarchistes des années 70, les
évènements de Monte Capanno qui ont eu lieu en 1970 à Umbria seront
décrits ici brièvement. Très peu de personnes savent qu'un groupe de
30 étudiants Américains du San Jose State College (Californie) menés
par David Zak, un des créateurs officiels de l'identité multiple Monty
Cantsin, ont porté un collectif et auto-géré une expérimentation
éducative de Janvier à Juin 1970.

Une expérimentation composée d'un programme d'études intense, avec des
séminaires organisés par les étudiants eux même et une coordination de
David Zack, où iels lisaient des livres, écrivaient, développaient
des actions artistiques, et se focalisaient sur des concepts sociaux
et des politiques alternatives, dont la documentation a été un temps
accessible à l'adresse
[`http://montecapanno1970.com/`](https://web.archive.org/web/20130614001039/http://montecapanno1970.com/). Une
première tentative d'émergence d'une *Communauté d'Apprentissage*
appelée *Monte Capanno*, ce qui rappelle pas mal *Monty Cantsin*...

[^1]: Parmis eux, la maison d' édition *AAA Edizioni* de Vittore Baroni et Piermario Ciani, dont l' activité est décrite dans le paragraphe de ce chapitre consacré au *Mail art*.


[^2]: Pour en savoir plus à propos de l'influence de Albrecht Dürer en Italie, consulter le site de la Triennale Europea dell’Incisione à l'adresse `http://karaartservers.ch/udine/durer/durer.i.html`.


[^3]: Marco Deseriis, Giuseppe Marano, *Net.art, l’arte della connessione*, Milan, Shake Edizioni, 2003.


[^4]: Pour une biographie détaillée et une analyse du travail de Giuseppe Chiari, nous recommandons le paragraphe qui peut être trouvé à l'adresse `www.wikiartpedia.org/index.php/Chiari_Giuseppe` sur *WikiArtPedia La Libera Enciclopedia dell’Arte e le Culture delle Reti Telematiche*, idealisé par Tommaso Tozzi.


[^5]: À propos du processus d'affirmation de la foule urbaine comme un sujet social, cf. Alberto Abruzzese, *Lo splendore della TV*, Genova, Costa & Nolan, 1995, Geneve, Costa & Nolan, 1995. Dans le travail de Abruzzese, la splendeur des images inhérente au langage télévisuel, commençant avec les premières manifestations de la civilisation urbaine, en se référant au paradigme selon lequel, en partant de la spectacularisation de la place de marché, traversant les non-lieux des premières Expositions Universelles, de la foule urbaine aux premiers voyages en train à vapeur, en touchant aux simulacres fantasmagoriques des cartes postales, des photographies, du monde de la marchandise, de l'univers radiophonique, cinématographique jusqu'aux réseaux de télévision.


[^6]: Samuel Taylor Coleridge (1772-1834), poête, philosophe et critique, est considéré comme un des fondateurs du romantisme britannique. Nous avons lu à propos du concept d'*intermedia* utilisé par Coleridge dans le texte écrit par Dick Higgins *Synesthesia and Intersenses: Intermedia*, initialement publié en 1965 dans *Something Else Newsletter 1, No. 1* (Something Else Press, 1966) et plus récemment sur [`UbuWeb`](http://www.ubu.com/), un site spécialisé dans l'art d'Avant-garde. Higgins affirm que Coleridge utilisa le terme *intermedia* en 1812: *« Le terme que j'ai choisi, le mot *intermedia*, apparaît dans les écrits de Samuel Taylor Coleridge en 1812, exactement sous son sens contemporain – pour définir des oeuvres qui tombent conceptuellement entre des media qui sont déjà identifiés comme tels, et j'ai utilisé le terme depuis plusieurs années durant des conférences et des débats, avant que mon petit essai soit rédigé »*


[^7]: Par la suite, même Ken Friedman, un artiste associé à Fluxus, travailla sur le catalogage des différents *fluxévènements*, en ajoutant même des happenings posthumes, dont parmi eux les *52 Events*. Sa dernière publication est le *FluxusPerformanceWorkBook* (2002), faisant suite à la précédente édition de 1990; éditée avec Owen Smith et Lauren Sawchyn. Ce e-book, qui peut être téléchargé librement en PDF, contient de nombreuses partitions de *fluxévènements* et de *fluxconcerts*, ainsi que des travaux posthumes d'autres artistes qui étaient associés à Fluxus pour leurs convergences avec ce mouvement. L'interprétation de ce mouvement par Ken Friedman joue un rôle important (même si elle est personnelle), et la publication contient des textes auxquels il est normalement dificile d'accéder. Il peut être téléchargé à l'adresse `www.thecentreofattention.org/artists/Fluxus.pdf`. Plus d'informations sur le travail de Friedman seront trouvés sur le site *The Centre of Attention* maintenu par Pierre Coinde et Gary Gary O’Dwyer, à l'adresse `www.thecentreofattention.org`.

[^8]: En 2005, en Allemagne, un catalogue, *Maciunas' *« Learning Machines »*, From Art History to a Chronology of Fluxus* était publié suite à l'exposition qui s'était tenue à la Kunstbibliothek de Berlin, de Octobre 2003 à Janvier 2004 (Schmidt-Burkhardt A. 2005). Il s'agit d'une publication fondamentale pour saisir l'étendue de Fluxus au travers des diagrammes créés par George Maciunas.

[^9]: Cf. Owen F. Smith, *Fluxus. The History of an Attitude*, San Diego, 1998.

[^10]: La liste complète des *exclus* peut être retrouvée dans le diagramme publié par Georges Maciunas en 1966, appelé *Fluxus (Ses développements historiques et Relations avec les Mouvements d'Avant-garde)* et est publiée dans l'ouvrage de Astrit Schmidt-Burkhardt, *Maciunas’ Learning Machines, From Art History to a Chronology of Fluxus*, The Gilbert and Lila Silverman Fluxus Collection, Vive Versa Verlag, Berlin, 2005.

[^11]: Dans le catalogue d'exposition, on peut lire: *Maciunas’ Learning Machines, From Art History to a Chronology of Fluxus*, qui s'est tenue à Kunstbibliothek de Berlin, organisée par la maison d'édition Milanaise de Gino Di Maggio *Multhipla*, qui a offert d'organiser la distribution d'une centaine de copies du diagramme.


[^12]: Un terme qui est est fourni par une des plus importantes publications sur le mail art, *Eternal Network, A Mail Art Antology*, édité par Chuck Welch, University of Calgary Press, 1995.


[^13]:Piermario Ciani, né en 1951 à Bertiolo (Udine) et est disparu du jour au lendemain le 2 juillet 2006. Comme cela a été posté sur les site des AAA Edizioni, il fut un *« étudiant de Luther Blisset à l'École d'Art Psych-géographique de Londres. Il produisit des images utilisant des méthodes manuelles, photochimiques, électrostatiques et numériques. Il publia et montra ses travaux à partir de 1976: tout d'abord des peintures, puis des photographies, des xerographies, du mail art, des installations multimedia et de scollages. Il préfère les media papier mais ses travaux étant souvent immatériels, il sont percus comme se situant aux frontières de la télépathie et des transmissions de données »*, traduit depuis un extrait de `www.aaa-edizioni.it/Ciani.htm`


[^14]: En 1996, Vittorre Baroni et Piermario Ciani ont fondé la maison d'edition *AAA Edizioni*, fondamentale à la reconstruction de l'idée de *réseautage* en Italie. Ils offrirent de fascinantes publication à l'univers de la contre-culture internationale, allant du mail art et à la culture Internet, de Fluxus, au Néoïsme, aux situationistes, et à touts les phénomènes iconoclastes et anti-art des dernières décades. Parmis les nombreux travaux publiés par les *AAA Edizioni*, on retrouve *Arte Postale* [Art Postal, N.d.T.] (1997), *L'arte del timbro* [L'Art du Timbre, N.d.T.] (1999), et *Artistamps (2000), des textes qui sont des clés de compréhension pour le phénomène du mail art et qui le reconnectent avec bien d'autres pratiques antérieures. L'ouvrage le plus récent de Vittore Baroni est *Post-cards, Cartoline d'artista* [Post-cards, Cartes Postales d'Artistes, N.d.T.] (2006), publié par Coniglio Editore, Rome. Le site de *AAA Edizioni* est `www.aaa-edizioni.it`

[^15]: Une intéressante reconstruction du mail art, écrite par Vittore Baroni, se trouve dans le premier numéro de l'e-zine indépendant *Ah!*, sous la direction de Bruni Capatti (*net director*) et Attilio Fortini (*webmaster*). Le magazine en ligne *Ah!* publia malheureusement seulement deux numéros (Septembre 2000 et Mars 2001), et peuvent être trouvé à l'adresse `http://digilander.libero.it/wwwart/AH/`. L'introduction éditoriale de la sortie du magazine révèle un esprit simultanément critique et ironique. Un interviewer demandant à Marcel Duchamp, à propos de sa célèbre peinture *Nu descendant l'escalier*, *« si elle représentait une femme ou un homme. Pour moi? Pour toi? Pour obtenir une citation, le Maître dit que il n'y avait jamais pensé. En effet, qu'est ce que cela aurait pu changer... Quelle vérité il y a-t-il en une vérité qui n'est pas apparente? Vouloir réduire une personne, à tout prix, à sa connaissance, est plus un besoin de contrôle que un besoin de compréhension. Le reste a le résultat escompté, mais seulement pour ceux qui se satisfassent de vivre. Nous n'avons rien d'autre à offrir que le désir de...AH! Ne veux tu pas te simplifier les choses? Il suffit juste d'un petit geste de survie... ce n'est rien; précisément peut-être parce que c'est indéniablement nécessaire »* . Cité depuis `http://digilander.libero.it/wwwart/AH/ah!/AH!html`


[^16]: Vittore Baroni, tel que cité par le bulletin du mail art *Real Corrispondence*, en Septembre 1999. Le bulletin, distribué gratuitement par courrier postal (et par la suite par e-mail), fut créé en 1980.


[^17]: En 2001, Vittore Baroni et Oiermaria Ciano ont lancé le projet *F.U.N, Funtastic United Nations*, offrant un passage reliant des entités géographiques créatives et imaginatives. Les projets développés ont donné naissance à des rencontres, des projets de collecte individuelle ou collective de timbres dédiés aux personnes et évènements liés aux *Funtastic nations* et à l'édition de billets produits par les citoyens du F.U.N. et collectés dans le projet *Bank of FUN* (2003).


[^18]: Comme Vitorre Baroni me l'a expliqué dans une correspondance personnelle, *Real Corrispondence 6*, en 1981, était un fragment d'une série de flyers appelés *Real Corrispondence*. Le schéma fu re-imprimé par Ragged Edge Press en 1990, sur une carte postale intitulée *The Evolution of Art*, sur laquelle l'image était insérée dans un cadre délimité comme un timbre postal.


[^19]: Cf. l'introduction par Simonetta Fadda dans l'ouvrage de Stewart Home *Neoismo e altri scritti. Idee critiche sull’avanguardia contemporanea* [*Néoisme et autres écrits. Pensées critiques de l'Avant-garde contemporaine*, N.d.T.], Genova, Costa & Nolan, 1995.


[^20]: La définition du néoisme est disponible en ligne à l'adresse `https://en.wikipedia.org/wiki/Neoism`.


[^21]: Cité depuis le livret *Centre de recherches Néoistes*, 1981.


[^22]: Plus de lecture à `http://www.thing.de/projekte/7:9%23/horobin_index_index.html`.


[^23]: Cité depuis `/www.thing.de/projekte/7:9%23/y_Proletarian_Posturing.html`, qui publie un essai critique de John Berndt sur le festival du plagiarisme de Londres.


[^24]: Klaus Maeck est une figure reconnue de la culture post-punk germanique des années 80. Il a organisé un label de musique indépendante et écrit le livre documentant officiellement la vie du groupe Einstürzende Neubauten (*Hör mit Schmerzen*, 1996). On se rappelle de lui avant tout pour avoir écrit et produit le film *Decoder* (1984). Le réalisateur et co-scénariste du film fut Muscha (Jürgen Muschalek), un réalisateur de Düsseldorf qui sortit divers films punk en Super-8. Le troisième co-scénariste était Trini Timpop, un musicien Punk de Düsseldorf qui était aussi le batteur principal du fameux groupe Die Toten Hosen. Parmi les nombreuses autres personnes participantes en tant qu'acteurs, on retrouve William S. Burroughs, Genesis P'Orridge, FM Einheit de Einstürzende Neubauten et Alexander Hacker. La publication *Decoder Handbuch* [*le Manuel du Decoder*, N.d.T.] (1984), qui accompagnait le film, faisait figurer des écrits de William S. Burroughs et Brion Gysin, Jean Beaudrillard, Genesis P'Orridge, Klaus Maeck entre autres.


[^25]: Un rapport du festival, écrit à Tozzi en 1991 par le collectif Decoder *Decoder on tour – Festival on Plagiarism – Glasgow 4-11 August 1989*, p.203, cité dans *Decoder*, 5, 1990, Milano.


[^26]: Guy Debord et Asger Jorn en particulier ont appliqué le concept de psycho-géographie à la pratique du détournement, en réalisant notamment l'ouvrage *Fin de Copenhague* en 1957, une publication dont fut imprimée 200 exemplaires, faite de collages d'articles et d'illustrations découpés dans les quotidiens de Copenhague, et de *dripping* par Asger Jorn, reformulant la représentation visuelle de la vie urbaine, en la transformant en une carte émotionnelle. Une description importante de la collaboration entre Guy Debord et Asger Jorn peut être trouvée dans l'ouvrage en anglais par Christian Nolle, *Books of Warfare: The Collaboration between Guy Debord & Asger Jorn from 1957-1959*, publié par Vector `http://virose.pt/vector/b_13/nolle.html`.


[^27]: Ces considérations sur les trois identités collectives sont le fruit d'un échange de mails avec Florian Cramer, actif dans le cercle néoiste, ainsi que dans les expériences postérieures de Luther Blissett avec l'art logiciel.


[^28]: Tel que Roberto Bui l'a affirmé dans une conversation privée avec Florian Cramer


[^29]: Considérations extraites de *Rendez-vous coi ribelli: Intervista a Coleman Healy*, une interview de Coleman Healy par Luther Blissett, disponible en ligne à l'adresse `www.lutherblissett.net/archive/215-04_it.html`.


[^30]: Tel que décrit sur `http://www.lutherblissett.net/archive/228_it.html`.



[^31]: Extrait de *The Night Luther Blissett Hijacked a Bus in Rome* [*La Nuit où Luther Blissett Détourna un Bus à Rome* en français, N.d.T.], disponible sur [`https://tinyurl.com/ybrhyh6s`](https://web.archive.org/web/20150507054306/http://www.wumingfoundation.com/english/biography.html#nightbus), Juillet 2007.


[^32]: Sur le site de la Wu Ming Foundation, il est également dit que le canular était même révélé dans le quotidien *la Repubblica*, dans un article reconstruisant l'affaire en détails. L'article, *La beffa firmata Luther Blissett*, écrit par Loredana Lipperini, peut être consulté en ligne à l'adresse [`http://www.repubblica.it/online/sessi_stili/blissett/blissett/blissett.html`](https://web.archive.org/web/20171112215026/http://www.repubblica.it/online/sessi_stili/blissett/blissett/blissett.html).


[^33]: Pour en apprendre plus sur Seppuku, le suicide collectif de Luther Blissett, lire [`http://www.lutherblissett.net/#%20indexes/thematic_it.html#seppuku`](http://www.lutherblissett.net/#%20indexes/thematic_it.html#seppuku).


[^34]: La liste complète des ouvrages peut être trouvée à l'adresse [`https://www.wumingfoundation.com/italiano/downloads.shtml`](https://www.wumingfoundation.com/italiano/downloads.shtml)


[^35]: Se trouvant respectivement aux adresses [`www.wumingfoundation.com/english/giap/giapissues.html`](www.wumingfoundation.com/english/giap/giapissues.html) et [`www.wumingfoundation.com/italiano/nandro_sezione.html`](www.wumingfoundation.com/italiano/nandro_sezione.html)

[^36]: *« work-events »*, N.d.T.

[^37]: Ensemble d'*« event cards »* comprenant des instruction pour la réalisation des évènements ou éventuellement de pièces, N.d.T.

[^38]: Recueil d'instructions, N.d.T.

[^100]: En français dans le texte, N.d.T.

[^39]: *« Theatre of Futuristic Objects »* N.d.T.

[^40]: Front de Gauche pour les Arts, N.d.T.

[^41]: Diagramme Temps-Espace, N.d.T.

[^42]: *« mai »* signifiant *« en aucune circonstance »* en italien, N.d.T.

[^43]: *Réseau éternel*, N.d.T.

[^44]: Équivalents des *appels à projets* de nos jours, N.d.T.

[^45]: *« Les Débuts du Mail Art »*, N.d.T.

[^46]: *Chemins Interrompus. Crise de la représentation et iconoclasme dans les arts des années cinquante à la fin du siècle* N.d.T.

[^47]: *Opérateur en réseau*, N.d.T.

[^48]: *Networker*, N.d.T.

[^49]: *La table du petit iconoclaste*, N.d.T.

[^50]: Festival d'Appartement Néoiste, N.d.T.

[^51]: *« pranks »*, N.d.T.

[^52]: *« l'Église du Sous-Génie »*, N.d.T.

[^54]: *« Ultimate Manifest of Neoism »*, N.d.T.

[^55]: Festival d'Appartement, N.d.T.

[^56]: *Qu'est ce qu'un hum, hum, Festival d'Appartement*, N.d.T.

[^57]: *Le Projet Appt, une Cause Commune en Pratique*, N.d.T.

[^58]: *Web-network*, N.d.T.

[^59]: Pillard, N.d.T.

[^60]: Bonheur, N.d.T.

[^61]: Diffuseur, N.d.T.

[^62]: L'Italie, N.d.T.

[^63]: *le Manifeste des Nouvelles Libertés*, N.d.T.

[^64]: *Radio Ville du Futur*, N.d.T.

[^65]: *L'Œil de Carafa*, N.d.T.

[^66]: *Les Haches de la Guerre*, N.d.T.

[^67]: *Working slowly*, N.d.T. Voir `www.lavorareconlentezza.com`

[^68]: *Internationale situationiste, 1*, 1958

[^69]: Debord, 1958

[^70]: *« condividualità »* dans le texte italien de Tatiana Bazzichelli, N.d.T.

[^71]: *Qui l'a vu•e?, émission de télévision diffusée sur la Raï, équivalent italien au *Perdu de Vue* français, N.d.T.

[^72]: *Alice est le Diable*, N.d.T.

[^73]: *Alice au Paradis*, N.d.T.
