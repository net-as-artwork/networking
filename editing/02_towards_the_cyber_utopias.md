# En route vers les cyber utopies

## Punk et *« Do It Yourself »*


Pendant les années 60 et 70 grandit le désir de progressivement ouvrir le champs
de l'art à la vie quotidienne, avec l'intention de disséquer le concept
d'avant-garde. Celle-ci était vue comme un phénomène élitiste confiné à
l'intérieur des murs des musées et des galleries. L'idée était alors d'imaginer
des formes d'expressions dans des lieux autre que ceux avec lesquels il y avait
eu des dialogues jusquà ce moment, en privilégiant

> la fuite des peintures hors de leurs chassis. Cependant, dans les
> pratiques artistiques des années 60 et 70, le principe de base
> élitiste et mercantile resta le même, même si une place fut laissée
> à la dimension des espaces et lieux d'expositions utilisés à ces
> fins. L'intention était révolutionnaire, mais il fut rapidement
> réclamé par le système que ce soit l'art qui vienne rencontrer la
> vie, et pas la vie qui soit transformée en art[^1].


Le concept de la vie qui se transforme en art se retrouve conséquemment dans
d'autres formes de pratiques radicales qui ont établi des relations art/vie
encore plus intimes. Initialement, elles ont émergé sous forme d'entités
indépendantes issues du monde de l'art institutionnel, et se sont développées
seulement véritablement dans les rues et dans les réalitées locales propres aux
espaces du quotidien. Les pratiques telles que le graffiti sur les murs de la
ville, créé à New York dans les années 70, démontrent qu'il est possible de
produire un art, allant au delà de l'idée d'oeuvre elle même. Initialement, les
*écrivain·e·s* n'écrivaient pas sur les murs, parce qu'iels étaient des
artistes: leurs *« oeuvres »* ne


pouvaient pas être catégorisées par le marché et le phénomène de la galerie
d'art leur était étranger. Souvent, les *tags* sur les murs avaient soient des
significations purement personnelles, ou bien suivaient les dynamiques
d'appartenance à tel ou tel collectif, comme une sorte de communication directe
entre communautés. La plupart du temps, ils étaient créés simplement pour aller
au delà d'un ensemble de limites, dans une sorte de hacking des murs de la
ville.


Ici aussi, résident différentes techniques caractérisant le flux d'information,
ils sont des signes mobiles s'adressant à la ville, situés aux endroits les plus
insolites: sur les réverbères, les feux tricolores, sur les panneaux de
signalisation, la présence visible d'autocollants, de posters auto-produits, de
graffiti, de pochoirs (reproductions d'images photocopiées), de performances
instantanées et expéditives, ayant lieu principalement la nuit, soit sur les
murs, soit sur les toits des immeubles. Au cours des années 80 et 90, la
pratique de l'art du graffiti s'est intensifiée et s'est largement diffusée,
abandonnant les traditionnels *tags* en faveur de peintures murales plus
structurées: *urban art*[^2], comme il fut défini par certain·e·s. Même
aujourd'hui, dans les divers non-lieux de la ville, des écrits et des peintures
sont créées et re-créées, comme une voix spontanée des villes, particulièrement
dans celles qui changent en permanence[^3].


À la fin des années 70, le mouvement qui a le mieux distillé l'idée rupture avec
la tradition de l'art et de la musique était le punk, qui fut le plus important
en Italie dans les années 80. Une rebellion *« faite d'intolérance et de refus,
mais aussi d'une urgence de liberté - donnant un sens communicatif à la vie de
chacun·e »*[^4]. Beaucoup de personnes engagées dans le mouvement punk sont
issu·e·s des écoles d'art et l'esthétique du punk puise ses racices dans les
courants de dada et du lettrisme, dans l'Internationale situationiste, et
naturellement, dans Fluxus. L'essentiel étant que le punk ne s'est jamais
déclaré comme un phénomène artistique, s'il s'est même jamais déclaré comme
étant quelque chose, le punk refusa l'*« art »* au sens le plus traditionnel du
terme, et donna naissance à un imaginaire culturel, musical et créatif
différent.


En Italie, le mouvement punk prit différences formes, souvent s'alignant sur des
mouvements de pratiques politiques radicales et d'autogestion, ou bien
accentuant parfois sa dimension *« anarchique »* et *« apolitique »*. Le point
de vue le plus *« politiquement engagé »* (même si à ce moment, personne
n'aurait défini cela comme tel) se retrouva dans plusieurs publications de cette
époque, telles que les divers livres produits par la maison d'édition Shake
Edizioni Underground à Milan et dans le livre *Opposizioni '80*, edited by
Tommaso Tozzi (1991). Dans *Opposizioni



'80*, au travers de la collecte d'écrits et de témoignages provenant de membres
de la période contre-culturelle des années 80, Tozzi lie ces différentes
pratiques de vie ensembles, allant du domaine du punk au monde des artistes du
graffiti, de la musique industrielle au raggae et au hip hop, à l'univers
hacker, à celui du mail art, en suivant le fil de l'auto-gestion jusqu'au
cyberpunk et au subliminal art.


Le point de départ est l'opposition à l'idée qui voudrait que les années 80
étaient considérées comme nihilistes et indifférentes à touts types de pratiques
politiques d'opposition. Le thème central qui relie ces actions reste pourtant
l'idée de *Do It Yourself* (au travers de magazines auto-produits, de labels
indépendants, de programmes de radio, de disques, de vidéos, de critiques de
films), les plaçant comme promoteurs d'actions concrètes aux niveau politique et
social, avec pour objectif la création de réseaux horizontaux et de communautés
(idéalement au moins) donnant vie à une nouvelle forme d'art procédural et
collectif.


*Do It Yourself* signifie créer personnellement, d'une façon indépendante, en
dehors d'un environnement commercial ou de toute forme de marché. Le *Do It
Yoursel* est contraire aux logiques traditionnelles des partis, à la délégation,
à l'encadrement et il offre lui-même une forme de gouvernance directe,
insensible aux chefs de parti, amenant l'idée de la production collective au
niveau créatif, souvent grace à un réseau de relations amicales, culturelles et
politiques. Pour cette raison, quiconque a gravité autour du panorama du Do It
Yourself possède souvent des réserves à propos de l'adhésion à des mouvements
politiques, même quand il s'agissait de formations illégales, ceci étant du au
rejet du concept de délégation et de direction, qui sont les produits même de
ces contextes.


La musique est un aspect très important du mouvement punk, particulièrement en
raison de son opposition à l'idée de la scène, de la figure de l'artiste en tant
que diva, de l'étalage artificiel dicté par le mercantilisme et de l'idée de
séparation entre l'artiste et le public. Fréquemment, de jeunes punks
s'expriment en créant des groupes de musique (sans aucun professionnalisme) dans
lesquels les chanteur·e·s *« crient comme si iels étaient possédé·e·s »*, en se
fondant souvent avec le public, *pogotant* avec elleux, en faisant plus de bruit
que la musique même.


Au début, en Italie, il y avait une tentative d'imiter le punk anglo-saxon avec
des paroles en Anglais, mais par la suite il y eu une focalisation sur les
réalités locales, avec la composition de chansons en Italien sur la thématique
du rejet des institutions et, avant tout dans la région Milanaise, à propos du
rejet d'un personnage politique hostile.



> NOUS REFUSONS LA LOGIQUE QUI VEUT QUE NOUS SOYONS DES OBJETS PASSIFS ASSISTANT
> À UN SPECTACLE, NOUS CRÉONS DE LA MUSIQUE ITALIENNE NÉE DE NOS BESOINS RÉELS:
> DU VRAI FUN FACE À LEUR MASCARADE. NOUS REFUSONS LES INTERMÉDIAIRES (…)
> Celleux qui viennent aux concerts doivent comprendre que le rôle du spectateur
> peut aussi être inversé – les grands changements technologiques nous poussent
> à la production indépendante. Nous n'avons plus besoin de scientifiques à
> vedettes commerciales. Nous ne voulons plus quémander les labels et les
> nightclubs: seul(·e·)s les meilleur(·e·)s jouent là-bas, seul(· e·)s celleux
> qui ont fait cinq ans d'études, chacun·e jouera dans nos espaces…[^5]


Le punk n'était pas seulement *« conceptuel »*, il avait un impact fort au
niveau physique, corporel. Il s'agissait d'une grande performance de *body
art*. Il suffit de penser aux cheveux colorés, aux piercings, aux tatouages, aux
vêtements déchirés et recouverts de textes: ces éléments se retrouvèrent
rapidement commercialisés par l'industrie du vêtement et, dès lors, diffusés
dans les courants dominants, à tel point qu'ils sont encore aujourd'hui de très
forts symboles corporels.


La signification donnée à l'aspect extérieur est centrale dans le punk, celle-ci
étant vue comme le porte-parole d'un style et d'une philosophie de vie. Sur le
corps de chacun·e, le punk imprime ses idées de refus d'une société qui veut que
chacun·e soit læ même et standardisé·e. Un nouveau concept de vie est proposé
par rapport à la vie imposée par le haut et qui se réalise au travers de la
famille, du travail, dans un certain type de consommation, dans une façon de
s'habiller, d'écouter de la musique, de parler, etc.


 Marco Philopat, un des fondateurs de la maison d'édition milanaise Shake
 Eizioni, ramène à la vie dans son livre intitulé *Costretti a sanguinare*[^6]
 les idéaux et modes de vie des jeunes punks de 1977 à 1984, donnant une
 fraction de vie très incisive de la réalité milanaise du début des années 80
 (il s'agit de vécu pour Philopat).


Le point de vue de *Costretti a sanguinare* est que le punk n'est ni une mode,
ni une tendance, ni une sous-culture ou un *groupe spectaculaire de jeunes*,
mais un mouvement qui amène avec lui de très forts idéaux de rupture contre la
tradition et de rejet des idéologies du passé. Les punks sont conscients de la
mort de telles utopies, mais en dépit de cela, ne cèdent pas au nihilisme, tel
que les informations provenant de cette époque voudraient bien nous faire
croire. Ils travaillent ensemble à la construction d'un futur qu'ils font à
leurs (pauvres) moyens et à leurs idées (opposition et rejet), se tenant
volontiers aux confins des environnements de la *visibilité spectaculaire*
établie, tel que défini dans la majeure partie de la



frange la plus commerciale de l'industrie culturelle. Les punks sont opposé au
spectre du héros et à la vie *normale*, essayant de construire de nouvelles
possibilités, indépendemment.


Cette idée de construction de la transformation avec un état d'esprit
d'*« opposition »* était central à de nombreuses marges du punk dans les années
80, et en Italie, cela s'est appliqué tout spécialement au cœur des *Centres
Sociaux Squatés*[^7]. Il devrait être souligné cependant que alors que à Milan,
l'aile anarchiste punk se fondait avec les Centres Sociaux, à Rome, par exemple,
le punk resta pour sa majeure partie réticent à toute forme de pratique
*« politique »*, bien que se tenant à l'extérieur de la tradition et de
l'institution. Au dessus de tout, beaucoup étaient intéressé·e·s par l'idée de
liberté et d'anarchie expérimentées au travers de la culture punk, en allant
contre les règles et toutes les boites à outils théoriques.


Un example concret de ceci est le groupe romain Bloody Riot, officiellement
formé en 1982, qui créa le premier disque auto-produit à Rome en 1983. Se
trouvant souvent souvent en conflit direct avec d'autres membres de la
*« scène »* pour être ouvertement et *« joyeusement »* politiquement incorrects,
ce groupe incarna un aspect plus *« hooligan »* du punk, comme on peut le lire
dans le livre *Bloody Riot*[^8]. Bloody Riot a même joué dans les Centre
Sociaux, comme au *Feste del Non Lavoro*[^9] du *CSOA Forte Prenestino*[^10] de
Rome, mais leur approche restait différente, bien décrite par le chanteur
principal du groupe, Roberto Perciballu, dans son livre *Comme si Rien ne
s'était Passé*[^11].


## les centre sociaux squattés

Les punks amenèrent plusieurs idées dans la réalité des CSOA[^12] italiens des
années 1980: le refus d'une réalité *homologuante*, essayant de donner vie à des
zones autonomes d'expérimentations indépendantes et créant quelque chose de
nouveau, des zones définies comme *« libérées »* (Primo Moroni). Ce dernier
concept accompagnerait l'aventure de l'auto-gestion dans les futures
années 90. Durant ces années, de nombreux espaces autonomes se répandirent en
Italie: libérés et occupés en même temps, dans lesquels le concept de
*squatting* était exprimé au travers de musique, d'art, de language, de sens et
de corps *« libérés »*.  était



Vivre dans ces contradictions était la caractéristique de ces différents
occupants/habitués des Centre Sociaux, dans lesquels les habitudes de
*« résistance »* étaient exprimées vers l'adhésion de l'extérieur et la
libération depuis l'intérieur, répondant aux catégories telles que le travail,
la famille, les productions culturelles standardisées avec une autre
vision. Cette attitude a essayé d'interpréter les changements politiques et
sociaux d'une façon *« active »*: pour se les approprier, de cette façon, en
créant un monde différent. Une attitude de *« résistance »* qui dans les années
80 et 90 était les bases pour de grandes expérimentations au niveau créatif et
culturel. Peut être aujourd'hui, cela risque de se transformer en une fermeture
aux oppositions venant de l'extérieur, déjà fragmenté et n'étant plus
interprétable au moyen du contraste et du dualisme de l'opposition. En réalité,
ce mécanisme court le risque de recréer les dynamiques de pouvoirs qu'il voulait
initialement combattre.


Dans les années 80, les Centre Sociaux ont été créés pour proposer un moyen
alternatif de *« produire de la culture »*, en organisant des évènements Do It
Yourself, tels que des concerts, des films et des projections de vidéos, des
expositions artistiques et photographiques, ou en distribuant des livres, des
magazines, des CDs de musique, de la vidéo et autres productions pour un prix
modeste. Pendant tout ce temps, des zones de mise en réseau étaient nées, par la
création de bistrots, de cuisines, de maisons de thé, de lieux où danser et
présenter des musiques nouvelles à des prix considérablement réduits.


[//]: # (Pour ce symbole, voir https://it.wikipedia.org/wiki/Invasione_di_terreni_o_edifici#I_centri_sociali_occupati)
[//]: # (Ce paragraphe n'est pas très bien traduit, à éclaircir)

Le symbole du Centre Social devint un cercle avec un éclair, une réalité fermée
(aussi fermée qu'une réalité urbaine peut l'être) dilatée par une nouvelle force
fusionnante, qui parmi ses actions variées est opposée à l'utilisation
d'héroïne: un véritable fléau social parmi les jeunes personnes de cette
époque. Poursuivant la tradition des *« cercles du sous-prolétariat »* des
années 60, les Centres Sociaux se développèrent à travers toute l'Italie comme
des interfaces d'un mouvement politique *« antagoniste »* qui verra le pic de
son développement dans les années 80 et 90, tout particulièrement dans les
villes les plus grandes.


Un point de référence d'importance pour les punks italiens du début des années
80 était le Virus à Milan (1982-1984), une maison squattée dans via Corregio,
dans laquelle tous les groupes punk majeurs de cette période jouèrent,
organisèrent des évènements et produisirent du contenu indépendant, parmi
lesquels le punkzine *« Anti Utopia »*[^13]. Dans un flyer présentant les
éléments majeurs du projet Virus, on peut lire[^14]:

> PROLIFËRATION VIRALE D'ACTIONS IMAGES ET SONS
> Virus est un espace autogéré par un groupe de jeunes personnes situé au 18 via Corregio de la zone occupée de Milan.



> Virus est un projet à but non-lucratif de développement des cultures autonomes.
> Virus est participation – l'espace appartient à quiconque participe au projet – et est aussi ouvert à des situations externes visant à atteindre le même but.
> Virus est activité.
> Virus est la négation des drogues.
> Virus est maintenant dans Milan parce ce que il y a beaucoup d'ennui et tous les autres espaces nous sont fermés – c'est notre intention d'en créer d'autres.
> Virus n'est pas fermé en soi – mais opère/organise même au niveau extérieur d'après des faits/situations en cours.
> Virus est un grand point de rassemblement pour tous les groupes qui souhaiteraient jouer ou réaliser leurs activités – les rencontres se tiennent la nuit du Jeudi.
> En tant qu'espace, Virus est le résultat de batailles acharnées contre les propriétaires. En tant que structure, c'est le résultat de la bataille contre le manque d'argent
> Virus n'est ni un commerce, ni achetable – pour cette raison, les évènements seront qualitativement plus vrais ou réels ou … plus limités (selon le point de vue) – dans tous les cas, le prix du ticket sera assurément plus bas.
>
> Signé
> Collectivo punx anarchici – PUNK/ATTIVI VIRUSIANI


Dans les cercles autogérés tels que que Virus, des documents auto-produits ont
circulaient, incluant des punkzines, les magazines punk produits par les punks
même. La création de punkzines devint un point central. La présence de boutiques
de photocopie proliféra au travers des labyrinthes urbaines, donnant la
possibilité au *« do it yourself »* de se concrétiser et impliqua concrètement
tous les *Punx du Virus*. Le troc devint une pratique quotidienne. Même les
projets solitaires se multiplièrent: des projets éditoriaux où l'éditeur·e est
aussi graphiste, imprimeur·e, relieur·e et distributeur·e. Une *Diffusion
Virale* était créée: elle devint une tribune permanente pour la vente de disques
auto-produits et de punkzines[^15].


Quand le mouvement punk s'est répandu sur l'Italie au début des années 80,
*PUNKKamINazione*[^16] (1982) fut créé avec une équipe éditoriale mouvante gérée
par les communautés punk italiennes. En échange, iels disposaient chacun·e d'une
page disponible dans laquelles iels décrivaient l'état actuel de la scène punk
locale et dans laquelles iels donnaient des informations à propos des concerts
et des nouveaux


disques auto-produits. Les autres expérimentations notables du réseau punk, il
s'inscrivit dans le sillage de différents autres punkzines précédemment
diffusés, tel que *« Dudu »* (1977, une publication de subversion philo-dadaiste
auto-produite qui tira son nom de *dada* + *punk*), *« Pogo »* (1978, la version
suivante de *« Dudu »*), dans laquelle les paroles de chansons de The Clash et
de The Sex Pistols étaient traduites en italien, *« Xerox »* (1979),
*« Attack »* (1982, créé à Milan, cette publication inclut du contenu politique
radical) et immédiatement après, *« T.V.O.R. (Empty Heads Broken Bones) »* sur
la scène punk hardcore, *« Anti Utopia »* déjà cité précédemment et créé par le
collectif Virus, et beaucoup d'autres[^17].


Dans la même sphère que celle qui connecte la musique, les magazines et les
lieux, des groupes de communication indépendants se formèrent, tels que *Amen*,
qui commença en 1983 à donner vie à un véritable fanzine et à des disques, à des
vidéos, des livres, des performances et pour la première fois, inclut des
disquettes d'ordinateur dans ses productions (*Amen* publia aussi *Opposizioni
80'* de Tommaso Tozzi cité précédemment).


Le group Amen gravitait autour de Helter Skelter, un autre espace indépendant
créé comme un espace pour les concert et s'ouvrant alors à de nouveaux ferments
culturels tels que l'art industriel, la *« noise »* et les expérimentations
électroniques.


> Avec l'arrivée des groupes de musique industrielle, Einsturzende Neubauten,
> Test Department et bien d'autres, les punx milanais se retrouvèrent d'une
> certaine manière suppantés. La musique conserva le granuleux et la vélocité du
> punk, mais les instruments n'étaient plus le classique basse, batterie,
> guitare. L'un d'entre eux jouaient sur des instruments dérobés à la décadente
> société industrielle, tels que des percussions faites de bidons d'huile, des
> perceuses, meuleuses, des tubes, des chaînes et autres cordes[^18].


Quelque part au milieu des années 80, l'expérience punk en Italie sembla prendre
fin. Virus fut évacué en mai 1984 et même dans les autres lieux, les concerts
disparurent. Même si quelques groupes continuèrent de jouer pendant le début des
années 90, l'esprit du punk *« authentique »* s'est éteint au milieu des années
80 et dans les années suivantes, d'autres formes de créativité libre seraient
crées, de la musique électronique, à la techno, aux raves…


Une expérience singulière qui a réuni des expérimentation artistiques et des
mentalités punk au milieu des années 80 était le collectif *Bang Amen* (Florence
, 1986) et sont successeur Pat *Pat Recorder* (Florence, 1986-88), fondé par
Tommaso Tozzi, Steve Rozz, Nielsen Gavyna, et Priscila Lena Farias, que Massimo
Cittaini rejoindra plus tard. Tommaso Tozzi décrit cette expérimentation de
cette façon:



> Au sein *Pat Pat Recorder*, en une période d'approximativement deux ans d'une
> série initiatives alternatives frénétiques qui se sont succédées les unes
> autres autres comme des performances poétiques, sonores, multimedia, des
> spectacles ou des présentations de fanzines, collectivement ou
> indiviuellement, des nuits de graffiti dans la ville, des publications et
> d'autres choses, qui d'une certaine manière *« ébranlèrent »* le mouvement
> alternatif florentin en 1986 et 1987. Certaines performances que j'ai créé
> durant cet espace de temps étaient des parfaites réitérations de celles créées
> par des artistes d'avant-garde pendant ce siècle. L'idée était justement de
> montrer l'académisation des pratiques de l'avant-garde qui intervenait, tel
> que c'était le cas pour l'art conceptuel. C'était aussi une façon de contester
> la frange des artistes la soi-disant bohème dans son ensemble, qui durant les
> années 80, utilisèrent divers modes culturelles alternatives pour présenter
> des héros, des victimes, des personnes *« folles »* et les génies du moment,
> comme cela avait été fait précédemment[^19].


Il y aurait de nombreuses expériences à citer dans la parenthèse punk, cyberpunk
et le mouvement hacker. Malheureusement, il n'est pas possible ici de
re-présenter les multiples expériences d'aller-retours entre les anciens et les
nouveau média dans les cultures en réseau[^21] qui ont eu lieu entre Milan,
Bologne, Florence, Rome et Catane ces années là. Il est important de noter
toutefois, que qu'une part importante de ces expériences furent créées et
développées au cœur des Centre Sociaux. En fait, le statut actuel des Centre
Sociaux peut être crucial à la compréhension de la question des racines
politiques d'une large partie du mouvement hacker et des expérimentation
artistiques avec la technologie en italie.


Même si la plupart du temps, beaucoup des expérimentations DIY étaient inspirées
par des idéaux utopistes, une fois mis en pratique, les Centres Sociaux
devinrent les propulseurs de beaucoup d'expériences de réseautage en Italie –
malgré la lenteur programmatique souvent dissuasive qui a caractérisé une grande
part des projets collectifs. Durant les années 80 et 90, dans les différents
espaces italiens, des projets éditoriaux, musicaux, visuels, technologiques et
politiques prirent forme, établissant les fondations pour un concept déterminant
de *« mouvement »* qui serait utilisé par les expériences à suivre, pour de
nombreuses personnes et collectifs (dont moi, je le précise) qui ont essayé de
donner vie à leurs propres rêves de *« résistance »*, imaginant un *« autre »*
modèle de société et d'économie promouvant les productions en autogestion (même
si elles impliquaient une forme de *« recettes »*).


Malheureusement, la plupart de ces tentatives ne réussirent pas à produire des
modèles alternatifs concrets avec une certaine continuité dans le temps qu'ils
soient politiques, sociaux ou économiques, avec une large diffusion à un niveau
international et avec une viabilité économique sur le long terme. C'est peut
être du au fait que les Centres Sociaux, beaucoup des expérimentations
refusèrent ouvertement tout type de *« centralisation »*, appliquant à la place
un modèle de


fragmentation et de décentralisation. On peut citer par exemple, le cas de
G.R.A., Grande Racordo Autoproduzioni[^20] défendue par le CSOA *Forte
Prenestino* de Rome, un des Centres Sociaux les plus *« fréquenté/collaborés »*
et dans les plus anciens - il est encore actif aujourd'hui, bien qu'il ait
changé au fur et à mesure des années. Le G.R.A. avait été créé avec l'idée de
coordonner la production de documents issus de travaux autoproduits, distribués
dans les *infoshops* variés (les librairies des Centres Sociaux), avec pour
objectif de créer une sorte d'agence qui exploiterait les nombreuses expériences
locales fragmentées. Malheureusement, le projet ne s'est pas développé comme
prévu, particulièrement à cause de la vision négative qui était véhiculée par
toute tentative de *« centralisation »* dans ce contexte.


La dynamique de réseau a plutôt débouché sur des résultats plus orientés sur le
long terme dans le domaine des utilisations alternatives des nouvelles
technologies, qui même malgré la *« fragmentisation »*, a permis la génération
d'un large réseau national de personnes, et a coordonné des collectifs
initialement au travers de la diffusion de réseaux de transmission de données
et, plus tard, au travers d'internet. Le Centre Social de Forte Prenestino de
Rome a fonctionné de cette manière, avec le projet *CyberSyn II*, puis
*Forthnet, proposé par le collectif AvANa.net[^22] qui permit la création du
réseau informatique à l'intérieur des Centre Sociaux, planifiant leur câblage
élément par élément[^24].


Forte Prenestino a donné vie à d'autres expériences collectives qui se sont
imprimées sur l'imaginaire de nombreuses personnes à ce moment: il suffit de
penser aux raves et aux activités du champ musical, ainsi que la
re-interpretation de la musique *trash* des annés 70-80 avec *Toretta Stile*
créé par Luzy L et Corry X[^25]. Ici, le fantastique des dessins animé japonais
de la la musique très *« commerciale »* était réactivée dans une sort de mixture
qui désassemblait le language et les codes, et les rassemblait, donnant vie à de
nouveaux héros libérés de *Ufo Robot Goldrake*[^26] à *Captain Harlock*[^27] (un
hacker de l'imaginaire fantastique, il suffit d'écouter le générique du dessin
animé...)[^28]..


Durant la seconde moitié des années 80 se répandirent les idées qui pourraient
créer une connexion entre celleux qui fréquentaient les Centre Sociaux italiens
à de nouveaux instruments technologiques, tels que les premiers ordinateurs
personnels et les modems. Les technologies liées aux ordinateurs étaient vues
comme *« une opportunité extraordinaire de dilater la sphère de la
démocratie »*[^29], en utilisant comme example les expérimentations des
*phreakers*[^30] américains et des hackers dans les années 70[^31]. L'importance
des outils informatique avait été comprise: ils permettent l'amplification des
rapports interpersonnels et la création de productions encore plus complètement
autoproduites, limitant ainsi les coûts et le temps et même améliorant leur
qualité.


Les concepts fondamentaux de ce qui serait appelé une *éthique cyberpunk*[^32]
commencèrent à émerger en Italie, puisant dans l'inspiration et réinterprétant
des ouvrages de la veine littéraire du même nom, de William Gibson à Bruce
Sterling, et donnant vie à des pratiques qui se matérialiseraient dans la
réalité quotidienne et dans la politique. Les Centres Sociaux furent toujours
des territoires d'expérimentation libertaires avec la première phase utopique de
diffusion des technologies, allant des modems aux ordinateurs. Une large partie
de l'art en réseau des années 90 possède ses origines et son élaboration là.


C'est peut être aussi pour cette raison que le cyberpunk en Italie a pris des
connotations de mouvement politique, influençant une grande part de la réalité
et des expérimentations artistiques des futurs hackers des technologies
numériques. De façon certaine, les activités de quelques collectifs à
l'intérieur des Centres Sociaux, le premier d'entre eux étant le collectif
Decoder à Milan, ont contribué à la dissémination de l 'idée d'un cyberpunk
orienté politiquement, le tournant en Italie dans une direction différente de
celle suivie par le cyberpunk sur le reste de la scène européenne et américaine
(généralement apolitique, plus un phénomène littéraire que quoi que ce soit
d'autre). Il est fait référence au *« cas italien »*, pour lequel le phénomène
du réseau Italien est unique en son genre, à cause de ses mélanges et de la
pénétration parmi les zones politiques, artistiques, radicales et
technologiques, qui a vu plusieurs collectifs et individus perdurer tout au long
des années 80 et 90, et jusqu'à aujourd'hui.

## Le cyberpunk en Italie

Le terme *cyberpunk* fut initialement créé comme un néologisme journalistique
pour qualifier l'activité littéraire de quelques écrivains américains pendant
les années 80, de Bruce Sterling et John Shirley à William Gibson. Ou, si l'on
veut être plus précis, cela référait au fait de tirer son inspiration des livres
des années 60 et 70 écrits par Pjilip K. Dick, William S Burroughs et James
G. Ballard, ainsi que les écrits inventifs de Thomas Pynchon et les travaux
visionnaires et libertaires de Timothy Leary.


> Certains thèmes centraux émergent constamment du cyberpunk. Le theme de
> l'invasion corporelle: les membres, les prothèses, les implants de circuits,
> la chirurgie plastique, les modifications génétiques. Et la plus puissante
> invasion mentale: l'interface cerveau-ordinateur, l'intelligence artificielle
> et la neurochimie. Toutes les techniques qui redéfinissent la nature humaine:
> sa nature propre[^33].


Des livres tels que *Le Festin Nu*[^34] de William Burroughs, les nouvelles de
James G. Ballard et de Philio K. Dick, les travaux de Allen Ginsberg et des
écrivains de la Beat Generation sont redécouverts et considérés comme de réelles
sources d'inspirations pour leur écriture troublante et visionnaire. La
technique du *cut-up* de Burroughs met l'écrivain·e, peut-être pour la première
fois, dans une condition de non passivité (comme cela arrivera par exemple plus
tard avec un livre comme *TAZ* de Hakim Bey en 1991): au beau milieu d'un
apparent délire psychédélique, l'écrivain est stimulé afin de créer des
narrations sensorielles personnelles, pour construire sa propre mosaïque
personnelle.


La technique du cut-up sera considérée comme un point de départ *« esthétique »*
pour les futures pratiques de l'art des hackers et des expérimentations
artistiques numériques. Au niveau cinématographique, ces ouvrages deviendront de
véritables films cultes, tels que *Blade Runner* de Ridley Scott (1982),
*Videodrome* de David Cronenberg (1982), ainsi que sa version filmée de *The Naked
Lunch* du roman de Burroughs (1991), et le film Decoder de Klaus Maeck (1984).


Une autre source d'influence fut trouvée dans les textes de Timothy Leary, à
partir des années 60, et sa conception de la libération psychique au travers de
l'utilisation de drogues psychédélique et *« psychoactives »*, telles que le
LSD. Avec l'arrivée des ordinateurs, le mysticisme de Timothy Leary arrive à un
concept d'expansion sensible au travers des données et bits de l'informatique,
théorisant une interzone sous forme d'hybridation ordinateur-cerveau à
l'intérieur d'espace virtuel plus démocratique, un lieu de pratiques
autopilotées (fantasmes qui seront finalement concrétisés dans la Réalité
Virtuelle des années à venir). Selon ses propres mots, l'utilisation visionnaire
de drogues est une stratégie de libération en soi, avec une extrême conviction
dans le progrès et l'évolution humaine, alors que l'*« interzone »* deviendrait
une icône importante de l'underground italien des années 90.



Dans les différents livres cyberpunk, tels que le bien connu *Neuromancien* de
William Gibson[^35], dans lequel le terme *cyberspace*[^36] apparait pour la
première fois, les personnages vivent dans un univers visionnaire et souterrain,
peuplé de créatures monstrueuses, de marginaux solitaires à la dérive. Iels
vivent où les drogues, les armes, les machines, les créatures artificielles et
les mutants organico-electroniques font rages. L'imaginaire évoqué par des
romans cyberpunk s'est concrétisé dans la sphère créative de ces années au
travers des performances d'artistes déterminant·e·s, qui organisèrent des
spectacles post-futuristes dans lesquelles les machines devenaient les
principales acteures des espaces scéniques du théatre traditionnel. Parmi
elleux, la Mutoid Waste Company, le Fura Dels Baus, le collectif SRL (Survival
Research Laboratories), le performer Stelarc.


Dans le livre *Realtà del virtuale*[^37], Pier Luigi Capucci s'exprime sur les
liens entre science fiction et art:


> C'est avec l'art électronique que les connexions semblent les plus
> significatives, en particulier quand les problématiques contemporaines
> inhérentes à l'utilisation des technologies et à leur impact social sont
> questionnées, lorsque l'on utilise ces instruments, ces médias détournées de
> leur utilisation initiale: dans ce domaine, il y a des expérimentations qui
> sont basées sur la science-fiction de façon évidente, en fait, il s'agit de
> citations directes. Ce n'est pas par hasard que ces expériences sont
> particulièrement intéressantes dans le secteur des télécommunications, à
> vocation des masses sociales, à ses aspects totalisant et conditionnant et
> dont le travail consiste souvent en une critique radicale, tout ceci exprimé
> avec un refus des technologies médiatiques en elle même et par elle même, de
> la nature nouvelle que ces expérimentations sont capables de générer, via une
> appropriation et une utilisation qui est radicalement différente de
> l'utilisation initiale[^38].


En Italie, entre la fin des années 80 et le début des années 90 des scénarios
utopiques commencèrent à émerger allant de la VR (Réalité Virtuelle) et des
réflexions sur les nouvelles technologies numériques: vues depuis le magazine
*Virtual* et dans les livres de Antonio Caronia, tels que *Nei labirinti della
fantascienza*[^39] (1979), *Il Cyborg. Saggio sull'uomo artificiale*[^40] (1985)
et *Il corpo virtuale*[^41] (1996), ils sont les miroirs d'une époque. Même si,
en tant que phénomène littéraire, le cyberpunk doit être considéré comme terminé
lors des premières années 90 (comme Bruce Sterling le maintient dans un article
de 1991 qui figure dans le magazine anglais *« Interzone »*), les fantasmes
cyberpunk seraient ré-introduits dans les années à venir par de nombreux
écrivain·e·s. N'oublions pas les textes postérieurs à 1991, clairement inspirés


par le cyberpunk, de Neal Stephenson (*Snow Crash*), Par Cadigan (*Myndplayers*),
Richard Calder et Alexander Besher.


En Italie, le cyberpunk fut plus qu'un courant littéraire: il prit la forme d'un
mouvement radical inspiré par les écrivain·e·s de science-fiction qui avaient
inspiré de nouvelles utopies. L'initiative principale allant dans cette
direction était l'activité du collectif milanais Decoder, qui publia en 1990 une
anthologie sur le cyberpunk, nommée *Cyberpunk. Antologia di testi
politici*[^42], publiée par la masison d'édition Shake en 1990. Ainsi
interprété, le cyberpunk devint le parfait fantasme au travers duquel transférer
certaines pratiques des caractéristiques de l'opposition, gravitant dans le
monde politique et amené en première ligne par les hackers, dans des films, de
la musique expériementale et dans des performances artistiques qui ont été
développées en Italie au cours des années 90.


Pendant cette période, se développèrent les conditions de production
d'évènements libertaires - pré-annoncés par le courant littéraire concret et
réel - répondant à la crise italienne postérieure à 1977 en *« lisant la science
fiction depuis la gauche »*[^43]. La raison pour laquelle le cyberpunk italien
prit cette direction particulière s'explique en se référant au scénario qui
commença à prendre forme durant les années précédentes aux niveaux politiques,
sociaux, culturels dans les écosystèmes *« contre-culturels »*.


Dans les années 80, parmi les centres sociaux et sur la scène activiste
underground, la présence d'une librairie nommée *Calusca City Lights* se devrait
d'être notée. Elle était gérée par Primo Moroni (un nom qui rappelle celui de la
librairie beat de San Francisco[^44]) à l'intérieur du Centre Social *Cox 18*,
situé à via Conchetta à Milan[^45]. Calusca, existait en réalité depuis 1971,
gérée par Primo Moroni, dans un immeuble de via Calusca à Milan, d'où le nom de
la librairie. En 1995, après avoir été expulsée, elle resta sans local et trouva
refuge plus tard au Cox 18[^46].


Ce lieu est connu pour sa diffusion de documents perpétuant les mouvements
politiques et contre-culturels des dernières décennies (jusqu'aux années 60) et
fut l'origine des activités de la coopérative Shake. Les Shake Edizioni ont
proposé des éditions et des traductions de travaux sur le punk, le cyberpunk, le
cyberfeminisme, les contre-cultures psychédéliques et leurs mouvements relatifs,
aux côtés d'essais sur les *« gourous »* tels que William S. Burroughs, James
G. Ballard, Brion Gysin et Hakim Bey.


Le collectif *Decoder*, qui a fondé le magazine du même nom et les Shake Eizioni
citées plus haut a été créé en 1986 et fut parmi les promoteur·e·s des premiers
débats sur sur l'utilisation sociale des technologies de
télécommunication. Formé par Raf *« Valvola »* Scelsi, Ermanno*« Gomma »*
Garneri, Gianni *« u.v.L.S.I. »* Mezza, Giampaolo *« Ulisse Spinosi »* Capitani,
Marco Philopat, Rosie Pianeta, Paoletta Nevrosi et d'autres, il fut un des
principaux difusseurs des télécommunications des hackers et cyberpunks en
Italie.


Gomma, dans un numéro du magazine *« Decoder »*, rappelle la naissance de
l'expérimentation de l'intérieur de Calusca. En expliquant comment, en Italie,
les circuits DIY étaient le principal point de difusion des réseaux cyberpunks,
partant de l'hypothèse punk. Un fil conducteur donc, inséré dans le contexte des
activités de l'activisme radical de la prériode. En 1983, Primo Moroni autorisa
les punks à auto-gérer une pièce interne à la librairie Calusca, afin de pouvoir
vendre de façon autonome leur production en dehors du marché culturel.


> Donc nous avons commencé à travailler dans la librairie, au moment où
> l'expérience des années 70 prenait fin (mal) [...]. Mais pour nous, Calusca
> était un lieu tellement particulier que l'on ne voyait que la beauté qui
> résidait dans ce mouvement: la grande quantité de magazines et de livres
> d'auteur·ice·s communistes, anarchistes, libertaires, hérétiques, des génies,
> des *« fous »*, des personnes accro à la drogue, gays, lesbiennes, freax and
> divers *« artistes »* [...]. À la fin de ces deux années (en 1985), Primo
> proposa, à notre petit groupe de punks déjà *« contaminés »*, de continuer
> avec l édition de la *« Calusca Newsletter »*, un magazine capable de
> représenter les très grande diversité composant la librairie[^47].

Et plus loin, au regard des changements du panorama culturel à la fin des années
80 en Italie, à la suite de la diffusion de l'informatique, il poursuit:


> La *« fin du mouvement »* sembla correspondre avec la fin d'une époque dans le
> champ de la production. L'information commençait à devenir une marchandise de
> valeur et le marteau perforateur technologique était sur le point de
> décoller. En quelques années, tout particulièrement à Milan, tout sembla se
> transformer. Une sorte de mal-être aux racines profondes, que certains
> théoricien·ne·s de la *« modernité »* indiquaient comme étant le résultat d'une
> accélération typique de notre temps, en le réduisant donc, et devait être pris
> avec du recul et un certain goût du risque, de façon à ne pas se laisser
> submerger par lui. Changer de peau sans perdre son identité: transformer le
> mouvement, ou au moins, essayer de se transformer personnellement afin de ne
> pas succomber passivement à la transformation. À ce stade, nous comprîmes que
> le jeu en valait la chandelle et le collectif éditorial décida de créer son
> propre magazine: Decoder[^48].



Le collectif Decoder essaya d'interpréter les besoins du moment, en commençant à
créer de nombreuses traductions de travaux étrangers, parmi lesquelles les
publications de la maison d'édition RE/Search de San Francisco et de nombreux
autres livres de la contre-culture internationale anarchiste, technologique et
littéraire. Leur opération éditoriale contribua à difuser une vision politique
et antagoniste du cyberpunk en Italie. Hors de l'Italie, le cyberpunk n'était
pas vu comme un mouvement politique et était considéré strictement comme un
phénomène littéraire adepte de pop culture, comme cela peut être constaté dans
le magazine *« Vague »*, fondé au début des années 80 par Tom Vague. Le numéro
de vague ayant eu pour thème le cyberpunk (1988) ne pointe en aucune façon sur
une composante politique du cyberpunk, même si l'approche
anarhcico-situationiste resta (le graphiste de *Vague* est est le situationiste
Jamie Reid, qui est aussi le designer des Sex Pistols).


Même Lee Felsenstein, cité dans *« Cyberpunk. Antologia di testi politici »*
comme celui qui a *« élaboré techniquement le concept de cyberpunk »*, souligne
qu'il ne se rappelle pas avoir utilisé le terme cyberpunk avec ce sens. Lee
Felsenstein était une figure clé de la vision utopique de la technologie et du
hacking, mais ne s'est jamais identifié à un réel mouvement politique numérique
underground, ce qui est plutôt un phénomène entièrement italien[^49].


En Italie, le cyberpunk collecta ses idées parmi toutes les utopies libertaires
et les traditions radicales des années précédentes, donnant lieu à un courant
unique en son genre pour une participation collective. En Italie, la
tradition punk rencontre celle du Do It Yourself, ainsi que le besoin de
création d'espaces libérés: de nouvelles possibilités technologiques, une
attitude de partage et de création de réseaux: le cyberpunk et l'hacktivisme,
comme nous les entendons aujourd'hui, convergent tous en une étique bien
particulière. Le cyberpunk est entré en Italie d'une façon *« poreuse »*,
assimilant à l'intérieur de ses besoins et imaginaires déjà consumés depuis de
nombreuses années et prenant une direction unidirectionnelle, ce qui a par la
suite contribué à donner une identité très structurée à notre mouvement hacker,
très différemment du reste de l'Europe ou de l'Amérique du Nord, qui n'a pas eu
de connotations politiques spécifiques, à l'exception de quelques incidents
isolés[^50].


Le livre *Cyberpunk. Antologia di testi politici* fait figurer des textes
émanant de différents personnages et collectifs, créant un panorama qui va de la
littérature cyberpunk au cyber-psychédélisme, des cyber-applications aux
cyber-anarchistes et cyberpunks politiques.


Tous les textes reproduits n'étaient pas ouvertement attribuables à un discours
*« politique »*. Le cyberpunk est cependant reconnecté à par des *« affinités
électives »* aux pratiques antagonistes antérieures et au phénomène punk dans
ses composantes les plus politiquement actives, soulignant le fait qu'il a été
modelé par deux éléments: cyber+punk, dans une continuité de pratiques radicales
apparues à la fin des années 70.


Ce point de convergence entre le punk et le cyberpunk, vu comme un mouvement
d'opposition, se trouve clairement défini dans *Mela al Cianuro*[^51], un texte écrit
par Raf *« Valvola »*. Je choisi ce passage du livre:

> Il apparaît aujourd'hui essentiel de conduire une lutte pour le droit à
> l'information, au travers de la construction de réseaux alternatifs qui sont
> toujours plus ramifiés. Il s'agit d'une bataille qui peut être remportée, si
> l'on garde en tête que le même capital ne peut pas arrêter un mouvement
> économique de progresser, pour des raisons d'opportunisme
> politique. L'ordinateur est un outil, qui est potentiellement extrêmement
> démocratique: la chose importante est d'acquérir une conscience au niveau
> collectif. De plus, la littérature cyberpunk semble être un grand chevale de
> Troie, propre à intéresser ces secteurs voisins, toujours pas impliqués
> aujourd'hui, qui gravitent sur les orbites les plus éloignées des
> mouvements. Aujourd'hui, au travers du cyberpunk, l'opportunité est offerte à
> tous kes opérateurs culturels du mouvement, d'écrire un nouveau, énorme champ
> de production d'un imaginaire collectif, capable de détruire le coffre-fort
> imaginaire tenace, dans lequel nous sommes compressés depuis un moment. (…) Le
> thême inspirant du cyberpunk appartient aux mouvements contre-culturels, grâce
> à son histoire, évocations et futures fascinations. Nous devons collectivement
> le reprendre[^52].


C'est là qu'est née la *« Légende Cyberpunk »*, qui inspira en Italie les
hackers, les artistes, les intellectuels, les esprits créatifs et toutes les
jeunes personnes avec une passion pour les ordinateurs qui se reconnaissaient
dans cette vision du monde. Au travers des romans de Bruce Sterling et de
William Gibson, s'est diffusée l'idée que le panorama de la technologie puisse
être une nouvelle réalité dans laquelle chacun puisse étendre et auto-déterminer
ses actions. Il fut pensé que c'était qu travers du numérique et des
utilisations critiques de la technologie qu'il était possible d'étendre le
concept de démocratie et de liberté dans un nouveau territoire ouvert qui
restait à explorer. L'expérimentation de Decoder nous fait comprendre à quel
point le concept de DIY fut fondamental pour affirmer certaines pratiques qui
aujourd'hui aspirent à donner un sens critique aux usages du web, and pour une
large part l'éthique hacker en Italie aujourd'hui. Ce n'était cependant pas la
seule expérience qui tendait dans cette direction, mais cela s'est inséré dans



un débat né dans notre pays durant les années 80, qui a impliqué de nombreux
individus et collectifs. Avec l'idée de combattre l'institutionnalisation d'une
technologie initialement libre telle que Internet, on voit, au travers du
panorama Italien, la création de réseaux de BBS et de sites radicaux qui
réclament l'indépendance des télécommunications et se battent pour une
accessibilité pour tous de l'informatique.



## Les réseaux informatiques amateurs


En Italie, le contexte cyberpunk décrit plus tôt a pris forme dans un contexte
de réseau actif avec l'objectif de garantir un certain nombre de droits (les
soi-disant *« cyberdroits »*) aux individus, de façon à promouvoir une
utilisation consciente de la technologie et la circulation libre des
données. Entre 1980 et 1990, l'idée d'utiliser la technologie informatique pour
créer de réseaux de relations horizontaux entre individus était
activement soutenu, avec pour objectif de permettre un *« flux de communication
libre et incontrollable »*[^53]. Permettant également la possibilité de
connexions entre des *« îles-réseaux »*, par exemple des espaces de discussion
libre et de flux d'information. Ce sont les zones indépendantes dont la
philosophie est décrite dans un des textes qui ont le plus inspiré quiconque
ayant gravité dans les espaces undergrounds de ces années, tels que *Zones
Autonomes Temporaires* par Hakim Bey (1997).


En 1985, Hakim Bey écrit:


> Je suis persuadé que en tirant des conclusions des histoires du passé et du
> futur en prenant en compte les *« Îles du Réseau »*, nous pourrions collecter
> des preuves suggérant qu'un certain type d'*« enclave libre »* n'est pas
> possible actuellement, mais est pourtant présente. La totalité de mes
> recherches et spéculations se cristallisent autour du concept de Zone
> Autonome Temporaire (dorénavant abrégé en *TAZ*). En dépit de la force de
> synthétisation qu'elle exerce sur mes pensées, je ne veux cependant pas que
> TAZ soit comprise autrement que comme un *essai* (une tentative), une
> suggestion, presque un fantasme poétique. En dépit de l'enthousiasme initial
> du prédicateur, je n'essaie pas de construire un dogme politique. En réalité,
> j'ai délibérément évité de définir TAZ: je parcours le sujet en mitraillant
> des rayons exploratoires. À la fin TAZ s'explique presque d'elle même. Si la
> l'expression devenait d'usage commun, elle serait comprise sans
> dificulté,…même en activité[^54].


Entre 1980 et 1990, le concept de cyberespace était vu par la scène radicale
comme un territoire et confrontation réciproque et de relations collectives
permettant de diffuser et de partager un savoir: une métaphore dans des réseaux
neuronaux où l'information voyage selon des dynamiques réticulaires et
rhizomatiques.


Le cyberespace est imaginé comme une lieu dans lequel chacun peut agir de façon
globalisée, mais ce n'est pas pour cette raison que ce sera de façon uniforme:
l'individu peut très bien se sentir une partie d'une communauté mondiale, sans
avoir à renoncer à ses besoins individuels en matière d'information. Le terme
*« glocalisation »* connote la première phase utopique des communications en
ligne. Au travers des nombreuses *« communautés virtuelles »*, il est possible
d'expérimenter avec de nouveaux liens et en même temps, d'agir sur un
univers trans-national de données et d'informations, en ramenant à l'intérieur
ses individus et expériences *« locales »*.


En Italie, ces utopies crées au milieu des années 80 avaient un nom:
*FidoNet*. Mais revenons un instant en arrière. À la fin des années 70, le
réseau Internet, tel que nous le connaissons aujourd'hui, s'appelait Arpanet, et
était en réalité accessible seulement par quelques privilégié·e·s, tels que des
universités ou des centres de recherche. Les premiers ordinateurs disponibles
étaient les *mainframes*[^55], trop encombrants et coûteux pour l'utilisateur
moyen, qui se tourna plutôt vers l'informatique de la fin des années 70 avec
l'*Apple I* (1976) et au début des années 80 avec le premier PC IBM (1981). En
réalité, le premier ordinateur personnel historique fut l'*Altair 8800*, qui
était produit par la société Model Instrumentation Telemetry System (MITS) de Ed
Robert au Nouveau-Mexique et vendu en kit, pour être assemblé. L'Altair emprunta
son nom de la série Start Trek (un des lieux d'où le vaisseau spatial Enterprise
était dirigé) et apparu pour la première fois sous le nom de *« Altair Kit »* en
couverture du magazine *« Popular Electronics »* en 1975.


À Chicago, à la fin des années 70, Ward Christensen créa la distribution libre
du logiciel MODEM, qui permettait à deux ordinateurs d'échanger des données au
travers d'une ligne téléphonique et *« jeta les bases d'une fondation pour les
télécommunications sociales, qui deviendrait la culture des *bulletin boards*
électroniques »*[^56].


Ward Christensen utilisa l'Altair 8800 équipé d'un processeur Intel 8080 et fit
circuler la première version de MODEM à l'intérieur du Chicago Area Computer
Hobbyst's Exchange, créant en partenariat avec Keith Peterson la version
suivante, XMODEM. Durant cette période, de nombreuses personnes tentèrent de



créer des *« assemblages collaboratifs »* de cette version, jusqu'à ce que Chuck
Forsberg donne vie au nouveau protocole ZMODEM écrit en language C pour les
systèmes Unix. Ce ne fut que en 1978 que Ward Christensen et Randy Suess
créèrent le premier BBS, Bulletin Board System, appelé CBBS à cette époque.


La plupart du temps, le terme BBS était traduit en Italien par *« bacheca
elettronica »*[^57]: c'était basé sur l'échange de messages envoyés par les
modems et ordinateurs de plusieurs utilisateurs différents, dans les sphères de
divers domaines thématiques de discussion.


Les ordinateurs de ce réseau amateur étaient connectés au travers des lignes de
téléphone par les modems et on continué ainsi pour constituer les
*« noeuds »* du réseau de nombreux BBS, qui, en retour, étaient
connectés. Les messages circulaient sur le réseau principalement la nuit, quand
les ordinateur étaient configurés à recevoir et envoyer les données, qui voyageaient
de noeud en noeud, de façon beaucoup plus lente que aujourd'hui.


> Chaque ordinateur qui est utilisé comme un BBS est configuré pour répondre aux
> appels entrants, de façon automatique: pour chaque noeud dans le réseau,
> différents utilisateurs se connectent de façon à récupérer les messages qui
> les concernent. En même temps que la récupération, des messages sont postés
> sur le *« bulletin board »*: à la fois les messages privés (courrier
> électronique destiné spécifiquement à un utilisateur en particulier) et les
> messages publiques, lisibles par tous les participants de la discussion de
> groupe[^58].


En Italie, les BBS furent diffusés grâce au réseau FidoNet. En 1983, Tom
Jennings (né à Boston dans le Massachussers en 1955) a donné vie au BBS Fido N.1
à San Francisco. En 1984, Jennings connecta Fido BBS avec Fido2, administré par
John Madill et donnant lieu à la première connexion FidoNet. Le FidoNet Network
serait alors diffusé dans toutes les directions, atteignant jusqu'à 160 noeuds
au début de 1985.


Jennings est un personnage intéressant étant non seulement intéressé par la
technologie et la science, mais se définissant aussi comme *« un artiste avec une
formation technique »*. Il se tient à l'interstice de différentes disciplines,
appliquant la logique de la *résolution de problèmes* de 1977 issue des champs
de l'informatique, la programmation et de l'électronique au domaine de la mise
en réseau de BBS de 1984 et à partir de 1992 d'Internet, transférant tout ceci
dans ses travaux artistiques: machines réalisées avec des technologies
obsolètes, qui se réfèrent à *« une culture obscure, technologique »*. Les
machines de Jennings ne sont pas facilement reconnaissables par leur conception
et leurs matériaux de celles des fabriquants des premiers ordinateurs des années


50, entièrement fonctionnels et hautement visionnaires. Il les appelle
ironiquement les produits du *« World Power Systems »*[^59], rappelant la
période de naissance de la plupart des cultures du web et de la technologie et
reproduisant les contradictions qui existaient durant la Guerre Froide à notre
époque.


> Les compétences d'aujourd'hui et les cultures libertaires, la pop culture, le
> travail sur Internet, la musique et les cultures de la communication, les
> voyages, le commerce interculturel et international: tout commença durant
> cette période. La surveillance et la culture du contrôle fit la même chose: la
> crise économique mondiale, la perte de l'anonymat, la *ligne rouge* des frais
> médicaux et d'assurance. La technologie n'est pas quelque chose d'isolé, qui
> éclot dans des usines, c'est une part entière de notre culture occidentale,
> qui ne quitte plus le quotidien au travers des vêtements et de la langue, les
> mêmes ambivalences et tensions sont présentes entre la marchandise et la
> culture[^60].


Les objects de Jennings racontent une histoire d'une esthétique du design, et
leurs fonctions mettent en lumière un récit aux intérêts politiques et
économiques. Tom Jennings a un passé de punk et est ouvertement homosexuel, il
était donc habitué à aller au delà du prévisible et de voir les choses sous
leurs multiples points de vue. Seul un esprit brillant pouvait avoir donné vie à
ce qui était un des principales contributions à la culture du net, qui vit le
BBS comme sa plate-forme de lancement.


1985 es une année important pour les cultures du web, puisque c'est à ce moment
qu'intervient en Californie la naissance de *WELL* (*Whole Heart 'Lectronic
Link*), un BBS dérivé du magazine *« Whole Heart Review »* fondé par Stewart
Brand et Larry Brilliant. Un environnement pour une discussion *« explicitement
désinhibée, intelligente et iconoclaste »* (the WELL) qui rassemble
intellectuel·e·s, artistes, programmeur·e·s, journalistes, professeur·e·s,
activistes, et tou·te·s celleux qui souhaitaient exprimer leurs idées
librement. C'est là que Howard Rheingold forgea le terme de *communauté
virtuelle* et que de nombreuses théoriciens du net se rencontrèrent, dont Bruce
Sterling. En 1999, WELL était racheté par Salon.com mais un lieu ouvert et
actif pour la discussion fut conservé sur `www.well.com`. C'est parmi ce milieu
que les fondations de l'éthique hacker furent diffusées.


En 1986, FidoNet arriva en Italie et Giorgio Rutigliano lança le premier noeud
dans la ville de Potenza, appelé Fido Potenza. Au niveau international, Jeff
Rush créa Echomail, un programme de messagerie permettant la création de zones
de discussions depuis la diffusion de messages sur FidoNet.


> George, passionné d'informatique, écrivit un programme qui permettait de
> transformer son centre de service informatique en un BBS durant les heures
> nocturnes, profitant des avantages de certaines des lignes commutées qui
> demeuraient inutilisées la nuit. Lorsque FidoNet représentait environ 100
> noeuds et plusieurs milliers d'utilisateur, Giorgio apprit l'existence du
> Web[^61].


Le premier noeud FidoNet n'a rien à voir de près ou de loin avec la scène
italienne de la contreculture et du DIY.


De façon à reconstruire l'histoire du réseau numérique underground en Italie,
une série de facteurs complexes qui prirent leurs racines dans les mouvements
politiques des années 70 doivent être pris en considération. Les étapes
principales de ce parcours complexe sont bien résumées dans le texte *La nuova
comunicazione interattiva e l'antagonismo in Italia* (edité par Tommaso Tozzi,
Stefano Sansavini, Ferry Byte and Arturo di Corinto, 2000) permettant la
contextualisation des pratiques artistiques en réseau que nous avons décrites
dans le chapitre précédent:


> Dans la seconde moitié des années 60 et dans les années 80 l'Italie a assisté
> à la naissance de deux formes de protestation sociale distinctes, mais avec
> des échanges continus et points de comparaison. La première est identifiable
> dans le mouvement autonome de la *Coordinamento Antinucleare e
> Antimperialista*[^62]: elle était issue de l'opposition et du conflit avec les
> institutions politiques et économiques. Ce domaine réunit une bonne partie de
> la réalité des mouvements en Italie et dans de nombreux Centres Sociaux. La
> seconde peut être rattachée au mouvement punk (et dans son orbite, un réseau
> de fanzines et d'espaces alternatifs autogérés). C'est dans des pratiques
> artistiques individuelles et collectives connectées aux pratiques de rue
> (graffiti et autres), aux manifestations sociales et à but non-lucratif, et
> dans le secteur du mail art que le concept de *« Networking »* s'est
> développé. Ce deuxième domaine a trouvé son champ de bataille dans les
> secteurs contre-culturels, musicaux ou artistiques, et dans les
> expérimentations des nouveaux langages de communication. C'est autour de
> ces deux *« zones »* que les premières formes d'utilisation sociale de la
> technologie multimédia a été testée, la création de *« communautés
> virtuelles »* et de *« relations rhizomatiques »*. (…) Bien que les
> composantes sociales en soient bien plus complexes et différenciées,
> l'histoire du réseautage en Italie correspond essentiellement à l'histoire des
> deux réseaux radicaux italiens: l'European Counter Network (E.C.N.) et
> CyberNet. Malgrés tout, d'autres réseaux ont existé à côté, Peacelink et
> d'autres mouvements culturels par exemple.


L'European Counter Network[^63] (E.C.N.) a été créé en conséquence à une
proposition du collectif fanois TV Stop, qui en 1988 proposa l'idée de créer en
Europe, un réseau numérique contre-culturel.


> La proposition incluait d'autres contact issus de collectifs en provenance de
> France, d'Angleterre (Class War), d'Allemagne (the Autonomen, quelques
> collectifs d'occupants de maisons de Hambourg et Berlin, Radio Dreickland à
> Fribourg à la frontière avec la Suisse), d'Italie (la zone qui se référait
> Coordinamento Antinucleare e Antimperialista). (…) En 1989 les rencontres
> internationales de la section E.C.N. prirent fin et les premiers débats et
> connections expérimentales commencèrent en Italie. C'est ainsi que le réseau
> E.C.N. fut créé en Italie. *Zero BBS* fut créé à partir du noyau de *Cyberaut
> Oll* qui devint *« E.C.N. Torino »*[^64].


En Italie, les premiers noeuds E.C.N. européens furent créés en 1990 à Padova, à
Bologne, Rome et Milan. Pendant ce temps, à l'intérieur du réseau FidoNet, l'on
voyait la création d'une catégorie de messages appelée *Cyberpunk*. À
l'intérieur de cette zone, un nouvel axe de lecture des télécommunications
allait commencer à être proposée.


Nous avons donc


L'E.C.N., d'un côté, qui pour le moment considère l'informatique comme un simple
outil disponible pour l'action politique, et de l'autre côté, un domaine plus
hétéroclite (incluant des éléments de: Decoder, le futur AvANa BBS de Rome, la
Cayenna de Feltre et d'autres) qui voient dans l'informatique une nouvelle
modalité rhizomatique de communiquer un nouveau défi visant à l'action humaine[^65].


En 1992, l'espace Cyberpunk se détacha de FidoNet. Le schisme intervient du fait
de raisons aussi bien politiques que techniques, qui furent avant tout liées au
discours sur la cryptographie, qui n'était pas autorisé sur FidoNet et à la
palce encouragé par certaines réalités internes de l'espace Cyberpunk comme une
forme de protection des libertés individuelles[^66]. À partir de ce schisme,
naquit un réseau de BBS alternatif appelé CyberNet et quelques BBS expérimentaux
furent créés, configurés de façon à dialoguer entre eux au travers du réseau
Cybernet étendu.


*Senza Confine BBS* (Macerata) était la plate-forme nationale et les trois
autres noeuds étaient *Hacker Art BBS* (Florence), créé en 1990 (devint *Virtual
Town TV BBS* en 1994), *Lamer Xterminator BBS* (Bologne), créé en 1991, *Bits
Against the Empire* (Trento) et *Decoder BBS* (Milan), qui fut créé spécialement
pour l'accasion.


Après quelques mois, d'autres plate-formes furent ajoutées, qui, en quelques
années, atteignirent la taille d'une quarantaine de BBS à l'intérieur du réseau
CyberNet (parmi eux, *AvANa Avvisi Ai Naviganti BBS*, créé à Rome en 1994). Ce
réseau autonome offrit des portes d'entrées à tous les réseaux qui demandèrent
d'y entrer et fut présenté comme une zone transversale à de nombreux réseaux
(*P-Net*, *FreakNet*, *E.C.N.*, etc.), se configurant elle même comme un circuit
d'échange entre différents réseaux. Une grande rigueur dans cette réalisation
*« transverse »* se retrouva dans les actions du *Senza Confine BBS*, la
première plate-forme du réseau Cybernet créée avec pour objectif la défense
civile et juridique de migrants.


Pendant la phase de naissance des premiers réseaux de BBS en Italie, FreakNet a
également pris forme. Il s'agissait d'un réseau de BBS détaché du mondial
FidoNet et similaire aux réseaux CyberNet et E.C.N., créé par Asbesto et
Hecatombles à Catania, bientôt rejoints par de nombreuses personnes. Durant ces
années, une collaboration spontanée eu lieu entre FreakNet et le magazine
historique anti-mafia *I Siciliani* (créé par Giuseppe Fava), faisant du
magazine la première publication qui utilisa les outils informatiques pour
l'édition d'articles et pour la diffusion d'une version numérique de ses
contenus. *« Je me rappelle de quelques réunions dans les bureaux du I Siciliani
portant sur comment créer des CD-ROMs avec divers contenus ou bien comment créer
un BBS sur place, comme un noeud de télécommunication qui puisse servir à
diverses choses: accusations anti-mafia, collection et classification
d'information, base de données anti-mafia, etc. »*[^67].


À part FidoNet, CyberNet, FreakNet et E.C.N., on retrouve parmi les réseaux de
BBS en Italie durant la période *« amateur »* de la culture du net: OneNet,
FirNet, ToscaNet, ScoutNet, ChronosNet, EuroNet, Itax Council Net, LariaNet,
LinuxNet, LogosNet, P-Net, RingNet, RpgNet, SatNet, SkyNet, VirNet, ZyxelNet et
PeaceLink[^68]. Ce dernier, géré par Giovanni Pugliese, a été créé en 1991 comme
un BBS interne à FidoNet et deviendrait alors un réseau de BBS autonome l'année
suivante. Il fut ensuite une des victimes des mesures de répressions italiennes
qui allaient se déchaîner en mai 1994 contre les BBS italiens.


Dans le livre *Italian Crackdown*, Carlo Gubitosa retrace les évènements qui
amenèrent au premier, et majeur, effondrement du réseau nuérique underground
italien et qui commença comme une enquête pour piratage informatique initiée par
les bureaux des procureurs de Pesaro et Torin. Le 11 mai 1994, plusieurs BBS du
réseau FidoNet furent confisqués par la *Guardia di Finanza*[^69],


et il y eut des accusation pour *« crime organisé, trafic, copie de logiciel
illégal, fraude informatique, altération de réseaux et/ou de systèmes de
télécommunication »*.


> Des dizaines d'ordinateurs qui contenaient des programmes librement
> distribuables furent saisis et de nombreux opérateurs de systèmes (*sysops*)
> furent incriminés sur la base d'une simple suspicion, dans l'ignorance totale de
> ce qu'il se passait en réalité sur les réseaux de BBS amateurs. Une grande
> partie du matériel saisi restera de nombreuses années dans les entrepôts de la
> Guardia di Finanza, sans jamais avoir été examinés[^70].


Cela a conduit à une confusion entre les Opérateurs du Réseau, qui étaient frappés
par les mandats et la propagation de saisies qui se répandait dans toutes les
directions, rejoignant tous les noeuds des réseaux de BBS italiens
(approximativement 300 durant la période FidoNet). De façon prévisible,
l'enquête échoua, démontrant que FidoNet n'était pas utilisé pour échanger des
logiciels piratés. Cependant, après cet épisode, le réseau souffrit de ce coup
dur (même Giorgio Rutigliano, de Fido Potenza, démissionna au travers d'une
lettre adressée à Oscar Luigi Scalfaro, le président de la République de
l'époque), même si de nombreuses autres personnes continuèrent à travailler pour
faire de l'informatique une ressource partageable et publique.


Après l'*Italian Crackdown*[^71] au niveau national arriva une stratégie de
répression envers les BBS amateurs établis. Le 3 juin 1994, une autre enquête
visant le noeud central du réseau PeaceLink, une association en réseau pour la
défense de la paix, fut la victime d'une saisie par la Guardia di finanzia de
Tarante, accusée de revente de logiciels protégés par copyright. À partir de là,
un réseau de solidarité et de soutien pour PeaceLink se forma, *« luttant pour
donner une voix à celleux qui n'en ont pas »*, jusqu'au total acquittement de
Giorgio Rutigliano, responsable du réseau PeaceLink, six ans plus tard.


Ce récit de la censure en Italie ne s'est pas arrêté ici. Il suffit de se
remémorer les mesures prises en 1998 contre le serveur de l'association
Isole nella Rete[^72], à la suite de l'accusation de Turban Italia Srl, une
agence de voyages, déclenchée par un message diffusé par un collectif sur une
liste de diffusion de Isole nella Rete, invitant ses lecteurs au boycott du
tourisme en Turquie en soutien à la population kurde.


Ensuite arriva la saisie des espaces web du *Roman Civic Network* en
juillet 1998. Même si cela frôle l'absurdité, puisque la justification était
l'accusation par le prêtre sicilien Don Fortunato, originaire de Noto, perpétrée
envers l'Associazione Telefono Arcobaleno[^73] pour un *« contenu sataniste »*
présumé, sur le site de la Foro Romani Digitale Association, hébergé par la
commune de Rome. La raison en était *« une citation issue d'une oeuvre
littéraire, publiée dans un magazine distribué au niveau national, qui pouvait
être acheté dans n'importe quelle librairie. La citation était issue du projet*
Femminile nella fantascienza: modelli di scrittura *qui est une thèse de
doctorant·e présentée avec succès à la Faculté de lettres de Rome »*[^74].


Il y a toute une serie de cas de censure qui suscitent la réflexion et qui
dépeignent fidèlement le climat qui est toujours de mise en Italie
aujourd'hui. Un grand nombre de ces situations sont décrits sur le site *Sotto
Accusa*, qui est une archive listant les cas de censure et les tentatives de
limiter la liberté d'expression sur Internet gérée par Isole nella
Rete[^75]. Les informations que l'on trouve sur cette archive sont diffusées sur
la liste de diffusion `cyber-rights@ecn.org` de façon à les évaluer. Fondée et
gérée par Ferry Byte, la liste de diffusion est un point de référence central au
débat numérique en Italie, allant des thématiques de la censure à la vie privée
jusqu'à la possibilité d'accès à l'information en ligne.


Avec le temps, la réponse des réseaux de BBS italiens à ce type de censure prit
la forme d'une série d'initiatives collectives aux niveaux local et national de
façon à améliorer la sensibilisation autour des droits numériques tels que la
vie privée, l'anonymat, l'instruction autour de l'informatique, la fracture
numérique, etc. L'un des premiers évènements et sûrement le plus révélateur au
niveau national dans cette période de naissance des BBS et par conséquent lors
de l'Italian Crackdown, fut le *Diritto alla comunicazione nello scenario di
fine millennio*[^76], organisé par le collectif Strano Network à Florence. Le
rassemblement, dont à la suite duquel fut publié le livre *Nubi all'orizzonte*[^77]
(1996), se tint le 19 février 1995 au Centro per L'Arte Contemporeana Luigi
Pecci à Prato et offrit une intéressante vue panoramique de la situation des
réseaux italiens, avec des conférences de divers théoricien·ne·s, sysops,
artistes et *networkers* de cette phase de la culture numérique.


Dans le livre *Nubi all'orizzonte*, il est possible de revivre une partie de
l'athmosphère de cette période: dense de questions à propos des utilisations
futures de la technologie dans les environnements indépendants et radicaux
italiens.



C'est un exemple direct de comment la création des BBS a donné vie à un réseau
de personnes, de projets, de collectifs, qui ont poursuivi leur travail dans les
années postérieures. L'histoire qui suivra ces réalités est intimement imbriquée
avec les concepts de l'*éthique hacker*, synthétisée avec le terme
*« hacktivisme »*: le maître de cérémonies d'une grande part des futures
initiatives du réseau.



## Du cyberpunk à l'industriel


Le travail vidéo et cinématographique de Mariano Equizzi[^78] est un exemple de
comment les cyber-utopies ont influencé la créativité de nombreux artistes et
activistes italien·ne·s au début des années 90. Sa description nous permet de
tiser un nouveau lien dans le paysage du réseau italien en connectant les
utilisations *« alternatives »* de la technologie et de l'informatique avec les
expérimentations musicales et visuelles se situant entre le cyberpunk et
l'industriel. Matiano Equizzi, un auteur qui est né et qui a grandi à Palerme, a
travaillé par la suite entre Rome, New York et Tokyo. Inspiré d'auteurs tels que
William S. Burroughs, James G. Ballard et Philip K. Dick, Equizzi transfère dans
ses travaux le cadre de nombreux ouvrages de la Beat Generation et de la science
fiction. Il a dirigé de nombreux courts métrages qui sont depuis devenu des
oeuvres italiennes indépendantes cultes. Parmi celles-ci, *Syrena* (1998),
*AgentZ* (2000, Premio Italia 2001), *Giubilaeum* (2000), *Ginevra Report*
(2001, Premio Italia 2002), *Crash* (2001), *Sign* (2001), *New Order* (2001,
Premio NokiaWind Contest 2002) *D.N.E.*[^79] (2002), *The Mark* (2003), et
*R.A.C.H.E.* (2005). Depuis 1999, il a travaillé avec Luca Liggio (producteur
vidéo numérique)[^80] et Paolo Bigazzi (designer sonore et producteur
son)[^81]. Il travaille actuellement sur le film *Revenge*, basé sur l'histoire
*Gorica tu sia maledetta* (1998) de Valerio Evangelisti et *Le Bestie di
 Satana*, un livre d'horreur en partenariat avec Andrea Colombo.


Mariano Equizzi s'est consacré à la vidéo produite avec les outils analogiques
largement utilisées durant les années 80, étudiant au Centro Sperimentale di
 Cinematografia de Rome en 1996[^82]. C'est ici qu'il travailla à la production
 des courts métrages de l'école et travailla plus tard comme
 assistant-producteur et *runner* sur les plateaux de tournage, devenant
 l'assistant-réalisateur de Michele Soavi. En 2000, en partenariat avec


Emiliano Farinella, Equizzi créa *Next Text TV* (Interact production, Rome), un
portail qui explorait le contenu de différents types de médias (texte, flash,
vidéo et audio), les mixant tous ensemble et créant tout le contenu vidéo et
flash lui même[^83].


Mariani Equizzi raconte, en revenant sur ses premières tentatives technologiques
en collaboration avec Luca Liggio et Paolo Bigazzi,


> Fidèles au Cyberpunk comme une pratique fonctionnelle et comme un verbe
> esthétique, nous nous sommes lancés dans l'arène du montage non-linéaire à une
> époque où les ordinateurs disposaient au mieux de processeurs MMX 166Mhz. Il
> semblerait que les visiophones d'aujourd'hui sont plus puissants. Mais à cette
> époque, tout était délégué à la légendaire carte Targa 2000 et au contrôleur
> SCSI II qui avaient touts deux été achétés par Luca. Nous nous étions habitués
> aux épouvantables opérateurs de Sony qui les avaient mountés avec des cables
> Control-L[^84] et avec une avalanche d'énormes boites permettant de traiter le
> matériel filmique créatif, énormes boites qui fonctionnent toujours
> parfaitement aujourd'hui. Les orinateurs étaient maintenus ouverts avec des
> ventilateurs à proximité, de façon à éviter les soucis fameux d'étalonnage
> thermique. Dans cet état de crise du matériel, qui a même concerné
> l'équipement audio (des fichiers MIDI à la place de fichiers WAV non
> compressés et un Roland DM-80[^85], que Paolo utilisait comme si il s'agissait
> d'un NEVE[^86]), nous avons posé les bases de notre style de guérilla[^87].


La perception cinématographique de Mariano Equizzi est unique car, même si elle
a pris forme en Italie durant la même période de diffusion que de nombreuses
instances Cyberpunk *« subversives »*, il n'y avait initialement pas de contacts
directs avec les expériences des réalités créatives et artistiques italiennes
les plus politisées[^88]. Le travail de Equizzi, transmet les fantasmes
cybernétiques, plus que toute autre chose, d'un point de vue qui est résolument
*« politiquement futuriste »*[^89], proposant le contenu comme une forme de
*divertissement*,


> travailler avec des détritus par nécessité, se baladant dans des entrepôts
> abandonnés, des lieux qui représentent de toute façon parfaitement les
> processus paranoïdes et le déclin technologique dont le gibsonisme était le
> garant (…) Le chemin irrégulier de la production vidéo nous a permis de
> fournir un résultat qui n'était pas *« parfait »*, mais inédit, et à un prix
> très réduit. (…) Nous avons appliqué cette modalité au Cyberpunk, cette
> tendance à décoder et à approcher la narration d'un point de vue de hacker,
> paranoïde et complexe, se référant au motto de Oncle Bill (William
> S. Buroughs): *« Rien n'est réel, tout est permis »*[^90] (…) Une mosaïque de
> média morts et vivants, notre mélange favori.


Mariano Equizzi, expliquant en quoi ses productions vidéo et cinématographiques
ont été influencées par les fantasmes Cyberpunk, ajoute:


> Le Cyberpunk était pour moiun système d'observation de la réalité, de décodage
> des manières et possibilités de réalisation, indépendamment des possibilités
> des outils. La réalisation indépendante du pouvoir détenu par les opérateurs,
> qu'il soit économique, au travers des médias ou politique, toutes les formes
> de pouvoir qui prennent part aux médias d'une façon pénétrante et
> castratrice. Le Cyberpunk créa pour nous un alibi, commettant l'erreur, le
> média corrompu, une matrice de transmission crashée, le sujet, ou un des
> sujets, de la narration. Le manque de ressources datant de la vidéo analogique,
> trop profond pour nos moyen, nous a stimulé dans ce sens. Mon amour pour les
> garages, les ateliers, et les entrepôts de matériel industriel en dit long sur
> les modalités productives que je poursuis toujours aujourd'hui. Bigazzi a eu
> un mur de vieux synthétiseurs et Luca des caméras 16mm utilisées par les
> chinois durant la période maoïste (le *Canon Scoopic*). L'utilisation de tous
> types de média, par exemple, des anciennes caméras à des lampes, de systèmes
> de vidéo-surveillance jusqu'au numérique à la point, sans oublier le Super 8
> est le résultat de ce melting pot où chaque oeuvre doit s'apparenter à une
> culture uniforme, tels les courants chaotiques de nos flux médiatiques.


Les scènes du court métrage *Syrena*[^91] en particulier, a marqué un jalon de la
vidéo indépendante italienne d'après de nombreuses personnes. Elles furent
créées en recyclant des parties d'un ordinateur central[^92] Honeywell abandonné
dans un entrepôt. Mariano Equizzi raconte,


> par l'utilisation seule de Premiere 2.0, After Effects 2.0 et 3ds Max 1.0,
> ainsi qu'une large quantité de vidéo et d'illustrations (plus de 250 CD ROMs),
> nous avons créé une épopée Cyberpunk qui reste toujours inégalée aujourd'hui,
> selon certain·e·s par sa complexité, originalité et par son économie de
> moyens. (…) Le travail indépendant qui nous consacra est toujours *Ginerva
> Report*[^93] (2001), que nous avons produit avec la distribution *Interact Web
> Streaming* et la patience habituelle de Luca Liggio (video) et Paolo Bigazzi
> (audio). Ce travail fut dédié à un de nos héros: James G. Ballard, le chantre
> de *l'intérieur* un auteur de science fiction très atypique et vénéré. (…) Une
> fois de plus, nous avons appliqué un système de production *« absurde »*: nous
> avions envoyé la webcam avec ses pilotes et son logiciel de capture
> *shareware*. Les discours ainsi enregistrés étaient assemblés sur une vidéo
> multi-écran. Il y avait toutes sortes de contributeurs: un spécialiste de
> Ballard, un professeur du Cern... tout ce petit monde sur une chaine stéréo
> audio 7 et des vidéos provenant de webcams séparées. À cette époque, il n'y
> avait pas de possibilités d'obtenir de l'ADSL et de toute façon, la qualité de
> transmission des webcams est toujours très basse pour nos besoins.


Au même moment, l'activité de Mariano Equizzi, Luca Liggio et Paolo Bigazzi
était inspirée par une autre vague créatrive. Cette fois c'était la scène
musicale Industrielle qui influença de nombreux autres artistes et créatifs qui
étaient actifs en Italie des années 80 aux années 90[^94]. Pour décrire cet
autre fragment imaginaire, important ppour comprendre la situation en Italie
durant cette période, nous citerons encore Mariano Equizzi:



> L'Industriel ne nous a touché que par la force des choses. Elle a été créée en
> 1975 et Luca et moi sommes nés en 1970 et 1971, nous avons donc manqué le raz
> de marée, mais nous l'avons personnellement assimilée inconsciemment dans sa
> tendance extrême à la provocation et son aspect propice à la discussion
> artistique, avec une liberté féroce, même le pire aspect des humain·e·s:
> quelque chose que le Punk n'a pas *« trop »* fait, en se limitant à ses
> expressions politiques à prendre partie d'un côté plutôt que de
> l'autre[^95]. En un sens contraire, le mouvement Industriel a tout ébranlé et
> ceci dans un processus d'*« exaltation »* (…) Dans ce chemin à contresens dans
> les abysses des pratiques artistiques et parmi les horizons obscures de l'Art
> Moderne, le mouvement industriel signale, d'après moi, le point de vue le plus
> BAS et aussi le plus douteux/incivilisé. Nous avons baigné dans ces
> *« abysses »* depuis que nous avons débuté le projet *Revenge*, avec le
> soutien d'opérateurs étrangers, en particulier des *noise-ophiles*
> occidentaux. Et je place *Anime 28* sur la base de cette même dérive envers
> l'incivilité artistique. Je crois radicalement que cette exploration des
> couches les plus dégradées (Burriani?) de l'expression artistique se doit
> d'être connectée à un processus qui intervient dans différents domaines
> médiatiques. Les tendances des musiques *glitch* ou du *soundscape* et aussi
> les rituels de *dark ambient*[^96] sont des moments de dérive technologique
> organisée qui devraient, je crois, être coordonnées avec l'American New Horror
> des années 70, qui a fait un *« grand »* retour dans des films tels que
> *« Saw »* (James Wan, 2004), *« Hostel »* (Eli Roth, 2005) et divers remakes
> de films extrêmes de cette période. Des oeuvres d'art rongées, granuleuses,
> délabrées et recouvertes de cendres, des images et documentation de ruines et
> de catastrophes, de paranoïa borderline et d'hypothèses étranges. Je dirais
> que l'évènement qui a fait dérailler le plus significativement mes processus
> artistiques depuis le début du vingtième sciècle est le 11 septembre 2001,
> avec ses décombres et son obscurité. Nous nous situons dans la pénombre de cet
> évènement et nous contruisons des images et sons taillés dans la masse de
> notre conscience qui est cernée par les sédimentations technologiques
> complexes qui surviennent, au fur et à mesure, comme du plastique brulé.


Des sédimentations technologiques parmi lesquelles revivent les mouvement punk, Cyberpunk
et industriel, tels que décrits dans ces paragraphes. C'est elleux qui
constitueront le substrat créatif de nombreuses expériences d'artistes et
d'activistes italien·ne·s entre 1980 et 1990, comme nous allons le voir dans le
chapitre qui suit.

[^1]: Simonetta Fadda, à l'occasion d'une conversation privée avec l'auteure, Avril 2006.

[^2]: Berlin est un example parfait du phénomène d'art urbain.

[^3]: Le graffiti artist Voegel/Bird fut l'un des premiers à parsemer la ville de ses oeuvres, amenant les messages d'une nouvelle génération d'artistes urbains dans Berlin Est: en créant des images qui étaient bien plus articulées que des tags, Voegel fut immédiatement suivi par les Katzen/Chats et par l'artiste appelé Sechsen, qui pendant dix ans, envahi la ville de Berlin avec le chiffre six: *sechs* en Allemand. Parmi les street artists les plus importants à Berlin, un des plus anciens, mais toujours en activité, est le groupe CBS, qui se caractérise par un poing fermé jaune, visible le long des voies de la S-Bahn, sur les murs des maisons et dans des lieux inhabituels. CBS sont connus pour leurs actions politiques, telles que le *« kidnapping »* du poster créé pour l'exposition *« Kommunikationsstrategien der Werbung »* [*Stratégies de communication publicitaires*, N.d.T.] dans la station de métro Alexanderplatz. Le collectif CBS a littéralement *kidnappé* l'affiche où DDR était inscrit [*Deutsche Demokratische Republik*, N.d.T.], pour l'afficher sur le mur du centre commercial *« Kaufhof »* sur Alexanderplatz, un immeuble en forme de nid d'abeille, le symbole du pouvoir soviétique avant la chute du mur (et qui, après le restructuration réçente, perdit son motif en nid d'abeilles avec la majeure partie de son attrait). CBS remplaça l'affiche avec son propre travail, avec le même poing fermé jaune, créant une situation sujette à controverse et de nombreuses discussions. CBS est en réalités connu pour des actes de sabotage, des tactiques d'appropriation et de guérilla communicative, portés par le motto *« la ville nous appartient »*, ce qui est une réminiscence des squatters qui occupaient des appartements et des immeubles dans les années 80 et 90 à Berlin. De temps à autre, les actions des graffiti artists sont projetés à la lumière du jour, tels que l'évènement collectif regroupant de nombreux street artists qui prit place en septembre 2004 à Helmholtzplatz Platzhaus (dans la zone de Prenzlauer Berg). Au delà des noms célèbres et d'autres moins connus de la scène du graffiti, le rassemblement permit à quiconque souhaitait participer d'y prendre part spontanément. L'évènement était complètement auto-financé, produisant un travail collectif qui est encore visible aujourd'hui sur les murs de la Plazhaus. Parmi les participants se trouvaient les street artists *Nomad*, dont le projet *GATA* était capable de connecter réciproquement l'art urbain de Berlin, de Copenhague et de nombreuses villes en Islande.

[^4]: Gavyna, 1994. *Ipertext Fluxus* a été créé par le collectif de télécommunication *Strano Network*, qui la même année (1994), coordina trois autres hypertextes: *Testi Caldi*, *Stragi di Stato* and *Metanetwork*, tous publiés par Wide Records, un label independant de Pisa. Les hypertextes peuvent être téléchargés à l'adresse `www.strano.net/snhtml/ipertest/ipertest.htm`.

[^5]: Philopat, 1997, p. 64. Il s'agit d'un texte issu d'un flyer punk qui était distribué devant le Studio 54 et à l'Odissea 2000 à Milan, tel que reproduit dans la série de livres à Milan entre 1977 et 198. La nouvelle édition du texte, avec une préface de l'auteur et des contributions de Stiv Rottame, est publiée chez Einaudi,

[^6]: *Nous devons saigner*, N.d.T. Le titre du livre de Philopat dérive des paroles de la chanson *We must bleed* du groupe californien *Germs*, dont le chanteur, Darby Crash, était une des personnalités les plus dérangeantes et en même temps fascinantes de cette période. Le texte de Pholopat possède aussi un style littéraire hors des sentiers battus: la façon habituelle d'écrire est substituée par des séquences où l'on ne retrouve pas les enchaînements habituels de lettre capitales pour le premier mot, phrase, suivis d'un point, même si elles sont reliées entre elles avec des *« – »* rappelant un flux de conscience ou un dialogue normal.

The title of Philopat’s book derives from the song lyrics of the Californian band Germs (We must bleed) whose singer, Darby Crash, was one of the most disturbing and at the same time fascinating personalities of that period. Philopat’s text also has a literary style which does not follow rules: the traditional way of writing is substituted with sentences that don’t present the canonical sequence of a capital letters for the first word, sentence, period, even if they are linked between them with "–" which reminds us of a stream of consciousness or a normal dialogue.

[^7]: *Centri Sociali*, des squats qui tissent des liens forts avec leur voisinnage et sont parfois légalisés, N.d.T.

[^8]: Perciballi, 2001

[^9]: Le Festival du Non-travail, N.d.T.

[^10]: Centre Social occuppé et autogéré depuis 1986, N.d.T.

[^11]: Come se nulla fosse (1998)

[^12]: *« Centro Sociale Occupato Autogestito »*, *Centre Social Occuppé Autogéré*, N.d.T., `http://ita.anarchopedia.org/Centro_Sociale_Occupato_Autogestito`

[^13]: Un court métrage à propos du punk italien, en particulier sur le Virus de via Correggio à Milan, peut être téléchargé sur le site de New Global Vision à l'adresse `www.ngvision.org/mediabase/88`. D'autres ressources de leur répertoire vidéo sur le Virus de Milan est disponible sur le site de Strano Network à l'adresse `www.strano.net/town/music/punk.htm`.

[^14]: Philopat, 1997, pp. 105-106

[^15]: Ibid. pp. 111-112

[^16]: Un jeu de mot sur *contaminazione*, N.d.T.

[^17]: Pour en savoir plus sur le flyer punk *« PUNKamINazione »*, viseter le site de Acrataz Collective, qui contient de nombreux posts historiques instructifs et de nombreux documents sur le punk italien des années 80. `http://acrataz.oziosi.org/rubrique.php3?id_rubrique=47`. Un autre texte utile pour creuser plus en profondeur la discussion sur les punkzines et les fanzines, même s'il est écrit d'une façon plus descriptive, est l'extrait de la thèse de doctorat de Susanna Vigoni sur l'histoire du Virus de Milan, intitulé *« Virus: Cont-aminazione punk a Milan »* publié dans le magazine *« Punkadeka »*, dans l'onglet *Articoli* > *Yesterday Heroes* à l'adresse `http://www.punkadeka.it/virus-contamin-azione-punk-a-milano-fanzine-e-punkzine/`.

[^18]: Philopat, Ibid. p. 176

[^19]: 1991, p. 17

[^20]: Grande Connexion de l'Auto-production

[^21]: Pour une analyse chronologique détaillée se référant à l'Hacktivisme, voir *la libertà nelle maglie della rete*, edité par Arturo di Corinto et Tommaso Tozzi, Manifestolibri, Rome, 2002,

[^22]: `http://avana.forteprenestino.net`

[^24]: Pour une description des projets CyberSyn II et Forthnet, visiter: `www.ecn.org/forte/cybersyn2/index.html`.

[^25]: `www.torettastile.com`

[^26]: *« Goldorak »* série animée japonaise diffusée en Europe les années 80, N.d.T.

[^27]: *« Albator »*, série animée japonaise diffusée en Europe les années 80, N.d.T.

[^28]: Pour se plonger au coeur de cet univers, nous recommandons de consulter le site de Goldorak `www.goldrake.info` et celui de Albator `www.capitanharlock.com`.

[^29]: Primo Moroni

[^30]: *Phone-hackers* ou *phone freaks*, hackers de lignes téléphoniques, N.d.T.

[^31]: De façon à contextualiser historiquement les techniques imaginées par les phone-freaks, voir le chapitre 4

[^32]: *etica cyberpunk* en Italien, N.d.T.

[^33]: Sterling, 1986, p. 40

[^34]: *The Naked Lunch* (1959), N.d.T.

[^35]: *Neuromancer*, 1984, N.d.T.

[^36]: *Cyberespace*, N.d.T.

[^37]: *Réalité virtuelle*, N.d.T.

[^38]: *Realtà del virtuale*, Pier Luigi Capucci, 1993, p. 128

[^39]: *Dans les labyrinthes de la science-fiction*, N.d.T.

[^40]: *Le cyborg. Essai sur l'homme artificiel*, N.d.T.

[^41]: *Le corps virtuel*, N.d.T.

[^42]: Cyberpunk. Une anthologie des textes politiques, Shake ,Scelsi, 1990

[^43]: Caronia

[^44]: City Lights Bookstore, voir https://fr.wikipedia.org/wiki/City_Lights_Booksellers_%26_Publishers, N.d.T.

[^45]: https://archive.org/details/PrimoMoroni-Calusca

[^46]: Un squat en place depuis 1976, https://radar.squat.net/en/milano/cox18, N.d.T.

[^47]: *Decoder*, 1998, p. 929, Gomma, Editorial in Decoder no.12, C'était bien avant que le projet de Calusca Newsletter prenne son envol à cause de l'expulsion qui força la fermeture de la librairie.

[^48]: Ibid, p. 930

[^49]: Ces réflexions sont le fruit d'un échange de messages avec Lee Felsenstein (en mars 2006), qui m'a envoyé une quantité considérable de documents écrits de façon à ce que je puisse creuser plus en profondeur sa façon d'approcher le hacking et les technologies infomatiques suivant les expériences menées pendant sa jeunesse lors du mouvement étudiant à Berkeley (Berkeley Free Speech Movement). Entre autres, se trouvait le texte de son discours *The Arming of Desire, Counterculture and Computer Revolution* donné au Waag à Amsterdam en septembre 2005 durant une conférence sur le *« Salut dans le Cyberespace »* dans lequel Lee Felsenstein expose les origines de l'ordinateur personnel et de l'internet en partant de la contreculture politique et sociale du San Francisco des années 60 (la vidéo du discours peut être téléchargée à l'adresse `http://connect.waag.org`). Un de ses essais autobiographiques et le texte de *The Hacker League* posté à l'adresse `http://www.leefelsenstein.com/wp-content/uploads/2013/01/The-Hackers-League.txt`,un développement de l'article *« Real Hackers Don't Rob »* commandé et dont le titre avait été choisi par *« Byte Magazine »* pour le trentième anniversaire de la revue. L'article reste inédit et a été lu par Lee Felsenstein en 1985 à une conférence sur le hacking organisée par l'ACM. Une description plus en profondeur des activités de Lee Felsenstein est donnée dans le chapitre 4 de ce livre.

[^50]: Le premier parmi eux fut le CAE (Critical Art Ensemble) et à sa suite l'Electronic Disturbance Theater dans lequel Ricardo Dominguez était actif. La vision politique du Cyberpunk de Dominguez l'attira à proximité de la philosophie de l'hacktivisme italien.

[^51]: *Pomme au Cianure*, N.d.T.

[^52]: 1990, pp. 32-33

[^53]: Incontrollé ne signifie pas non réglementé: il y a des règles qui s'appliquent à la communication en ligne (netiquette) de façon à faciliter la transmission des données sur le web, et en même temps, de s'assurer de conduites respectueuses envers les autres utilisat·eu·r·ice·s. Il s'agit cependant de règles non-écrites, appliquées au bon vouloir des personnes, faisant appel à leur bon sens et leur désir de garantir une communication de données fluides et *« correctes »*.

[^54]: 1997, pp. 12-13

[^55]: Un ordinateur central ou *mainframe computer*, est un ordinateur de grande puissance de traitement et qui sert d'unité centrale à un réseau de terminaux, N.d.T.

[^56]: Gubitosa, 1999

[^57]: Littéralement, *« vitrine électronique »*, N.d.T.

[^58]: *Ibid* p.14

[^59]: Littéralement, *« Systèmes du Pouvoir Mondial »* N.d.T.

[^60]: [`https://tinyurl.com/yc76yamy`](https://web.archive.org/web/20080618175015/www.wps.com/about-WPS/WPS/index.html), plus d'informations sur http://sr-ix.com/, N.d.T.

[^61]: Gubitosa, 1999, pp. 21-22

[^62]: Coordination Antinucléaire et Anti-impérialiste, N.d.T.

[^63]: *« Réseau de Riposte Européen »*, N.D.T.

[^64]: *Di Corinto*, Tozzi, 2002, p. 205

[^65]: Tozzi, Sansavini, Byte, Di Corinto, 2000

[^66]: Ces problèmes sont bien décrits dans l'essai *« Il messagio crittato »* de Luc Pac, que l'on retrouve dans la publication du Strano Network *Nubi all’orizzonte. Diritto alla comunicazione nello scenario di fine millennio*, Rome, Castelvecchi, 1996, pp. 149-154, et qui peut être consulté en ligne à l'adresse `www.dvara.net/HK/bits.asp`.

[^67]: Asbesto, `www.freaknet.org`

[^68]: Pour une liste complete des BBS et bien d'autres sources d'informations utiles concernant les réseaux amateurs télématiques, consulter le texte *Digital Guerilla. Guida all’uso alternativo di computer, modem e reti telematiche*, auto-produit par Cyberspace, Torino, 1995.

[^69]: *La Garde des finances*, la police douanière et financière italienne, N.d.T.

[^70]: Gubitosa, 1999, p. 41

[^71]: *La Répression Italienne*, N.d.T.

[^72]: *L'île dans le Réseau*, N.d.T., voir www.ecn.org

[^73]: *Association Téléphone Jaune*, N.d.T.

[^74]: Pour en savoir plus, consulter `www.ecn.org/censura/frd.htm`.

[^75]: `www.ecn.org/sotto-accusa`

[^76]: *Droit à la communication dans une hypothêse de fin de millénaire*, N.d.T.

[^77]: *Des nuages à l'horizon*, N.d.T.

[^78]: `www.marianoequizzi.com`

[^79]: *Descrambling Nova Express*, inspiré du *Nova Express* de Burroughs.

[^80]: `www.lucaliggio.com`

[^81]: `www.iter-research.com`

[^82]: Il fut inscrit à l'école dans un cours de production avec des professeurs tel·le·s que Fernando Ghia (Gold Crest), Giannandrea Pecorelli (Aurora), Dino Di Dionisio (Urania Film), Mario Liggeri, Francesco Ventura.

[^83]: www.ntxt.it

[^84]: LANC, un protocole de communication pour le matériel et les logiciel inventé par Sony et permettant de synchroniser les caméras, https://en.wikipedia.org/wiki/LANC, N.d.T

[^85]: Un studio intégré et enregistreur/éditeur multipiste à disque dur, N.d.T

[^86]: Une marque réputée de mixers professionnels (souligné par Mariano Equizzi).

[^87]: Ceci and les citations suivantes sont extraites d'un texte inédit de Mariano Equizzi, intitulé *Dal Cyberpunk all’Industrial, una involuzione crudele di Mariano Equizzi* (réflexions sur sa symbiose techno-créatrice avec Luca Liggio et Paolo Bigazzi). Mariano Equizzi écrit l'article en janvier 2006, au moment où je lui ai demandé qu'il raconte l'histoire de ses activités artistiques liées au Cyberpunk, alors que j'avais la seconde publication de ce livre en tête. L'article est un parfait example de l'esprit de l'époque (les années 90) et de l'évolution du concept de Cyberpunk en Italie, particulièrement au niveau concret des arts visuels et du cinéma indépendant.

[^88]: La confrontation inévitable avec de telles réalités arriva quelques années plus tard au *Hackmeeting 2000* à Forte Prenestino à Rome, pendant lequel Equizzi projetta ses courts métrages.

[^89]: Un point de vue irréaliste, N.d.T.

[^90]: Tiré de *Naked Lunch*, 1959, de William S. Burroughs, traduit par Claudio Gorlier, 1992, SugarCo Edizioni.

[^91]: Disponible à l'adresse `https://vimeo.com/126099901`

[^92]: *Mainframe*, N.d.T.

[^93]: Disponible à l'adresse `https://vimeo.com/128721837`

[^94]: Il existe une bonne définition du phénomène industriel sur sa page Wikipédia italienne: *« Né à la fin des années 70, l'industriel est certainement une des formes de musique les plus extrêmes jamais produites. Ce style englobe un dérivatif de Krautrock mais qui est moins attentif aux formes classiques des morceaux, tels que dans des groupes comme Faust et New. Ce genre a été orienté par des influences extra-musicales telles que les Avant gardes artistiques du début du début du vingtième sciècle (comme le Dadaisme, les Situationistes, mais avant tout la performance), des livres d'auteurs tels que Philip K. Dick et James Graham Ballard et toutes ces sous-cultures dédiées à l'ésotérisme et/ou au conspirationisme. Le genre a été initié par *The Throbbing Gristle*, un groupe britannique qui grandit parmi le collectif COUM Transmission. Leur premier disque, *The Second Annual Report* délimita les limites de cette scène (qui se développa par elle même gràce au travail incessant du groupe qui publia des fanzines, maintint des contacts avec des groupes similaires en Angleterre et aux U.S. et publia les premiers albums sur son propre label, *Industrial Records*). La scène industrielle fut marqué par une utilisation presque terroriste de la noise, le rejet total de la technique musicale, l'utilisation de bandes pré-enregistrées et d'un rejet presque-total de toute forme mélodique. Suivant leur exemple, d'autres groupes émergèrent, qui, tout en ayant un caractère similaire, développèrent le genre vers des directions très différentes, tels que le groupe britanique Cabaret Voltaire et Clock DVA, The American Factrix, Boyd Rice aet Monte Cazazza, les autraliens S.P.K., les allemands Einstürzende Neubauten, les italiens F.A.R., qui était composé de Maurizio Bianchi, et les slovéniens de Laibach. Au cours des années 90, de nouveaux musiciens, tels que Current 93, amèneraient le genre vers les rivages de la musique folk. Pendant ce temps, dans les années 90, le terme *inustriel* allait finir par être utilisé pour définir des groupes dérivatifs du métal tels que Ministry, Skinny Puppy, Godflesh et Nine Inch Nails »* (`http://it.wikipedia.org/wiki/Industrial`). Un bon livre pour une lecture approfondie sur le sujet est *Re/Search. Manuale di cultura industriale*, édité par Paolo Bandera, Milan, Shake, 1998, dérivé de *RE-SEARCH - Industrial culture handbook*, initialement publié en 1983.

[^95]: Mariano Equizzi se réfère évidemment ici à la phase la plus politisé du punk italien, pendant laquelle, dans chaque maison, prit d'autres dimensions, généralement apolitiques, comme décrit dans le paragraphe précédent.

[^96]: Mariano Equizzi recommande de consulter le site `www.oec.com` de façon à mieux comprendre les espaces et les sons du monde industriel et ses alentours.
