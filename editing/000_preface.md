# Préface de Derrick de Kerckhove

Tatiana Bazzichelli est une chercheuse et critique militante de la
culture numérique. Ce livre est un tournant dans la théorie critique
qu'elle a commencé à développer dans les années 90 alors qu'elle
étudiait la sociologie. Son intérêt pour la connexion entre l'art et
les médias a mûri à l'intérieur du champ académique, ce qui l'a amenée
à en étudier les implications sociales et politiques. Elle s'est
ensuite concentrée pleinement sur les thèmes de l'art et de
l'éthique hacker et a collaboré directement avec les communautés de
hackers Italiens indépendants ainsi que des activistes en réseau du
secteur artistique.

Ce livre suit l'itinéraire chronologique de sa documentation des
champs de l'art et de l'activisme numérique. C'est une quête qui n'est
pas purement personnelle; qui raconte certaines des expériences de
nombreuses autres personnes ayant commencé des expérimentations
artistiques et technologiques en Italie durant les années 80. Ce type
d'écrit que j'appelle *« autobiographies globales »*, dans le sens où
elles connectent une histoire personnelle à différents domaines
d'activités globalisés. Ce livre se situe dans le contexte de
l'évolution du projet de réseau artistique *AHA*:
*Activism-Hacking-Artivism*. Une initiative débuté en 2001 par
Bazzichelli dans le cadre d'un projet de valorisation de l'art sur
Internet et visant également à donner une plus grande visibilité à la culture
numérique Italienne, *AHA* a contribué à la création d'un vaste réseau
relationnel et de nombreux projets.

[//]: # (Sun Jun 19 13:49:54 Quelle est la "façon inimitable" des)
[//]: # (Italiens et que signifie ce passage)

L'activisme Italien et le net.art sont peu connus du reste du
monde. Il fallait remédier à cette situation, puisque les Italiens,
comme dans bien d'autres domaines et, à leur façon inimitable, sont
aussi innovants en ligne que hors ligne. L'exportation de la pensée
Italienne sur les médias et les technologies en réseaux est une sorte de
vocation que je suis de suis attentivement au travers de plusieurs institutions
en Italie: la Faculté de Sociologie à l'Université Federico II de
Naples, et le centre de recherche M Node de l'Académie des Beaux Arts
de Milan (NABA). Le Programme McLuhan de l'université de Toronto est
une plateforme potentielle de diffusion de l'hacktivisme et des
chemins artistiques Italiens comme mentionnés dans le livre.

En pénétrant au coeur des dynamiques de réseau actuelles, dans de
complexes processus sur Internet, chacun notera que plutôt que
l'habituelle focalisation sur les moyens technologiques, il y a une
tendance croissante a s'intéresser aux personnes, leur façon d'entrer
en contact, leurs amitiés et relations socio-culturelles, ainsi que
leur enracinement dans la réalité de leurs lieux de vie. Il n'y a pas
très longtemps, on parlait beaucoup de virtuel, mais aujourd'hui, il
est clair que ce sont bien des personnes en chair et en os et pas
seulement des machines qui constituent le réseau.

[//]: # (Sun Jun 19 15:06:32 On ne peut pas se "limiter à analyser des)
[//]: # (contenus singuliers" mais on devrait "analyser les liens entre les)
[//]: # (personnes qui créent ces contenus": c'est une déclaration)
[//]: # (d'intention qui semble extrêmement importante ici, particulièrement)
[//]: # (dans un contexte "Italien", mais cette dimension est sous-analysée)
[//]: # (de façon générale dans le milieu artistique, sauf bien sur dans)
[//]: # (l'analyse Bourdieusienne ou celles de Nathalie Heinich, qui se)
[//]: # (retrouve attaquée d'ailleurs sur ce travail pour des raisons venant)
[//]: # (du fait qu'elle écrit des processus que l'on préfère taire en)
[//]: # (général)

Le *réseau de participation*, la formation de réseaux ou de relations au
travers de la technologie est un phénomène mondial de plus en plus
omniprésent, et l'analyse des méthodes par lesquelles ces réseaux se
forment est devenu une nécessité pour celles et ceux qui traitent de
culture numérique. Chacun ne peut pas se limiter à uniquement analyser
des contenus singuliers se trouvant sur internet, mais devrait plutôt
essayer de comprendre les liens qu'entretiennent au moment présent, et de
façon approfondie les personnes qui créent de tels contenus. Il est
indispensable par conséquent de considérer les dimensions sociales de
ces liens. Ce livre se veut donc une contribution à une meilleure
considération des communautés de networkers Italiens.


[//]: # (Sun Jun 19 22:41:31 "prophetic with respect": je ne sais ce que)
[//]: # (cela peut vouloir dire excepté une forme de respect pour les)
[//]: # (religieux lors de l'utilisation du mot "prophétique" en milieu)
[//]: # (laïque)
[//]: # (Ce paragraphe est fortement à relire ^^)

En citant la fameuse phrase de Marshall McLuhan *« le médium est le
message »*[^1], on pourrait dire aujourd'hui que *le réseau est le
message du médium Internet*. Le phénomène du réseau a été anticipé par
la pratique du mail art bien avant que Internet n'ait été élaboré, de
la même manière que le *pointillisme* de Seurat peut être considéré
comme préfigurant les développements ultérieurs de l'image
télévisuelle. Jusqu'à récemment en Amérique, le terme réseau désignait
le médium télévisuel, mais aujourd'hui cela concerne une dimension
liée à une plus large et vaste réalité, qui est Internet. Le réseau
est devenu *« un maillage d'interactions sociales »*, c'est le
message transmis par le médium Internet, à son tour réseau permettant
la transmission.

Ce réseau de relations représente le message du réseau technique. Si
le médium conditionne le message (bien que convergeant au travers de
Internet, la télévision, les livres, la radio, les téléphones fixes
et les téléphones cellulaires, font transiter des messages
spécifiques), sur Internet (un médium basé sur la création de
réseaux de connexions), le message réside dans les relations sociales
tissées par ces médias.



[//]: # (Thu Jun 23 19:12:32 J'ai des doutes ici quand à la traduction de)
[//]: # ("utilisateur" pour "user", j'hésite pour "public" et "individu")

Tout ceci nous entraîne vers le rôle de l'utilisateur. McLuhan a
souvent plaisanté sur le fait que *« Si le médium est le message, alors
l'utilisateur est le contenu »*. Ce qu'il laisse entendre, je pense, est
que les médias ne sont pas juste des supports ou des contenants de
messages, mais bien leurs *environnements* primaires. Ainsi, le médium
prend tour à tour la forme du contenu et de l'utilisateur. Si le
médium conditionne le message, l'utilisateur devient le contenu de ce
message et cela vaut pour toutes les formes de réseautage. Avec
l'extension du réseau Internet, la position de chacun parmi le flux
d'informations a été modifiée: aujourd'hui, le net nous permet de
diffuser nos pensées à un niveau mondial; chose qui était dans le
passé tout simplement utopique, chacun peut désormais en expérimenter
la concrétisation. Une fois en ligne, nous devenons littéralement le
contenu d'Internet.


La structure du médium conditionne aussi la perception que chacun a de
son identité: le fait que chacun soit aux manettes de la production et
de la création de contenus numériques détermine aussi un changement
dans les structures du net et dans nos façons de communiquer et
d'interagir avec le monde extérieur. Nous portons une aura de
communication tout autour de nous.


[//]: # (Thu Jun 23 19:42:06 En 2016, le fait que Internet ne soit pas un)
[//]: # (vecteur de consommation passive est loin d'être vrai (Facebook,)
[//]: # (etc.), même si cela implique toujours plus "d'action" que le médium)
[//]: # (télévisuel)

Dans le cas de la télé, les images télévisuelles rentrent en
communication directe avec le corps du téléspectateur. La télévision
s'adresse à l'intériorité de chacun et le faisceau d'électrons imprime
ses dimensions sensorielles et émotionnelles directement dans le
système nerveux du téléspectateur; c'est une forme d'action physique,
qui est transmise par le son et des images en mouvement. Mais avec
Internet, nous partageons la responsabilité de la création du sens
avec la technologie; nous ne sommes pas seulement consommateurs de
l'information, mais également producteurs, créateurs, et notre
production devient une part active des dynamiques de réseau. Il suffit
de regarder *YouTube*. Internet est un médium, qui contient en lui même
tous les autres médias, de la même façon que le téléphone mobile
résume à lui seul l'histoire des médias en réunissant la parole,
l'écriture et l'électricité en un seul dispositif portable.


Avec Internet ou les téléphones mobiles, conçus comme des plateformes
en réseau, un flux d'informations connectées est généré et diffusé
mondialement et nos existences, avec leurs tendances propres, leurs
préférences et connections spécifiques, deviennent un levier dans la
production et réception de relations aussi bien que
d'information. Comment l'utilisateur devient-il le contenu d'Internet?
Tout d'abord, nous devons abandonner l'illusion qui nous fait croire
que nous sommes des conteneurs d'information. L'utilisateur devient
plutôt un producteur actif de cette information dans des pratiques
telles que le *partage de signets*[^2] ou le *taggage*[^4].

Les logiciels sociaux, que ce soit les encyclopédies personnelles
telles que *Del.icio.us*[^5], *Small Worlds*[^6] pour
les rencontres humaines et les contacts professionnels, ou comme
*Flickr*[^7] et *YouTube*[^8] pour partager des média personnels,
peuvent être utilisés à la fois sur le plan professionnel ou amateur,
chaque innovation renforçant la solidité et les possibilités du
réseau.

[//]: # (Sat Jun 25 00:35:47 C'est sans parler des meta-moteurs de recherche
[//]: # (comme DuckDuckGo))

La création de communautés en expansion basées sur des intérêts
communs, dans lesquelles il est possible d'interagir et d'impliquer
divers médias, est quelque chose que j'explique en examinant le
concept d'*« hypertinence »*. Il s'agit d'un néologisme que j'ai créé afin
de décrire l'exactitude croissante des rapports d'offre et de demande
(et vice-versa) au travers des contenus sur Internet et au travers des
contextes d'informations qui y sont créés. Un bon exemple
d'hypertinence est le perfectionnement des moteurs de recherche et
l'accès à l'information sur le net: désormais chacun passe en
permanence de Yahoo à Google et de Google à *Del.icio.us*. Au travers de
ces plateformes de réseaux sociaux telles que *Del.icio.us*, nous
passons de Wikipédia, une forme de collaboration sur le net plus ou
moins anonyme et néanmoins géniale, à la formation de myriades de
réseaux d'intelligence connectée en-flux-tendus-en-ligne-à-la-demande
impliquant diverses personnes partageant ainsi leurs intérêts
particuliers.

Chacun donne accès à une page d'accueil personnelle depuis laquelle
divers tags bifurquent, ce qui permet l'organisation d'éléments
numériques tels que des photos, du texte, et des vidéos, tous
répertoriés selon un catalogue spécifique de mots clés qui peuvent
aussi être partagés entre différentes personnes. Ces catalogues
deviennent accessibles à quiconque se connecte à la plateforme et a
l'intention d'échanger des information ayant un contenu
similaire. L'évolution du net démontre que, progressivement, sont
crées de plus en plus de situations selon lesquelles la créations et
et la production de connaissance sont toujours plus pertinentes et que
l'accès à l'information devient un socle commun - un processus qui
implique directement les personnes, les utilisateurs individuels qui
tendent à s'améliorer personnellement dans le but de créer une
cohérence plus forte dans leurs préférences personnelles.


La spécificité structurelle du net réside dans la commutation de
paquets, tous les processus sur le net aboutissant au travers
d'elle. Le principe technique sur lequel repose la commutation de
paquets consiste à fragmenter chaque message dans de petits paquets
d'informations, avant de les envoyer en ligne et de leur donner à
chacun une adresse/identité spécifique
ainsi qu'un code afin d'indiquer l'ordre dans lequel ils sont supposés
être recombinés à l'arrivée. Grâce aux tags, il est possible de rendre
le contenu de chacun accessible à une communauté mondiale sans
toutefois définir une hiérarchie entre eux et entre leurs points
d'accès. L'information partagée, les messages échangés, les goûts
personnels indexés sur le net au travers de systèmes de tags
interconnectés, offrent une évolution plus profonde des réseaux
sociaux et permettent l'ajout d'un degré supplémentaire de maturité
sur le net. Sur *Del.icio.us*, quiconque peut participer librement et
gratuitement sans restriction. N'importe qui peut insérer son contenu,
qu'il soit photographique ou textuel, et y associer des tags
spécifiques, mots clés qui sont interprétés comme autant de
connexions. Ces mots clés circulent parmi la communauté et au delà, et
peuvent attirer d'autres personnes avec des centres d'intérêts
communs, qui accéderont à leur tour à la plateforme et échangeront des
informations et des documents. Les groupes d'intérêts thématiques
générés sont du coup une forme d'évolution connectée des blogs, qui
étaient la première forme de personnalisation du net et un exemple
concret de réseau participatif, dans le cas où ils étaient associés
avec d'autres blogs *« amis »* au travers de sujets similaires.


Un exemple simple permettant de comprendre comment les réseaux sociaux
fonctionnent sur le net est celui de *Flickr*. Supposons que vous avez
été invité à un mariage et que vous ayiez pris quelques
photographies. Le site vous permet de les publier en ligne et d'y
ajouter des descriptions en tapant les noms des personnes qui
apparaissent sur les clichés. Vous pouvez aussi les laisser vierges,
en espérant que quelqu'un d'autre présent au mariage, et qui s'est
également connecté à *Flickr*, pour uploader ses propres photos ou pour
voir celles qui ont été postées, connaisse les noms manquants. *Flickr*
permet à ses utilisateurs d'ajouter les noms oubliés. Cet exemple
permet de pointer du doigt le fait qu'à travers ces plateformes en
réseau, chacun peut donner naissance à un réseau de connexions
complexe, comme si chacun tenait à jour un Wikipédia personnalisé sur
ses propres activités.


Des plateformes telles que Wikipédia, utilisent un mécanisme
similaire, avec des interconnexions stratégiques, ce qui est cohérent
avec le principe participatif, mais s'y oppose néanmoins au travers de
certaines modalités de ses pratiques. Wikipédia est une encyclopédie
libre en ligne et est rédigée par des contributeurs anonymes. Le
*taggage* de plateformes telles que *Del.icio.us* ou *Flickr*, permet
d'autre part la mise en avant de la présence d'utilisateurs
spécifiques sur le net et donne au public un cliché d'un ensemble
d'informations basées sur les intérêts de chacun. De cette façon, les
contributions utiles bien que anonymes ont autant leur place en ligne
que l'information personnalisée.


Ces deux projets sont des exemples de la systématisation technologique
de la pensée connectée qui provient de l'intelligence de ceux et
celles qui créent quelque chose d'utile et d'accessible au plus grand
nombre.

Au travers de ces stratégies d'interconnexion, donnant la possibilité
aux personnes de partager leur propre information par des méthodes de
plus en plus sophistiquées, nous sommes en présence de ce que j'ai
récemment décrit sous le terme d'*« intelligence connectée »*, où le net
accorde une place centrale aux stratégies d'auto-organisation. Sur ces
plateformes, il n'y a pas de limite à la taille des contenus publiés et
chacun peut les partager avec le monde entier, attirant plus
spécifiquement les personnes partageant nos centres d'intérêt. Le
*taggage* est une forme d'*amélioration des réseautages sociaux*, et donne
la possibilité de créer une vaste conscience plurielle. Dans ces
formes de connectivité, je vois une résurrection de l'aura, envisagée
comme cet halo tactile mais imperceptible, créé par les connections
informationnelles de chacun·e d'entre nous, telles que nos liaisons
sentimentales et nos amitiés, qui sont toutes organisées sous la forme
d'un tout étendu - un réseau de relations qui représente la façon dont
nous interagissons avec notre environnement et ce que nous avons
partagé jusque là. L'artiste devient un réseauteur et créé des
possibilités d'échange entre personnes devenant partie prenante du
réseau ainsi conçu, ce qui signifie, afin de poursuivre dans l'idée de
l'aura, de jouir de toutes les connexions possibles qui pourront se
présenter au sein du réseau. L'aura d'une personne connectée dans un
système d'information professionnel et amical du net représente sa
communicabilité et l'interconnexion de tous ses liens. Par exemple,
notre ordinateur, notre téléphone mobile contiennent eux aussi l'aura
de notre cohabitation dans ce monde, les messages que nous envoyons et
recevons, la liste d'amis et de connaissances dans notre carnet
d'adresse, les fichiers que nous avons sauvegardés et archivés, etc. Ce
réseau nous appartient personnellement, et fait de nous des membres
d'une communauté qui se rejoignent dans des systèmes hyper-cognitifs
de la sphère informationnelle. Cela constitue notre persona numérique,
malgré que la plupart de cela échappe entièrement à notre contrôle.


Dans le livre de Tatiana Bazzichelli, cette connexion est vue en tant
que pratique artistique. Les plateformes d'interaction, les systèmes
d'exploitation libre tels que GNU/Linux, les projets indépendants,
expérimentaux ou communautaires et les mouvements d'*hacktivistes*
sont présentés comme des oeuvres d'art. Le réseau lui même devient une
oeuvre d'art. Quand j'étais membre du jury du Ars Electronic Festival
à Linz en 1994, j'ai été confronté, parmi les quatre autres membres,
Joi Ichi Ito, Franz Manola, Morgan Russell et Mitsuhiro Takemura, au
besoin de formaliser des critères de jugement afin d'analyser les
travaux envoyés dans la toute nouvelle catégorie d'art développé sur
le net. Nous nous sommes demandés comment nous pouvions juger ces
formes d'art, si c'était le médium www qui les soutenait, et nous
avons alors décidé que la connectivité était le message[^3]. Parmi les
critères d'évaluation de la validité artistique d'un site web, j'ai
proposé *webness*". Par *webness* j'entendais la qualité de la
connectivité des projets. Ces critères nous ont mené à attribuer le
prix Golden Nica à Linus Torvald lui même, pour avoir inventé et
diffusé l'utilisation de Linux l'année précédente. En 1979, à la toute
première edition du Ars Electronic Festival à Linz, la culture
numérique était un thème avant-gardiste. Aujourd'hui, nous ne nous
référons plus à une Utopie, mais nous succombons à la tentation
d'interpréter en termes économiques et sociaux une période dans
laquelle le progrès technologique est devenu la grammaire du
présent. D'un coté, nous vivons dans une segmentation et
spécialisation progressives, ce qui est démontré par la technique du
*taggage*, et de l'autre coté, nous expérimentons une inclusion
généralisée impliquant le commun des mortels ayant accès à des
technologies complètement inimaginables il y a 20 ans de cela (telles
que la vidéoconférence, le multimédia ou les utilisations
multisensorielles des téléphones mobiles). Les personnes sont devenues
le contenu actif et les producteurs de l'information en réseau.


La pensée et les activités Italiennes relatives au net sont devenues
un point central d'explication du mécanisme d'évolution des médias,
qui voient la présence de l'utilisateur comme étant toujours plus
actif et pertinent. Les expérimentations racontées dans ce livre
montrent un réseau de personnes qui agissent comme une alternative aux
moyens standardises de production culturels, informationnels et
artistiques. Ces personnes inondent leurs propres relations et amitiés
politiques, sociales, artistiques directement dans des utilisations
créatives des médias; un réseau qui existait avant l'évolution
d'Internet, au travers de l'utilisation des BBS et même avant cela par
le mail art. Il est possible de comprendre au travers des projets et
activités contenus dans ce livre, à quel point la composante centrale
du réseau en Italie est composée du tissu relationnel: aller à une
conférence, participer à un festival, discuter et partager des projets
avec d'autres, se rencontrer dans un bar ou un restaurant avec des
personnes qui partagent nos centres d'intérêt, toutes ces situations
deviennent des occasions créatives de produire de nouvelles activités
et projets.


Le travail de Tatiana Bazzichelli propose que nous percevions notre
présent culturel au travers de l'art. L'art peut être utilisé comme un
outil pour analyser les processus culturels actuels, et ce genre
d'étude, toujours en cours à ce jour - comme le présent ouvrage - est
un outil précieux nous permettant de comprendre qui nous sommes et où
nous allons. C'est une tranche de culture Italienne à l'intérieur du
réseau, une leçon d'importance pour les jeunes et de nombreu·ses·x
étudiant·e·s, universitaires, critiques et artistes qui pourront puiser
leur inspiration de ce texte bien que ne connaissant pas les origines
du net.art et de la culture Italienne numérique.


L'Italie est un pays contrôlé par des moyens de communication
médiatique à sens unique, et tout particulièrement la télévision: il
est plutôt pertinent que la résistance à un médium collectif se fasse
par l'utilisation d'un médium connecté, et en même temps, donne
naissance à une tradition artistique du réseau impliquant le pays
dans son ensemble et reste toujours en évolution. Lors de la création
d'une connectivité tellement étendue, l'accès libre au net crée une
occasion de développer les moyens de communication de chacun depuis la
base, en la façonnant aux besoins de tous - besoins qui deviennent
une pratique artistique subversive afin de créer de nouvelles étapes
pour permettre la participation libre et la visibilité pour tous. Je
soutiens l'effort de Tatiana Bazzichelli en ce sens. Ainsi que ce
livre.

[^1]: Même s'il n'a jamais traité directement de quoi que ce soit s'approchant d'Internet, Marshall McLuhan l'a en un certain sens anticipé dans le cinquième chapitre de *Understanding Media*, en prédisant le développement de technologies électroniques au travers desquelles non seulement nos sens, mais aussi nos consciences pourront véhiculer (le livre fut écrit en 1962 et publié en 1964).

[^2]: Pour un approfondissement sur les concepts de taggage et partage de signets, se reporter aux descriptions sur les pages Wikipédia respectives: `en.wikipedia.org/wiki/Tags` et `en.wikipedia.org/wiki/Social_bookmarking`.

[^3]: La déclaration du jury pour la catégorie www se trouve sur le site de l'édition 1995 du Ars Electronic Festival: [`https://tinyurl.com/y745rnkl`](https://web.archive.org/web/20060202131039/www.aec.at/en/archives/prix_archive/prixjuryStatement.asp?iProjectID=2554).

[^4]: Respectivement *« social bookmarking »* et *« tagging »*, N.d.T.

[^5]: `http://del.icio.us`

[^6]: `www.asmallworld.net`

[^7]: `www.flickr.com`

[^8]: `www.youtube.com`
